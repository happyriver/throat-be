;; html/index.clj -- Index page for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.views.index
  (:require [cheshire.core :as cheshire]
            [hiccup.page :refer [html5]]
            [throat-be.protocols.asset-path :refer [asset-path]]))

(defn make-index-html
  "Make the HTML for the index page."
  [{:keys [assets env session csrf-token site-name head-data
           body-data]}]
  (let [language (or (:language session) "en")
        {:keys [title meta links]} head-data
        css-links (concat ["/css/purecss/base-min.css"
                           "/css/purecss/forms-min.css"
                           "/css/purecss/buttons-min.css"
                           "/css/purecss/grids-min.css"
                           "/css/purecss/grids-responsive-min.css"
                           "/css/purecss/menus-min.css"]
                          (if (= env :dev)
                            ["/css/tachyons.css"
                             "/css/main.css"
                             "/css/dark.css"
                             "/css/throat-fe.css"
                             "/css/custom.css"
                             "/css/pikaday/pikaday.css"]
                            ["/css/throat-fe.min.css"]))]
    (html5
     {:lang language}
     (into
      [:head
       [:title title]
       [:meta {:charset "utf-8"}]
       [:meta {:name "viewport"
               :content "width=device-width, initial-scale=1.0"}]
       [:link {:rel "icon" :type "image/png"
               :href (asset-path assets "/img/icon.png?v9")}]
       [:meta {:name "theme-color" :content "white"}]
       [:meta {:name "msapplication-navbutton-color" :content "white"}]
       [:meta {:name "apple-mobile-web-app-capable" :content "yes"}]
       [:meta {:name "apple-mobile-web-app-status-bar-style"
               :content "white-translucent"}]
       [:link {:rel "apple-touch-icon" :sizes "180x180"
               :href "/static/img/apple-touch-icon.png"}]
       [:link {:rel "icon" :type "image/png" :sizes "32x32"
               :href "/static/img/favicon-32x32.png"}]
       [:link {:rel "icon" :type "image/png" :sizes "16x16"
               :href "/static/img/favicon-16x16.png"}]
       [:link {:rel "manifest" :href "/static/img/site.webmanifest"}]]

      (concat (map (fn [m] [:meta m]) meta)
              (map (fn [l] [:link l]) links)
              (map (fn [css]
                     [:link {:rel "stylesheet"
                             :type "text/css"
                             :href (asset-path assets css)}])
                   css-links)))
     [:body
      [:noscript
       (str site-name
            " is a JavaScript app.  Please enable JavaScript to continue.")]
      #_(into
         [:div#data
          (when csrf-token
            [:label {:id "csrf" :data-value csrf-token}])]
         (map (fn [[k v]]
                [:label {:id (name k) :data-value (cheshire/generate-string v)}])
              body-data))
      (into
       [:div#app
        (when csrf-token
          [:label {:id "csrf" :data-value csrf-token}])]
       (map (fn [[k v]]
              [:label {:id (name k) :data-value (cheshire/generate-string v)}])
            body-data))
      [:script {:src (asset-path assets "/js/compiled/app.js")}]])))
