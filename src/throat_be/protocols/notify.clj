;; protocols/notify.clj -- Notifications protocol declaration for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.protocols.notify)

(defprotocol Notify
  "Protocol for message exchange between throat-be and Python throat."
  (subscribe-user [this event uid handler]
    "Subscribe to event notifications sent to a user.
     Call `handler` with the payload of the notification messages.")
  (subscribe-site [this event handler]
    "Subscribe to site-wide event notification messages.
     Call `handler` with the payload of the messages.")
  (subscribe-grab-title [this token handler]
    "Subscribe to grab title result notification messages.
     Call `handler` with the payload of the messages.")
  (publish [this event context resolution]
    "Convert a resolved value into a Python Throat message and publish it.")
  (close-subscription [this subscription]
    "Close a subscription."))
