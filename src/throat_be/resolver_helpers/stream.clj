;; resolver-helpers/stream.clj -- Streaming helpers for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.resolver-helpers.stream
  (:require [clojure.core.async :refer [<! alt! chan close!
                                        go go-loop thread timeout]]))

(defn action-at-intervals
  "Start calling 'f' at intervals of 'time-in-ms'.  Return a channel
  to which can be closed to stop the action."
  [f time-in-ms]
  (let [stop (chan)]
    (go-loop []
      (alt!
        stop :stop
        (timeout time-in-ms) (do (<! (thread (f)))
				 (recur))))
    stop))

(defn stream-resolution
  "Deliver a resolution, then close the connection."
  [resolution source-stream-callback]
  (let [action
        (go
          (source-stream-callback resolution)
          ;; If there's no pause in between the two calls, the
          ;; resolution doesn't get delivered.  1 ms works, using
          ;; 5 to be safe.
          (<! (timeout 5))
          (source-stream-callback nil))]
    #(close! action)))
