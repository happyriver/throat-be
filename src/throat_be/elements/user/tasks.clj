;; elements/user/tasks.clj -- Task descriptions for throat-be users
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.user.tasks
  (:require
   [cambium.core :as log]
   [throat-be.elements.user.query :refer [select-customer-update-log
                                           update-customer-update-log]]
   [throat-be.protocols.payment-subscription :as subscription]
   [throat-be.util :as util]))

(defn update-customers
  "Update the customer update log."
  [{:keys [db options]}]
  (let [subscription (:payment-subscription options)
        db-with-counter (assoc db :counter (atom 0))
        log-entries (select-customer-update-log db-with-counter)]
    (log/info {:count (count log-entries)} "Updating customer subscriptions")
    (doseq [entry log-entries]
      (let [{:keys [id action uid resets value]} entry
            user {:uid uid :resets resets}
            result (cond
                     (= "change_email" action)
                     (subscription/update-email subscription user value)

                     (= "cancel_subscription" action)
                     (subscription/cancel subscription user)

                     :else
                     (do
                       (log/error {:customer_update_log entry}
                                  "Unrecognized action in customer_update_log")
                       :failure))]
        (when (not= :retry result)
          (update-customer-update-log db-with-counter
                                      {:id id
                                       :completed (util/sql-timestamp)
                                       :success (= :success result)}))))))
