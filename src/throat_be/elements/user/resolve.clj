;; elements/user/resolve.clj -- GraphQL resolvers for User data
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.user.resolve
  "Contains custom resolvers for Users."
  (:require
   [cambium.core :as log]
   [clojure.core.async :refer [go close!]]
   [clojure.set :as set]
   [com.rpl.specter :as s]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [java-time.api :as jt]
   [promesa.core :as prom]
   [superlifter.api :refer [def-superfetcher]  :as superlifter]
   [throat-be.elements.constants :as constants]
   [throat-be.elements.user.query :as query]
   [throat-be.protocols.bus :as bus]
   [throat-be.protocols.current-user :refer [get-name get-uid is-admin?
                                             subs-moderated  totp-expiration
                                             user user-if-realized]]
   [throat-be.protocols.schema :as schema]
   [throat-be.resolver-helpers.data :as data]
   [throat-be.util :as util]))

;;; Data Fetchers and fixers

(defn- prune-username-history
  "Remove entries older than the configured number of days.
  Admins, mods, and users looking at themselves can see old entries."
  [history {:keys [uid]} current-user {days :username_change_display_days}]
  (if (or (= uid (get-uid current-user))
          (is-admin? current-user)
          (seq (subs-moderated current-user)))
    history
    (let [now (jt/zoned-date-time)
          limit (jt/minus now (jt/days days))]
      (filter (fn [entry]
                (jt/after? (-> (:changed entry)
                               (jt/zoned-date-time "UTC"))
                           limit))
              history))))

(defn fixup-user
  "Make the database user fields match the query user fields."
  [{:keys [status] :as user}
   {:keys [current-user site-config]}]
  (-> (if (keyword? status)
        user
        (update user :status constants/reverse-user-status-map))
      (update :username_history
              prune-username-history user current-user (site-config))
      (set/rename-keys {:subs ::subs
                        :mods ::mods
                        :meta ::meta
                        :content_blocks ::content_blocks
                        :username_history ::username_history})))

(defn load-users-by-uid
  "Load users for the superfetcher."
  [selectors {:keys [db current-user]}]
  (let [uids (map :uid selectors)
        or-maps (fn [maps]
                  (apply merge-with #(or %1 %2) maps))
        wants (or-maps selectors)
        params (merge wants
                      {:uids uids
                       :uid (get-uid current-user)})]
    (query/list-users-by-uid db params)))

(def-superfetcher FetchUserByUid [id]
  (data/make-fetcher-fn :uid load-users-by-uid fixup-user))

(defn load-users-by-name
  [selectors {:keys [db current-user]}]
  (let [names (map :name selectors)
        or-maps (fn [maps]
                  (apply merge-with #(or %1 %2) maps))
        wants (or-maps selectors)
        params (merge wants
                      {:names names
                       :uid (get-uid current-user)})]
    (query/list-users-by-name db params)))

(def-superfetcher FetchUserByName [id]
  (data/make-fetcher-fn :name load-users-by-name fixup-user))

(defn- fetch-user
  "Promise to fetch a user by uid or by name.
  Return a promesa promise"
  [context select bucket]
  (let [id (data/id-from-fetch-args select)
        constructor (cond
                      (contains? select :uid) ->FetchUserByUid
                      (contains? select :name) ->FetchUserByName)]
    (data/with-superlifter context
      (superlifter/enqueue! bucket (constructor id)))))

;;; Directive support

(defmethod schema/redact-content ::name-of-deleted-user
  [_ _ user roles]
  (if (and (= (:status user) :DELETED) (not (roles :IS_ADMIN)))
    (assoc user :name nil)
    user))

;;; Fields and query construction

(def ^:private fields-by-query-section
  "Lists of fields that trigger the need for parts of the user queries."
  {:subscriptions?       [:User/subscriptions]
   :score?               [:User/score
                          :User/level
                          :User/progress]
   :notification-counts? [:User/unread_message_count
                          :User/unread_notification_count]
   :attributes?          [:User/attributes]
   :subs-moderated?      [:User/subs_moderated]
   :content-block?       [:User/content_blocks]
   :personal?            [:User/language :User/resets]
   :username-history?    [:User/username_history]})

(defn- wants
  "Determine which parts of the user query need to be included."
  [context]
  (s/transform [s/MAP-VALS] (fn [fields]
                              (data/wants-fields? context fields))
               fields-by-query-section))

;; Not a resolver, but produces a cleaned-up user object for load-user.
(defn resolved-user
  "Get a user object, for use by load-user.
  Has everything except the notification counts"
  [{:keys [db]} {:keys [uid]}]
  (let [want-all-fields (into {} (map #(vector % true)
                                      (keys fields-by-query-section)))
        params (-> {:uid uid :uids [uid]}
                   (merge want-all-fields)
                   (assoc :notification-counts? false))]
    (-> (query/list-users-by-uid db params)
        first
        (update :status constants/reverse-user-status-map))))

;;; Schema Queries

(defn user-by-name-or-uid
  [context args _]
  (let [params (merge args (wants context))]
    (fetch-user context params :immediate)))

(defn user-by-reference
  "Resolve a user object based on results of the parent resolver.
  If ::user is bound, it contains a user object that has been
  prefetched, otherwise fetch the user using the uid at :uid."
  [context _ {:keys [uid ::user]}]
  (or user
      (when uid
        (let [params (merge {:uid uid} (wants context))]
          (fetch-user context params :user-bucket)))))

(defn user-list-by-reference
  [context _ {:keys [uids]}]
  (let [select (wants context)
        fetch-one (fn [uid]
                    (let [args (assoc select :uid uid)]
                      (fetch-user context args :user-bucket)))]
    (prom/all (map fetch-one uids))))

(defn attributes-by-reference
  "Resolve the user's attributes.
  Uses metadata fetched by the resolved parent :User query."
  [{:keys [current-user]} _ {:keys [::meta]}]
  (-> meta
      (set/rename-keys {:admin    :can_admin
                        :labrat   :lab_rat
                        :nochat   :no_chat
                        :nostyles :no_styles
                        :subtheme :sub_theme})
      (util/convert-fields-to-boolean [:can_admin
                                       :lab_rat
                                       :no_chat
                                       :no_styles
                                       :nsfw
                                       :nsfw_blur])
      ;; TODO this needs to check site config and allow
      ;; regular users to upload.
      (assoc :can_upload (some? (is-admin? current-user))
             :totp_expiration (some-> (totp-expiration current-user)
                                      str))))

(defn content-blocks-by-reference
  "Resolve the user's content blocks.
  Use data fetched by the resolved parent :User query."
  [context _ {:keys [::content_blocks]}]
  (let [blocks (->> content_blocks
                    (map #(update % :content_block
                                  constants/reverse-user-content-block-map)))]
    (data/with-superlifter context
      (-> (prom/promise blocks)
          (data/update-trigger! context :UserContentBlock/user :user-bucket
                                data/raise-threshold-by-count)))))

(defn subscriptions-by-reference
  "Resolve the user's subscriptions.
  Uses the list fetched by the resolved parent :User query."
  [_ _ {:keys [::subs]}]
  (->> subs
       (remove nil?)
       (map #(update % :status constants/reverse-subscription-status-map))))

(defn username-history-by-reference
  "Resolve the username history.
  Uses the resolved parent :User query."
  [_ _ {:keys [::username_history]}]
  username_history)

(defn moderates-by-reference
  "Resolve the subs the user moderates.
  Uses the list fetched by the resolved parent :User query."
  [context _ {:keys [::mods]}]
  (let [fixup-submod (fn [{:keys [sid power_level]}]
                       {:sid sid
                        :moderation_level
                        (constants/reverse-moderation-level-map power_level)})]
    (data/with-superlifter context
      (-> (prom/promise (map fixup-submod mods))
          (data/update-trigger! context :SubModeration/sub :sub-bucket
                                data/raise-threshold-by-count)))))

(defn current-user
  "Resolve the logged-in user."
  [{:keys [current-user] :as context} _ _]
  (when-let [uid (get-uid current-user)]
    (let [wants (wants context)
          ;; Fetching the logged in user record is common, so use that
          ;; result if we already have it.
          user (user-if-realized current-user)]
      (if (and user (not (:notification-counts? wants)))
        (fixup-user user context)
        (fetch-user context (merge {:uid uid} wants) :immediate)))))

;;; Resolver factories

(defn user-by-reference-factory
  "Generate a resolver to load a user reference from a uid."
  [key]
  (fn [context args resolved]
    (when-let [uid (key resolved)]
      (user-by-reference context args (assoc resolved :uid uid)))))

;;; Streamers

(defn stream-subscription-updates
  "Stream updates to subscriptions, as they are received."
  [{:keys [bus current-user]} _ source-stream-callback]
  (if-not (get-uid current-user)
    (let [action (go (source-stream-callback nil))]
      #(close! action))
    (let [uid (get-uid current-user)
          sub (bus/subscribe bus ::subscription-update uid
                             source-stream-callback)]
      #(bus/close-subscription bus sub))))

;;; Schema Mutations

(defn- new-status-from-change
  "Give the desired new status for a subscription based on the change."
  [change]
  (-> change
      {:SUBSCRIBE   :SUBSCRIBED
       :UNSUBSCRIBE :SUBSCRIBED
       :BLOCK       :BLOCKED
       :UNBLOCK     :BLOCKED}
      constants/subscription-status-map))

(defn- run-and-fixup-update-subscription-status
  "Insert, update or delete a subscription status, and return the new state."
  [db change params]
  (-> (if (#{:SUBSCRIBE :BLOCK} change)
        (query/update-subscription-status db params)
        (query/delete-subscription-status db params))
      first
      (update :status constants/reverse-subscription-status-map)))

(defn change-subscription-status
  "Subscribe, unsubscribe, block or unblock a sub."
  [{:keys [current-user db bus]} {:keys [sid change]} _]
  (let [status (new-status-from-change change)
        uid (get-uid current-user)
        params {:uid uid :sid sid :status status}
        subscr (run-and-fixup-update-subscription-status db change params)]
    (bus/publish bus ::subscription-update uid subscr)
    (log/info {:sid sid :status status} "Updated user subscription status")
    subscr))

(defn- filter-recent-history
  [history change-limit-days]
  (if (zero? change-limit-days)
    history
    (let [now (jt/zoned-date-time)
          change-date (jt/minus now (jt/days change-limit-days))]
      (filter #(jt/after? (-> (:changed %)
                              (jt/zoned-date-time "UTC"))
                          change-date)
              history))))

(defn register-user
  "Register user.
  This function is incomplete and should only be used for tests."
  [{:keys [db env]} {:keys [name _invite_code]} _]
  (cond
    (not= env :test)
    (resolve/resolve-as nil {:message "Use for testing only."
                             :status 404})
    :else
    (let [uid (util/uuid4)
          joindate (util/sql-timestamp)
          values {:uid uid
                  :joindate joindate
                  :name name
                  :status 0}]
      (query/insert-user db values)
      {:uid uid
       :joindate joindate
       :name name
       :score 0
       :given 0
       :status 0
       :language ""
       :posts nil
       :comments nil
       :attributes {}
       :subscriptions []
       :subs_moderated []})))
