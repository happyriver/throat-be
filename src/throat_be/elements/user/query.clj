;; elements/user/query.clj -- SQL queries for User data
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.user.query
  (:require
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [hugsql.core :as hugsql]
   [throat-be.elements.constants :refer [wrap-db-fn
                                         wrap-db-fns
                                         wrap-snip-fn
                                         subscription-status-map]]
   [throat-be.elements.spec.db :as db-spec]
   [throat-be.elements.sql :refer [start-cte-snip
                                   end-cte-snip]]
   [throat-be.elements.user.spec :as user-spec]
   [throat-be.util :as util]))

(hugsql/def-db-fns "sql/user.sql")

;;; Specs for snippet functions.

(spec/def ::where-snip ::db-spec/sqlvec)

(wrap-snip-fn badges-cte-snip)
(db-spec/fdef badges-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::where-snip]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn badges-field-snip)
(db-spec/fdef badges-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn badges-join-snip)
(db-spec/fdef badges-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn by-names-snip)
(db-spec/fdef by-names-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/names]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn by-uids-snip)
(db-spec/fdef by-uids-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uids]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn content-blocks-field-snip)
(db-spec/fdef content-blocks-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn meta-fields-snip)
(db-spec/fdef meta-fields-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn notification-cte-snip)
(db-spec/fdef notification-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::user-spec/uid]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn notification-field-snip)
(db-spec/fdef notification-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn notification-join-snip)
(db-spec/fdef notification-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn personal-field-snip)
(db-spec/fdef personal-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn score-cte-snip)
(db-spec/fdef score-cte-snip
  :args (spec/cat :args (spec/keys :req-un [::where-snip]))
  :ret ::db-spec/sqlvec)

(wrap-snip-fn score-field-snip)
(db-spec/fdef score-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn score-join-snip)
(db-spec/fdef score-join-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn subs-moderated-field-snip)
(db-spec/fdef subs-moderated-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn subscription-field-snip)
(db-spec/fdef subscription-field-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn username-history-field-snip)
(db-spec/fdef username-history-field-snip
  :ret ::db-spec/sqlvec)

;;; Specs for db functions.
(wrap-db-fns _delete-subscription-status
             _update-subscription-status
             insert-user
             select-customer-update-log
             uid-by-name
             update-customer-update-log)

(wrap-db-fn _select-users)
(spec/def ::start-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::end-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::subscription-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::notification-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::notification-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::notification-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::meta-fields-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::subs-moderated-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::content-blocks-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::badges-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::badges-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::badges-join-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::score-cte-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::score-field-snip (spec/nilable ::db-spec/sqlvec))
(spec/def ::score-join-snip (spec/nilable ::db-spec/sqlvec))
(db-spec/fdef _select-users
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::badges-cte-snip
                                            ::badges-field-snip
                                            ::badges-join-snip
                                            ::content-blocks-field-snip
                                            ::end-cte-snip
                                            ::meta-fields-snip
                                            ::notification-cte-snip
                                            ::notification-field-snip
                                            ::notification-join-snip
                                            ::personal-field-snip
                                            ::score-cte-snip
                                            ::score-field-snip
                                            ::score-join-snip
                                            ::start-cte-snip
                                            ::subs-moderated-field-snip
                                            ::subscription-field-snip
                                            ::where-snip]))
  :ret (spec/coll-of
        (spec/keys :req-un [::user-spec/given
                            ::user-spec/joindate
                            ::user-spec/name
                            ::user-spec/status
                            ::user-spec/uid]
                   :opt-un [
                            ;; badges-field-snip
                            ::user-spec/badges
                            ;; content-blocks-field-snip
                            ::user-spec/content_blocks
                            ;; meta-fields-snip
                            ::user-spec/meta
                            ;; notification-field-snip
                            ::user-spec/unread_message_count
                            ::user-spec/unread_notification_count
                            ;; personal-field-snip
                            ::user-spec/email
                            ::user-spec/language
                            ::user-spec/resets
                            ;; score-field-snip
                            ::user-spec/score
                            ::user-spec/level
                            ::user-spec/progress
                            ;; subs-moderated-field-snip
                            ::user-spec/mods
                            ;; subscription-field-snip
                            ::user-spec/subs])))

(defn- snips-for-query
  [{:keys [attributes? badges? content-block? notification-counts?
           personal? score? subs-moderated? subscriptions?
           username-history? uid uids] :as args}
   where-snip]
  (let [use-cte? (or notification-counts? score? badges?)]
    (assoc args
           :start-cte-snip            (when use-cte? (start-cte-snip))
           :end-cte-snip              (when use-cte? (end-cte-snip))
           :where-snip                where-snip
           :badges-cte-snip           (when (or badges? score?)
                                        (badges-cte-snip
                                         {:uids uids
                                          :where-snip where-snip}))
           :badges-field-snip         (when badges? (badges-field-snip))
           :badges-join-snip          (when badges? (badges-field-snip))
           :content-blocks-field-snip (when content-block?
                                        (content-blocks-field-snip))
           :meta-fields-snip          (when attributes? (meta-fields-snip))
           :notification-cte-snip     (when notification-counts?
                                        (notification-cte-snip {:uid uid}))
           :notification-field-snip   (when notification-counts?
                                        (notification-field-snip))
           :notification-join-snip    (when notification-counts?
                                        (notification-join-snip))
           :personal-field-snip       (when personal? (personal-field-snip))
           :subs-moderated-field-snip (when subs-moderated?
                                        (subs-moderated-field-snip))
           :subscription-field-snip   (when subscriptions?
                                        (subscription-field-snip))
           :score-cte-snip            (when score?
                                        (score-cte-snip
                                         {:uids uids
                                          :where-snip where-snip}))
           :score-field-snip          (when score? (score-field-snip))
           :score-join-snip           (when score? (score-join-snip))
           :username-history-field-snip (when username-history?
                                          (username-history-field-snip)))))

(defn- fix-username-history-timestamps
  [{:keys [username_history] :as user}]
  (assoc user :username_history
         (map #(update % :changed util/sql-timestamp-from-epoch)
              username_history)))

(defn list-users-by-uid [db args]
  (->> (snips-for-query args (by-uids-snip args))
       (_select-users db)
       (map fix-username-history-timestamps)))

(defn list-users-by-name [db {:keys [names uid] :as args}]
  (let [params {:names (map str/lower-case names)
                :uid uid}]
    (->> (snips-for-query args (by-names-snip params))
         (_select-users db)
         (map fix-username-history-timestamps))))

(defn user-by-uid-with-notification-counts
  "Get a user record along with notification counts and modded subs."
  [db {:keys [uid]}]
  (first (list-users-by-uid db {:uid uid
                                :uids [uid]
                                :notification-counts? true
                                :subs-moderated? true})))

(defn update-subscription-status
  "Subscribe to or block a sub, and update its subscription count."
  [db {:keys [status] :as args}]
  (let [adjust (if (= (subscription-status-map :SUBSCRIBED) status) 1 0)]
    (_update-subscription-status db (assoc args :adjust adjust))))

(defn delete-subscription-status
  "Unsubscribe to or unblock a sub, and update its subscription count."
  [db {:keys [status] :as args}]
  (let [adjust (if (= (subscription-status-map :SUBSCRIBED) status) 1 0)]
    (_delete-subscription-status db (assoc args :adjust adjust))))

(comment
  (require 'user)
  (def db (:db user/system))
  (list-users-by-name db {:names ["lanewilliam"]
                          :score? true
                          :subscriptions? true
                          :notification-counts? true
                          :attributes? true
                          :subs-moderated? true
                          :uid "e7d7004c-fad8-4b58-8488-9c640fe6ec8e"})
  (snips-for-query {:subscriptions? true})
  )
