;; elements/user/spec.clj -- Specs for users
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.user.spec
  "Specs for user SQL query arguments and fields."
  (:require
   [clojure.spec.alpha :as spec]
   [throat-be.elements.constants :as constants]
   [throat-be.elements.spec :as element-spec]
   [throat-be.elements.sub.spec :as sub-spec]))

(spec/def ::uid string?)
(spec/def ::given int?)
(spec/def ::joindate ::element-spec/timestamp)
(spec/def ::name string?)
(spec/def ::names (spec/coll-of ::name))
(spec/def ::status (-> constants/user-status-map vals set))
(spec/def ::uids (spec/coll-of ::uid))
(spec/def :throat-be.elements.user.spec.badges/name string?)
(spec/def :throat-be.elements.user.spec.badges/alt string?)
(spec/def :throat-be.elements.user.spec.badges/icon string?)
(spec/def ::badges
  (spec/coll-of
   (spec/keys :req-un [:throat-be.elements.user.spec.badges/name
                       :throat-be.elements.user.spec.badges/alt
                       :throat-be.elements.user.spec.badges/icon])))
(spec/def ::content_block
  (spec/nilable
   (-> constants/user-content-block-map vals set)))
(spec/def ::content_blocks
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [::uid
                        ::content_block]))))
(spec/def :throat-be.elements.user.spec.meta/admin #{"1" "0"})
(spec/def :throat-be.elements.user.spec.meta/labrat #{"1" "0"})
(spec/def :throat-be.elements.user.spec.meta/invitecode string?)
(spec/def :throat-be.elements.user.spec.meta/nochat #{"1" "0"})
(spec/def :throat-be.elements.user.spec.meta/nostyles #{"1" "0"})
(spec/def :throat-be.elements.user.spec.meta/nsfw #{"1" "0"})
(spec/def :throat-be.elements.user.spec.meta/nsfw_blur #{"1" "0"})
(spec/def :throat-be.elements.user.spec.meta/subtheme string?)
(spec/def ::meta
  (spec/nilable
   (spec/keys :opt-un [:throat-be.elements.user.spec.meta/admin
                       :throat-be.elements.user.spec.meta/labrat
                       :throat-be.elements.user.spec.meta/invitecode
                       :throat-be.elements.user.spec.meta/nochat
                       :throat-be.elements.user.spec.meta/nostyles
                       :throat-be.elements.user.spec.meta/nsfw
                       :throat-be.elements.user.spec.meta/nsfw_blur
                       :throat-be.elements.user.spec.meta/subtheme])))
(spec/def ::unread_message_count nat-int?)
(spec/def ::unread_notification_count nat-int?)
(spec/def ::language (spec/nilable string?))
(spec/def ::resets nat-int?)
(spec/def ::score int?)
(spec/def ::level (spec/and number? #(<= 0 %)))
(spec/def ::progress (spec/and number? #(<= 0 % 1)))
(spec/def :throat-be.elements.user.spec.mods/power_level
  (-> constants/moderation-level-map vals set))
(spec/def :throat-be.elements.user.spec.mods/sub_name string?)
(spec/def ::mods
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:throat-be.elements.user.spec.mods/power_level
                        ::sub-spec/sid
                        :throat-be.elements.user.spec.mods/sub_name]))))
(spec/def :throat-be.elements.user.spec.subs/order (spec/nilable int?))
(spec/def :throat-be.elements.user.spec.subs/status
  (-> constants/subscription-status-map vals set))
(spec/def :throat-be.elements.user.spec.subs/name string?)
(spec/def ::subs
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:throat-be.elements.user.spec.subs/order
                        :throat-be.elements.user.spec.subs/status
                        :throat-be.elements.user.spec.subs/name
                        ::sub-spec/sid]))))
