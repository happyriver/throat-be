;; elements/site/tasks.clj -- Site-related tasks for throat-be
;; Copyright (C) 2022 The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.site.tasks
  (:require
   [cambium.core :as log]
   [hugsql.core :as hugsql]
   [throat-be.elements.constants :refer [wrap-db-fns]]
   [throat-be.protocols.payment-subscription :as subscription]
   [throat-be.protocols.bus :as bus]))

(hugsql/def-db-fns "sql/site.sql")
(wrap-db-fns pick-a-user)

(defn update-funding-progress
  [{:keys [bus db options state]}]
  (let [subscription (:payment-subscription options)
        db-with-counter (assoc db :counter (atom 0))]
    (try
      (log/debug {} "Updating funding progress")
      (let [user (pick-a-user db-with-counter)
            topic :throat-be.elements.site.resolve/site
            update (fn [old-value]
                     (let [new-value (subscription/stats subscription user)]
                       (if new-value
                         (do
                           (when (not= old-value new-value)
                             (log/info {:new-value new-value}
                                       "Funding progress update")
                             (bus/publish bus topic
                                          "funding-progress" new-value))
                           new-value)
                         old-value)))]
        (swap! state update))
      (catch Exception e
        (log/error e "Error updating funding progress")))))
