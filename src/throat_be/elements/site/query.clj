;; elements/site/query.clj -- Build SQL queries for site information for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.site.query
  "Build SQL queries for site info"
  (:require
   [honey.sql :as sql]
   [hugsql.core :as hugsql]
   [throat-be.elements.constants :refer [wrap-db-fns wrap-snip-fns]]))

(hugsql/def-db-fns "sql/site.sql")
(wrap-snip-fns downvotes-all-time-snip
               downvotes-snip
               upvotes-all-time-snip
               upvotes-snip
               users-who-snip)
(wrap-db-fns _select-stats
             delete-banned-username-string
             insert-banned-username-string
             insert-invite-code
             select-banned-username-strings
             select-invite-code-settings
             select-invite-codes
             update-invite-code-settings
             update-invite-codes-expire)

(defn where-timespan
  "Construct a snippet containing a where clause with a timespan.
  Either the since time or the until time may be omitted.  The
  `extra-clause` will be ANDed in with the timespan, if it is
  supplied."
  ([timespan var]
   (where-timespan timespan var nil))
  ([{:keys [since until]} var extra-clause]
   (when (or since until extra-clause)
     (sql/format
      {:where (into [:and] [extra-clause
                            (when since [:> var since])
                            (when until [:< var until])])}))))

(defn select-stats
  [db {:keys [since until users-who?]}]
  (let [timespan {:since (when since (java.sql.Timestamp. since))
                  :until (when until (java.sql.Timestamp. until))}
        where-snips
        {:user-where-timespan-snip (where-timespan timespan :joindate)
         :sub-where-timespan-snip (where-timespan timespan :creation)
         :post-where-timespan-snip (where-timespan timespan :posted)
         :comment-where-timespan-snip (where-timespan timespan :time)

         :upvote-where-timespan-snip
         (where-timespan timespan :datetime
                         [:raw "positive = 1"])

         :downvote-where-timespan-snip
         (where-timespan timespan :datetime
                         [:raw "positive = 0"])

         :comment-vote-where-timespan-snip
         (where-timespan timespan :datetime)

         :post-vote-where-timespan-snip
         (where-timespan timespan :datetime)}

        snips {:upvotes-snip (if (or since until)
                               (upvotes-snip where-snips)
                               (upvotes-all-time-snip))
               :downvotes-snip (if (or since until)
                                 (downvotes-snip where-snips)
                                 (downvotes-all-time-snip))
               :users-who-snip (when users-who?
                                 (users-who-snip where-snips))}]
    (_select-stats db (merge where-snips snips))))

(comment
  (sql/format {:where [:and [:raw "positive = 0"] nil]})
  (sql/format {:where [:and]})
  (sql/format nil)

  )
