;; elements/post/resolve.clj -- GraphQL resolvers for posts
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.post.resolve
  (:require
   [cambium.core :as log]
   [clojure.java.jdbc :as jdbc]
   [com.rpl.specter :as s]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [com.walmartlabs.lacinia.schema :as lacinia-schema]
   [promesa.core :as prom]
   [superlifter.api :refer [def-superfetcher] :as superlifter]
   [throat-be.elements.constants :as constants]
   [throat-be.elements.message.query :as message-query]
   [throat-be.elements.post.fixup :refer [fixup-post-fields]]
   [throat-be.elements.post.notify :as post-notify]
   [throat-be.elements.post.prepare :as prepare]
   [throat-be.elements.post.query :as query]
   [throat-be.elements.sub.prepare :as sub]
   [throat-be.elements.sub.query :as sub-query]
   [throat-be.elements.user.prepare :as user]
   [throat-be.protocols.bus :as bus]
   [throat-be.protocols.current-user :refer [get-uid is-admin? moderates?]]
   [throat-be.protocols.notify :as notify]
   [throat-be.protocols.schema :as schema]
   [throat-be.resolver-helpers.core :as args]
   [throat-be.resolver-helpers.data :as data]
   [throat-be.resolver-helpers.pagination :as pag]
   [throat-be.util :as util]))

;; Fetchers and Fixers

(defn load-posts-by-pid
  "Load posts for the superfetcher."
  [selectors {:keys [current-user db]}]
  (let [pids (map :pid selectors)
        or-maps (fn [maps]
                  (apply merge-with #(or %1 %2) maps))
        wants (or-maps selectors)
        params (merge wants
                      {:pids pids
                       :uid (get-uid current-user)})]
    (query/select-posts-by-pid db params)))

(def-superfetcher FetchPostByPid [id]
  (data/make-fetcher-fn :pid load-posts-by-pid fixup-post-fields))

(defn fetch-post
  "Promise to fetch a post by pid."
  [context select bucket]
  (let [id (data/id-from-fetch-args select)]
    (data/with-superlifter context
      (superlifter/enqueue! bucket (->FetchPostByPid id)))))

(defn fetch-posts
  [context args select]
  (let [postdata (query/list-posts (:db context) args select)]
    (if (nil? postdata)
      (resolve/with-error nil {:message "Not found"})
      (map #(fixup-post-fields % context) postdata))))

(defn fetch-default-posts
  [{:keys [db current-user] :as context} args]
  (let [uid (get-uid current-user)
        postdata (query/list-default-posts db args uid)]
    (if (nil? postdata)
      (resolve/with-error nil {:message "Not found"})
      (map #(fixup-post-fields % context) postdata))))

;;; Schema directive support

;; This has to go before redacting the deletion reason.
(defmethod schema/redact-content ::deleted-content
  [_ {:keys [current-user]} {:keys [status uid sid author_status] :as post}
   roles]
  (let [dissoc-all #(dissoc % :link :title :thumbnail :content ::content-history
                            ::poll_options :poll_open :poll_closes_time
                            :poll_votes :slug)
        dissoc-content #(dissoc % :link :content :thumbnail :poll_options
                                :poll_open :poll_closes_time :poll_votes
                                :slug)
        dissoc-some #(dissoc % :thumbnail :slug)
        post (if (= :DELETED author_status)
               (dissoc post :author_flair :distinguish)
               post)]
    (cond
      (= :ACTIVE status) post

      (and (= :DELETED_BY_USER status)
           (= :DELETED author_status)) (dissoc-all post)

      (and (= :DELETED_BY_USER status)
           (= uid (get-uid current-user))) (dissoc-content post)

      (and (not (roles :IS_ADMIN))
           (not (moderates? current-user sid))) (dissoc-all post)

      :else (dissoc-some post))))

(defmethod schema/redact-content ::deletion-reason
  [_ {:keys [current-user]} {:keys [status uid sid] :as postcomment}
   roles]
  (cond (or (= :ACTIVE status)
            (roles :IS_ADMIN)
            (moderates? current-user sid))
        postcomment

        (and (= uid (get-uid current-user))
             (#{:DELETED_BY_ADMIN :DELETED_BY_MOD} status))
        (assoc postcomment :status :DELETED_BY_MOD)

        (= uid (get-uid current-user))
        postcomment

        :else (assoc postcomment :status :DELETED)))

(defmethod schema/redact-content ::vote-breakdown
  [_ {:keys [site-config current-user]} {:keys [sid] :as postcomment} roles]
  (if (or (roles :IS_ADMIN)
          (moderates? current-user sid)
          (:show_votes (site-config)))
    postcomment
    (dissoc postcomment :upvotes :downvotes)))

;;; Field selection

(def ^:private fields-by-query-section
  "Lists of fields that trigger the need for parts of the post queries."
  {:info? [:Post/content
           :Post/status
           :Post/distinguish
           :Post/link
           :Post/nsfw
           :Post/posted
           :Post/edited
           :Post/type
           :Post/score
           :Post/upvotes
           :Post/downvotes
           :Post/thumbnail
           :Post/title
           :Post/comment_count
           :Post/author
           :Post/flair
           ;; is_archived and default_sort depend on :Post/posted
           :Post/is_archived
           :Post/default_sort
           :Post/comment_tree]
   :title-history? [:Post/title_history]
   :content-history? [:Post/content_history]
   :metadata? [:Post/default_sort
               :Post/comment_tree
               :Post/locked
               :Post/sticky_cid
               :Post/poll_open
               :Post/poll_closes
               :Post/poll_hide_results]
   :open-reports? [:Post/open_reports]
   :poll? [:Post/poll_options
           :Post/poll_votes]
   :poll-vote? [:Post/user_attributes]
   :saved? [:Post/user_attributes]
   :sticky? [:Post/is_archived
             :Post/comment_tree
             :Post/sticky]
   :viewed? [:Post/user_attributes]
   :vote? [:Post/user_attributes]})

(defn- wants
  "Determine which parts of the post query need to be included."
  [{:keys [site-config] :as context}]
  (let [show-history? (:edit_history (site-config))]
    (-> (s/transform [s/MAP-VALS] (fn [fields]
                                    (data/wants-fields? context fields))
                     fields-by-query-section)
        (update :title-history? #(and % show-history?))
        (update :content-history? #(and % show-history?)))))

;;; Resolvers

(def post-by-pid
  {:name ::post-by-pid
   :prepare
   [(args/convert-arg-to-int :pid)]

   :resolve
   (fn [context {:keys [pid]} _]
     (let [params (-> (wants context)
                      (assoc :pid pid))
           id (data/id-from-fetch-args params)]
       (data/with-superlifter context
         (-> (superlifter/enqueue! :immediate (->FetchPostByPid id))
             (data/update-trigger! context :Post/sub :sub-bucket
                                   data/inc-threshold)
             (data/update-trigger! context :Post/author :user-bucket
                                   data/inc-threshold)))))})

(defn post-by-reference
  [context _ {:keys [pid]}]
  (let [params (-> (wants context)
                   (assoc :pid pid))
        id (data/id-from-fetch-args params)]
    (data/with-superlifter context
      (-> (superlifter/enqueue! :post-bucket (->FetchPostByPid id))
          (data/update-trigger! context :Post/sub :sub-bucket
                                data/inc-threshold)
          (data/update-trigger! context :Post/author :user-bucket
                                data/inc-threshold)))))

(def posts-by-reference
  {:name ::posts-by-reference
   :prepare
   [(pag/verify-pagination-not-nested)
    (pag/verify-query-size :first)]

   :resolve
   (fn [context {:keys [first] :as args} {:keys [sid]}]
     (let [select {:sid sid}
           postdata (fetch-posts context (pag/one-more args) select)
           pagination-depth (list [:Post first])
           raise-threshold (fn [trigger-opts posts]
                             (update trigger-opts :threshold + (count posts)))]
       (data/with-superlifter context
         (-> (prom/promise postdata)
             (data/update-trigger! context :Post/sub :sub-bucket
                                   raise-threshold)
             (data/update-trigger! context :Post/author :user-bucket
                                   raise-threshold)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(def all-posts
  {:name ::all-posts
   :prepare
   [(pag/verify-pagination-not-nested)
    (pag/verify-query-size :first)]

   :resolve
   (fn [context {:keys [first] :as args} _]
     (let [postdata (fetch-posts context (pag/one-more args) nil)
           pagination-depth (list [:Post first])]
       (data/with-superlifter context
         (-> (prom/promise postdata)
             (data/update-trigger! context :Post/sub :sub-bucket
                                   data/raise-threshold-by-count)
             (data/update-trigger! context :Post/author :user-bucket
                                   data/raise-threshold-by-count)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(def default-posts
  {:name ::default-posts
   :prepare
   [(pag/verify-pagination-not-nested)
    (pag/verify-query-size :first)]

   :resolve
   (fn [context {:keys [first] :as args} _]
     (let [postdata (fetch-default-posts context (pag/one-more args))
           pagination-depth (list [:Post first])]
       (data/with-superlifter context
         (-> (prom/promise postdata)
             (data/update-trigger! context :Post/sub :sub-bucket
                                   data/raise-threshold-by-count)
             (data/update-trigger! context :Post/author :user-bucket
                                   data/raise-threshold-by-count)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(defn content-history-by-reference
  [_ _ resolved]
  (::content-history resolved))

(defn title-history-by-reference
  [context _ resolved]
  (data/with-superlifter context
    (-> (prom/promise (::title-history resolved))
        (data/update-trigger! context :TitleHistory/user :user-bucket
                              data/raise-threshold-by-count))))

(defn user-attributes-by-reference
  [_ _ resolved]
  (select-keys resolved [:is_saved :vote :poll_vote :viewed]))

(defn open-reports-by-reference
  "Resolve the open reports field of the :Post object.
  Only the :id and :rtype fields are implemented."
  [_ _ {:keys [::open_report_ids]}]
  (map (fn [id]
         (-> {:id id
              :rtype :POST}
             (lacinia-schema/tag-with-type :PostReport)))
       open_report_ids))

(defn poll-options-by-reference
  "Resolve the poll options field using data fetched by the outer resolver."
  [_ _ {:keys [::poll_options]}]
  poll_options)

;;; Streamers

(def stream-post-updates
  "Stream updates to a post, as received."
  {:name ::post-updates
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {})
             (args/verify-some [:post :pid] "Not found")]
   :stream
   (fn [{:keys [bus]} {:keys [pid]} source-stream-callback]
     (log/debug {:pid pid} "starting stream-post-updates")
     (let [callback #(do
                       (log/debug {:pid pid} "streaming post update")
                       (source-stream-callback %))
           sub (bus/subscribe bus ::post-update (str pid) callback)]
       #(bus/close-subscription bus sub)))})

(defn stream-announcement-post-id
  "Stream updates to the current site-wide announcement post."
  [{:keys [notify]} _ source-stream-callback]
  (log/debug {} "starting announcement post streamer")
  (let [add-handler (fn [{:keys [pid] :as payload}]
                      (log/debug {:payload payload}
                                 "Received announcement update message")
                      (source-stream-callback pid))
        rm-handler (fn [_]
                     (log/debug {} "Received delete announcement message")
                     (source-stream-callback ""))
        subs [(notify/subscribe-site notify :announcement add-handler)
              (notify/subscribe-site notify :rmannouncement rm-handler)]]
    (fn []
      (run! #(notify/close-subscription notify %) subs))))

;;; Mutations

(def save-post
  "Add or remove a post from the user's saved posts list."
  {:name ::save-post
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {})
             (args/verify-some [:post :pid] "Not found")]
   :resolve
   (fn [{:keys [current-user db]} {:keys [pid save]} _]
     (let [params {:uid (get-uid current-user)
                   :pid pid}]
       (if save
         (query/insert-user-saved db params)
         (query/delete-user-saved db params))
       (log/info {:pid pid :save save} "Saved or unsaved post")
       save))})

(def set-post-flair
  "Change a post's flair."
  {:name ::change-post-flair
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true})
             (args/verify-some [:post :pid] "Not found")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn :post #(= (:status %) :ACTIVE)))
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (user/verify-current-user-uid-match [:post :uid]))
             (args/verify-fn :post #(not (:is_archived %)) "Post archived")
             (sub/query-sub [:post :sid] :sub {:metadata? true})
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-user-can-set-flair :sub))
             (args/convert-arg-to-int :flair_id)
             (sub/query-post-flairs [:post :sid] :flair-choices)
             (prepare/verify-flair-id-match :flair_id :flair-choices :flair)
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn [:flair :mods_only] false? "Not authorized" 403))
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-flair-post-type :flair [:post :type]))]
   :resolve
   (fn [{:keys [db]} {:keys [pid flair]} _]
     (query/change-post-flair db {:pid pid
                                  :flair (:text flair)})
     (log/info {:pid pid :flair (:text flair)} "Set post flair")
     flair)})

(def remove-post-flair
  "Remove a post's flair."
  {:name ::remove-post-flair
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true})
             (args/verify-some [:post :pid] "Not found")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn :post #(= (:status %) :ACTIVE)))
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (user/verify-current-user-uid-match [:post :uid]))
             (args/verify-fn :post #(not (:is_archived %)) "Post archived")
             (sub/query-sub [:post :sid] :sub {:metadata? true})
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-user-can-delete-flair :sub))]
   :resolve
   (fn [{:keys [db]} {:keys [pid]} _]
     (query/change-post-flair db {:pid pid
                                  :flair nil})
     (log/info {:pid pid} "Removed post flair")
     pid)})

(defn- post-link
  "Create the URL for a post.
   TODO: a proper routing table."
  [{:keys [pid slug]} {:keys [name]}]
  (str "/o/" name "/" pid "/" slug))

(defn- sub-link
  "Create the URL for a sub.
   TODO: a proper routing table."
  [{:keys [name]}]
  (str "/o/" name))

(defn- self-update-post-title
  "Update a post's title by the author.
  Return a boolean, whether a change was actually made."
  [{:keys [db]} {:keys [post title]}]
  (let [slug (util/slugify title)
        updated? (-> (query/update-post-title db {:pid (:pid post)
                                                  :title title
                                                  :slug slug})
                     pos?)]
    updated?))

(defn- mod-update-post-title-notice
  [{:keys [tr]} post sub reason as-admin?]
  (let [markdown-post-link (tr "[your post](%s)" (post-link post sub))
        link (sub-link sub)
        markdown-sub-link (format "[%s](%s)" link link)]
    (if as-admin?
      (tr "The site administators changed the title of %s in %s. Reason: %s"
          markdown-post-link markdown-sub-link reason)
      (tr "The moderators of %s changed the title of %s. Reason: %s"
          markdown-sub-link markdown-post-link reason))))

(defn- message-user-re-mod-title-edit
  [{:keys [db current-user site-config tr] :as context}
   sub post reason as-admin?]
  (let [sid (if (and as-admin? (seq (:admin_sub (site-config))))
              (-> db
                  (sub-query/select-subs-by-name
                   {:uid (get-uid current-user)
                    :names [(:admin_sub (site-config))]})
                  first
                  :sid)
              (:sid post))
        mtype (:USER_NOTIFICATION constants/message-type-map)
        msg (-> db
                (message-query/insert-new-modmail-thread
                 {:content (mod-update-post-title-notice context post sub
                                                         reason as-admin?)
                  :subject (tr "Moderation action: post title changed")
                  :mtype mtype
                  :receivedby (:uid post)
                  :sentby (get-uid current-user)
                  :posted (util/sql-timestamp)
                  :sid sid}))]
    (log/info {:mtype mtype :mid (:mid msg) :mtid (:mtid msg)
               :received-by (:uid post) :as-admin as-admin?}
              "Post title edit notification sent")))

(defn- mod-update-post-title
  "Update a post's title by a mod or admin.
  Send a message to the author to notify them."
  [{:keys [db current-user] :as context} {:keys [post sub title reason]}]
  (jdbc/with-db-transaction [xds (:ds db)]
    (let [xdb (assoc db :ds xds)
          slug (util/slugify title)
          post-with-slug (assoc post :slug slug)
          as-admin? (boolean
                     (and (is-admin? current-user)
                          (not (moderates? current-user (:sid sub)))))
          updated? (-> xdb
                       (query/update-post-title-by-mod
                        {:pid (:pid post)
                         :uid (:uid post)
                         :mod-uid (get-uid current-user)
                         :title title
                         :slug slug
                         :reason reason
                         :link (post-link post-with-slug sub)
                         :is-admin as-admin?})
                       pos?)]
      (when updated?
        (message-user-re-mod-title-edit (assoc context :db xdb)
                                        sub (assoc post :slug slug)
                                        reason as-admin?))
      updated?)))

(def update-post-title
  "Replace a posts's title.
  Update the post's title edit history.  Return true if a change was
  made."
  {:name ::update-post-title
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true})
             (args/verify-some [:post :pid] "Not found")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (args/verify-fn :post #(= (:status %) :ACTIVE) "Post deleted"))
             (args/verify-fn :post #(not= (:status %) :USER_REMOVED,
                                          "Post deleted"))
             (args/verify-fn :post #(not (:is_archived %)) "Post archived")
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (user/verify-current-user-uid-match [:post :uid]))
             (sub/query-sub [:post :sid] :sub {:banned? true})
             (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)
             (sub/unless-user-is-mod-or-admin
              [:post :sid]
              (prepare/verify-title-edit-time-limit :post))
             (user/unless-current-user-uid-match
              [:post :uid]
              (args/verify-fn :reason #(> (count %) 3) "Reason required"))]

   :resolve
   (fn [{:keys [current-user] :as context} {:keys [post] :as args} _]
     (let [self? (= (get-uid current-user) (:uid post))
           updated? (if self?
                      (self-update-post-title context args)
                      (mod-update-post-title context args))]
       (log/info {:pid (:pid post) :updated updated?} "Edited post title")
       updated?))

   :notify
   [(fn [context {:keys [pid]} _ _]
      (post-notify/publish-post-update context _ _ {:pid pid}))
    (fn [{:keys [current-user notify] :as context} {:keys [post]} _ _]
      (when (not= (get-uid current-user) (:uid post))
        (doseq [event [:modmail-notification-counts
                       :recipient-notification-counts]]
          (notify/publish notify event context {:sid (:sid post)
                                                :receivedby (:uid post)}))))]})

(def update-post-content
  "Replace a posts's content.
  Update the post's content edit history and last edited timestamp.
  Return true if a change was made."
  {:name ::update-post-content
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (args/verify-fn :post #(= (:status %) :ACTIVE) "Post deleted")
             (args/verify-fn :post #(#{:TEXT :POLL} (:type %))
                             "Not a text post")
             (args/verify-fn :post #(not (:is_archived %)) "Post archived")
             (user/verify-current-user-uid-match [:post :uid])
             (sub/query-sub [:post :sid] :sub {:banned? true})
             (args/verify-fn :sub #(not (:banned %)) "Not authorized" 403)]
   :resolve
   (fn [{:keys [db]} {:keys [pid content]} _]
     (let [updated? (-> db
                        (query/update-post-content {:pid pid :content content})
                        pos?)]
       (log/info {:pid pid :updated updated?} "Edited post content")
       updated?))
   :notify
   [(fn [context {:keys [pid]} _ _]
      (post-notify/publish-post-update context _ _ {:pid pid}))]})

(def update-viewed
  "Record the time a post was viewed by the current user."
  {:name ::update-viewed
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (args/verify-fn :post #(not (:is_archived %)) "Post archived")]
   :resolve
   (fn [{:keys [current-user db]} {:keys [pid]} _]
     (query/update-viewed db {:pid pid
                              :uid (get-uid current-user)})
     (log/info {:pid pid} "Marked post viewed")
     nil)})

(def distinguish
  "Distinguish or un-distinguish a post.
   Return the new distinguish value."
  {:name ::distinguish
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (user/verify-current-user-uid-match [:post :uid])
             (args/verify= :sid [:post :sid])
             (args/verify-fn :post #(not (:is_archived %)) "Post archived")]
   :resolve
   (fn [{:keys [db current-user]} {:keys [pid sid distinguish]} _]
     (if (or (and (= distinguish :MOD) (not (moderates? current-user sid)))
             (and (= distinguish :ADMIN) (not (is-admin? current-user))))
       (resolve/with-error nil {:message "Not authorized" :status 403})
       (let [distinguish-val ((or distinguish :NORMAL)
                              constants/post-distinguish-map)]
         (query/update-distinguish db {:pid pid
                                       :distinguish distinguish-val})
         (log/info {:pid pid :distinguish distinguish}
                   "Set/cleared post distinguish")
         distinguish)))})

(def set-nsfw
  "Set a post's NSFW tag."
  {:name ::set-nsfw
   :prepare [(args/convert-arg-to-int :pid)
             (prepare/query-post :pid :post {:info? true
                                             :sticky? true})
             (args/verify-some [:post :pid] "Not found")
             (args/verify-fn :post #(not (:is_archived %)) "Post archived")]
   :resolve
   (fn [{:keys [db current-user]} {:keys [post nsfw]} _]
     (cond
       (not (or (= (get-uid current-user) (:uid post))
                (is-admin? current-user)
                (moderates? current-user (:sid post))))
       (resolve/with-error nil {:message "Not authorized" :status 403})

       (= nsfw (:nsfw post))
       nsfw

       :else
       (do
         (query/set-nsfw db {:pid (:pid post) :nsfw nsfw})
         (log/info {:pid (:pid post) :nsfw nsfw} "Set/cleared post NSFW")
         nsfw)))})

;; test-only mutations

(defn create-post
  "Create a post, for testing only."
  [{:keys [db current-user] :as context} {:keys [content title sid nsfw]} _]
  (let [post (-> (query/insert-test-post db {:content content
                                             :title title
                                             :slug (util/slugify title)
                                             :ptype 0
                                             :sid sid
                                             :nsfw nsfw
                                             :posted (util/sql-timestamp)
                                             :uid (get-uid current-user)})
                 first
                 (fixup-post-fields context))]
    (data/with-superlifter context
      (-> (prom/promise post)
          (data/update-trigger! context :Post/author :user-bucket
                                data/inc-threshold)
          (data/update-trigger! context :Post/sub :sub-bucket
                                data/inc-threshold)))))

(defn create-report
  "Create a report, for testing only."
  [{:keys [db current-user]} {:keys [pid reason]} _]
  (-> (query/insert-test-post-report db {:pid (util/id-from-string pid)
                                         :uid (get-uid current-user)
                                         :datetime (util/sql-timestamp)
                                         :reason reason
                                         :send-to-admin false})
      first
      (lacinia-schema/tag-with-type :PostReport)))
