;; elements/post/tasks.clj -- Task descriptions for throat-be posts
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.post.tasks
  (:require [cambium.core :as log]
            [throat-be.elements.post.query :refer [delete-old-comment-views]]))

(defn remove-old-comment-views-batch
  "Remove some old comment views, and return a map describing the work done."
  [db values]
  (try
    (delete-old-comment-views db values)
    (catch Exception e
      (log/error {:exception-message (.getMessage e)}
                 "remove old comment views task exception")
      {:deleted 0
       :remaining 0
       :completed_pid nil})))

(defn remove-old-comment-views
  "Remove old comment views."
  [{:keys [db options state]}]

  (let [db-with-counter (assoc db :counter (atom 0))]
    (when (nil? @state)
      (swap! state assoc :startpid -1))
    (loop [removed 0]
      (let [startpid (:startpid @state)
            rm-options (assoc options
                              :startpid startpid)
            {:keys [deleted remaining pid]}
            (remove-old-comment-views-batch db-with-counter rm-options)]
        ;; pid will be nil if no archived posts remain.  In that case,
        ;; leave the starting pid alone so we can look again later
        ;; for newly archived posts.  Otherwise, if there are no views
        ;; remaining to delete for posts at or before this pid, bump
        ;; up the startpid so we don't check them again.
        (when (and pid (zero? remaining))
          (swap! state assoc :startpid pid))

        ;; If we looked at a group of posts and found no views to delete,
        ;; continue looking at posts until some views are deleted or until we
        ;; run out of archived posts.
        (let [sum (+ removed deleted)]
          (if (and pid (pos? deleted) (pos? remaining)
                   (< sum (:batch-limit options)))
            (recur sum)
            (log/info {:next-pid (:startpid @state)
                       :remaining remaining
                       :deleted sum}
                      "Ran old comment view cleanup")))))))
