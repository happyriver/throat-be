;; elements/post/fixup.clj -- Make post SQL results match schema expectations
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.post.fixup
  (:require
   [clojure.set :as set]
   [clojure.string :as str]
   [throat-be.elements.constants :as constants]
   [throat-be.util :as util]))

(def day-ms (* 24 60 60 1000))

(defn- fixup-distinguish
  "Return nil for no distinguishing, or :MOD or :ADMIN."
  [val]
  (let [distinguish (constants/reverse-post-distinguish-map val)]
    (when (not= distinguish :NORMAL) distinguish)))

(defn- set-enum-fields
  "Convert integers to enums in a post object."
  [{:keys [ptype] :as post}]
  (-> post
      (dissoc :ptype)
      (assoc :type (constants/reverse-post-type-map ptype))
      (update :deleted constants/reverse-post-deleted-map)
      (update :distinguish fixup-distinguish)
      (update :author_status constants/reverse-user-status-map)))

(defn- set-meta-fields
  "Set post fields from the metadata section of the query. "
  [{:keys [meta] :as post}]
  (-> post
      (dissoc :meta)
      (merge meta)
      (assoc :locked (:lock-comments meta))
      (update :sort #(some-> %
                             str/upper-case
                             keyword))
      (util/convert-fields-to-boolean [:hide_results
                                       :locked
                                       :poll_closed])))

(defn- set-poll-fields
  "Set fields of poll posts to conform to the schema."
  [{:keys [type poll_closed poll_closes_time poll_options
           hide_results] :as post}]
  (if-not (= type :POLL)
    post
    (let [closes (when poll_closes_time
                   (-> (parse-long poll_closes_time)
                       (* 1000)))
          now (-> (java.util.Date.)
                  .getTime)
          open? (and (not poll_closed) (or (nil? closes)
                                           (> closes now)))
          options (if (and hide_results open?)
                    (map #(dissoc % :votes) poll_options)
                    poll_options)]
      (-> post
          (assoc :poll_open open?)
          (assoc :poll_closes_time (when closes (str closes)))
          (assoc :throat-be.elements.post.resolve/poll_options options)
          (dissoc :poll_closed :poll_options)))))

(defn- set-status
  "Set the status field based on the post's visibility."
  [{:keys [deleted] :as post}]
  (assoc post :status (deleted {:NOT_DELETED   :ACTIVE
                                :USER_REMOVED  :DELETED_BY_USER
                                :MOD_REMOVED   :DELETED_BY_MOD
                                :ADMIN_REMOVED :DELETED_BY_ADMIN
                                :ADMIN_DELETED :DELETED_BY_ADMIN})))

(defn- set-is-archived
  "Set the is_archived field depending on the age of the post."
  [{:keys [site-config]} {:keys [pid posted sticky] :as post}]
  (let [{:keys [archive_post_after archive_sticky_posts
                announcement_pid]} (site-config)]
    (assoc post :is_archived
           (and posted
                (not= (str pid) announcement_pid)
                (or archive_sticky_posts (not sticky))
                (> (- (.getTime (java.util.Date.))
                      (.getTime posted))
                   (* day-ms archive_post_after))))))

(defn- set-comment-sort
  "Set the fields describing default comment sort."
  [{:keys [site-config]} {:keys [posted sort] :as post}]
  (if (nil? posted)
    post
    (let [{:keys [best_comment_sort_init]} (site-config)
          best-sort-enabled (> (.getTime posted)
                               best_comment_sort_init)
          sort-option (or sort :BEST)
          sort-checked (if (or best-sort-enabled
                               (not= sort-option :BEST))
                         sort-option
                         :TOP)]
      (assoc post
             :best_sort_enabled best-sort-enabled
             :default_sort sort-checked))))

(defn- add-thumbnail-url
  "Add the URL prefix to the thumbnail field if it is set."
  [{:keys [site-config]} {:keys [thumbnail] :as post}]
  (if (seq thumbnail)
    (update post :thumbnail #(str (:thumbnail_url (site-config)) "/" %))
    post))

(defn- keywordize-vote
  [post]
  (update post :vote constants/reverse-vote-value-map))

(defn fixup-post-fields
  "Make the database post fields match the query post fields."
  [post context]
  (->> (set/rename-keys post {:content_history
                              :throat-be.elements.post.resolve/content-history

                              :title_history
                              :throat-be.elements.post.resolve/title-history

                              :open_report_ids
                              :throat-be.elements.post.resolve/open_report_ids})
       set-enum-fields
       set-meta-fields
       set-poll-fields
       set-status
       (set-is-archived context)
       (set-comment-sort context)
       (add-thumbnail-url context)
       keywordize-vote))
