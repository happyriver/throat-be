;; elements/post/prepare.clj -- Prepare post arguments for resolver execution
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.post.prepare
  (:require
   [throat-be.elements.constants :as constants]
   [throat-be.elements.post.fixup :refer [fixup-post-fields]]
   [throat-be.elements.post.query :as query]
   [throat-be.protocols.current-user :refer [get-uid user]]
   [throat-be.resolver-helpers.core :as args :refer [assoc-in*
                                                     get-in*
                                                     fail-verification]]
   [throat-be.util :as util]))

(defn query-post
  "Fetch a post object and add it to `args` at `post-path`."
  [pid-path post-path options]
  {:name ::query-post
   :func
   (fn [{:keys [current-user db] :as context} args]
     (let [post-id (get-in* args pid-path)
           params (merge {:pids [post-id]
                          :uid (get-uid current-user)} options)
           post (some-> (query/select-posts-by-pid db params)
                        first
                        (fixup-post-fields context))]
       (assoc-in* args post-path post)))})

(defn find-mentions
  "Find user mentions in the content and attach their names at `mention-path`."
  [content-path mention-path]
  {:name ::find-mentions
   :func
   (fn [{:keys [current-user]} args]
     (let [level (:level (user current-user))
           allowed-num (cond
                         (> level 30) 15
                         (> level 20) 10
                         :else        5)]
       (->> (get-in* args content-path)
            util/mentions
            (take allowed-num)
            (assoc-in* args mention-path))))})

(defn verify-user-can-set-flair
  "Ensure that users are allowed to add flair in the sub."
  [sub-path]
  {:name ::verify-user-can-set-flair
   :func
   (fn [_ args]
     (let [sub (get-in* args sub-path)
           {user-must-flair? :user_must_flair
            user-can-flair? :user_can_flair} sub]
       (if (or user-must-flair? user-can-flair?)
         args
         (fail-verification args "Not authorized" 403))))})

(defn verify-user-can-delete-flair
  "Verify that users are allowed to remove flair in the sub."
  [sub-path]
  {:name ::verify-can-delete-flair
   :func
   (fn [_ args]
     (let [sub (get-in* args sub-path)]
       (if (:user_must_flair sub)
         (fail-verification args "Not authorized" 403)
         args)))})

(defn verify-flair-id-match
  "Verify that the flair id exists in the list of flairs.
  If found, add it to `args` at `flair-path`."
  [flair-id-path flair-choices-path flair-path]
  {:name ::verify-flair-id-match
   :func
   (fn [_ args]
     (let [flair-id (get-in* args flair-id-path)
           flair (->> (get-in* args flair-choices-path)
                      (filter #(= flair-id (:id %)))
                      first)]
       (if flair
         (assoc-in* args flair-path flair)
         (fail-verification args "Not found"))))})

(defn verify-flair-post-type
  "Verify that the post type is permitted for the flair."
  [flair-path post-type-path]
  {:name ::verify-flair-post-type
   :func
   (fn [_ args]
     (let [disallowed? (->> (get-in* args flair-path)
                            :disallowed_ptypes
                            (map constants/reverse-post-type-map)
                            set)
           type (get-in* args post-type-path)]
       (if (disallowed? type)
         (fail-verification args "Not permitted")
         args)))})

(defn verify-title-edit-time-limit
  "Fail verification if outside the configured title edit time limit."
  [post-path]
  {:name ::verify-title-edit-time-limit
   :func
   (fn [{:keys [site-config]} args]
     (let [posted (-> (get-in* args post-path) :posted)
           age-ms (- (.getTime (java.util.Date.))
                     (.getTime posted))]
       (if (> age-ms (* (:title_edit_timeout (site-config)) 1000))
         (fail-verification args "Time expired")
         args)))})
