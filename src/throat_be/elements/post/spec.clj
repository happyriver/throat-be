;; elements/post/spec.clj -- Specs for posts
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.post.spec
  "Specs for post SQL query arguments and fields."
  (:require
   [clojure.spec.alpha :as spec]
   [throat-be.elements.comment.spec :as comment-spec]
   [throat-be.elements.constants :as constants]
   [throat-be.elements.spec :as element-spec]
   [throat-be.elements.user.spec :as user-spec]))

(spec/def ::pid int?)
(spec/def ::content string?)
(spec/def :throat-be.elements.post.spec.content_history/content
  ::content)
(spec/def :throat-be.elements.post.spec.content_history/time
  ::element-spec/timestamp)
(spec/def ::content_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:throat-be.elements.post.spec.content_history/content
                        :throat-be.elements.post.spec.content_history/time]))))
(spec/def ::comment_count nat-int?)
(spec/def ::deleted (-> constants/post-deleted-map vals set))
(spec/def ::distinguish
  (spec/nilable (-> constants/post-distinguish-map vals set)))
(spec/def ::downvotes int?)
(spec/def ::edited (spec/nilable ::element-spec/timestamp))
(spec/def ::title-edited (spec/nilable ::element-spec/timestamp))
(spec/def ::flair (spec/nilable string?))
(spec/def ::link (spec/nilable string?))
(spec/def :throat-be.elements.post.spec.meta/hide_results #{"1" "0"})
(spec/def :throat-be.elements.post.spec.meta/lock-comments #{"1" "0"})
(spec/def :throat-be.elements.post.spec.meta/poll_closed #{"1" "0"})
(spec/def :throat-be.elements.post.spec.meta/poll_closes_time string?)
(spec/def :throat-be.elements.post.spec.meta/sort #{"best" "new" "top"})
(spec/def :throat-be.elements.post.spec.meta/sticky_cid ::comment-spec/cid)
(spec/def ::meta
  (spec/nilable
   (spec/keys :opt-un [:throat-be.elements.post.spec.meta/hide_results
                       :throat-be.elements.post.spec.meta/lock-comments
                       :throat-be.elements.post.spec.meta/poll_closed
                       :throat-be.elements.post.spec.meta/poll_closes_time
                       :throat-be.elements.post.spec.meta/sort
                       :throat-be.elements.post.spec.meta/sticky_cid])))
(spec/def ::nsfw boolean?)
(spec/def ::pids (spec/coll-of ::pid))
(spec/def ::posted ::element-spec/timestamp)
(spec/def ::ptype (-> constants/post-type-map vals set))
(spec/def ::saved boolean?)
(spec/def ::score int?)
(spec/def ::slug string?)
(spec/def ::sticky boolean?)
(spec/def ::thumbnail (spec/nilable string?))
(spec/def ::title string?)
(spec/def :throat-be.elements.post.spec.title_history/content ::title)
(spec/def :throat-be.elements.post.spec.title_history/time
  ::element-spec/timestamp)
(spec/def ::title_history
  (spec/nilable
   (spec/coll-of
    (spec/keys :req-un [:throat-be.elements.post.spec.title_history/content
                        ::user-spec/uid
                        :throat-be.elements.post.spec.title_history/time]))))
(spec/def ::upvotes int?)
(spec/def ::viewed (spec/nilable ::element-spec/timestamp))
(spec/def ::vote (spec/nilable (-> constants/vote-value-map vals set)))

(spec/def ::report-id int?)
