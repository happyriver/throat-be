;; elements/post/notify.clj -- Post notification support for throat-be
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.post.notify
  (:require
   [throat-be.elements.post.fixup :refer [fixup-post-fields]]
   [throat-be.elements.post.query :as query]
   [throat-be.protocols.bus :as bus]))

(defn publish-post-update
  "Fetch a post from the db and publish it."
  [{:keys [bus db] :as context} _ _ {:keys [pid]}]
  (let [post (-> (query/select-posts-by-pid db {:pids [pid]
                                                :info? true
                                                :metadata? true
                                                :sticky? true})
                 first
                 (fixup-post-fields context))]
    (bus/publish bus :throat-be.elements.post.resolve/post-update (str pid) post)))
