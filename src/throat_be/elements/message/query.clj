;; elements/message/select.clj -- SQL queries for Message data
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.message.query
  (:require
   [clojure.spec.alpha :as spec]
   [hugsql.core :as hugsql]
   [throat-be.elements.constants :refer [wrap-db-fn wrap-snip-fn] :as constants]
   [throat-be.elements.message.spec :as message-spec]
   [throat-be.elements.report.spec :as report-spec]
   [throat-be.elements.spec :as element-spec]
   [throat-be.elements.spec.db :as db-spec]
   [throat-be.elements.sub.spec :as sub-spec]
   [throat-be.elements.user.spec :as user-spec])
  (:require
   [throat-be.elements.message.spec.change-mailbox :as-alias change-mailbox]
   [throat-be.elements.message.spec.insert-contact-mods-thread-returning
    :as-alias insert-contact-mods-thread-returning]
   [throat-be.elements.message.spec.insert-message-thread-returning
    :as-alias insert-message-thread-returning]
   [throat-be.elements.message.spec.insert-modmail-reply-returning
    :as-alias insert-modmail-reply-returning]
   [throat-be.elements.message.spec.insert-new-modmail-thread-returning
    :as-alias insert-new-modmail-thread-returning]
   [throat-be.elements.message.spec.modmail-message :as-alias modmail-message]
   [throat-be.elements.message.spec.nilable :as-alias nilable]
   [throat-be.elements.message.spec.select-mods-with-notification-counts
    :as-alias select-mods-with-notification-counts]))

(hugsql/def-db-fns "sql/messages.sql")

(spec/def ::latest_posted ::message-spec/posted)
(spec/def ::report-id ::report-spec/id)
(spec/def ::target_uid (spec/nilable ::user-spec/uid))
(spec/def ::unread (spec/nilable ::user-spec/uid))
(spec/def ::nilable/receivedby (spec/nilable ::message-spec/receivedby))
(spec/def ::nilable/sid (spec/nilable ::sub-spec/sid))

(spec/def ::modmail-message/mtype
  (-> constants/message-type-map
      ((juxt :MOD_DISCUSSION :MOD_TO_USER_AS_MOD :MOD_TO_USER_AS_USER
             :USER_NOTIFICATION :USER_TO_MODS))
      set))
(spec/def ::modmail-message/message
  (spec/keys :req-un [::message-spec/mid
                      ::message-spec/content
                      ::modmail-message/mtype
                      ::message-spec/sentby
                      ::nilable/receivedby
                      ::message-spec/first
                      ::message-spec/mtid
                      ::unread]))
(spec/def ::first_message ::modmail-message/message)
(spec/def ::latest_message ::modmail-message/message)

(wrap-snip-fn create-report-logs-snip)
(db-spec/fdef create-report-logs-snip
  :args (spec/cat :args
                  (spec/keys :req-un [::report-spec/report-id
                                      ::message-spec/receivedby
                                      ::report-spec/report-type]))
  :ret ::db-spec/sqlvec)

(spec/def ::change-mailbox/updated ::element-spec/timestamp)
(wrap-db-fn change-mailbox)
(db-spec/fdef change-mailbox
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::message-spec/mtid
                                  ::message-spec/mailbox
                                  ::user-spec/uid
                                  ::change-mailbox/updated]))
  :ret int?)

(spec/def ::insert-contact-mods-thread-returning/receivedby
  nil?)
(spec/def ::insert-contact-mods-thread-returning/mtype
  #(= (:USER_TO_MODS constants/message-type-map) %))
(wrap-db-fn insert-contact-mods-thread-returning)
(db-spec/fdef insert-contact-mods-thread-returning
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys
                :req-un [::message-spec/subject
                         ::message-spec/content
                         ::sub-spec/sid
                         ::message-spec/posted
                         ::insert-contact-mods-thread-returning/receivedby
                         ::message-spec/sentby]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::message-spec/posted
                            ::insert-contact-mods-thread-returning/mtype
                            ::insert-contact-mods-thread-returning/receivedby
                            ::message-spec/sentby
                            ::sub-spec/sid
                            ::message-spec/mtid])))

(spec/def ::insert-message-thread-returning/mtype
  #(= (:USER_TO_USER constants/message-type-map) %))
(spec/def ::insert-message-thread-returning/sid nil?)
(wrap-db-fn insert-message-thread-returning)
(db-spec/fdef insert-message-thread-returning
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys
                         :req-un [::message-spec/subject
                                  ::message-spec/content
                                  ::message-spec/posted
                                  ::message-spec/sentby
                                  ::message-spec/receivedby]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::message-spec/posted
                            ::insert-message-thread-returning/mtype
                            ::insert-message-thread-returning/sid
                            ::message-spec/sentby
                            ::message-spec/receivedby
                            ::message-spec/mtid])))

(spec/def ::insert-modmail-reply-returning/mtype
  (-> constants/message-type-map
      ((juxt :MOD_DISCUSSION :MOD_TO_USER_AS_MOD :MOD_TO_USER_AS_USER))
      set))
(wrap-db-fn insert-modmail-reply-returning)
(db-spec/fdef insert-modmail-reply-returning
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un [::message-spec/mtid
                                   ::message-spec/content
                                   ::message-spec/posted
                                   ::insert-modmail-reply-returning/mtype
                                   ::sub-spec/sid
                                   ::message-spec/sentby
                                   ::nilable/receivedby
                                   ::message-spec/mtid]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::message-spec/posted
                            ::insert-modmail-reply-returning/mtype
                            ::sub-spec/sid
                            ::message-spec/sentby
                            ::nilable/receivedby
                            ::message-spec/mtid])))

(spec/def ::insert-new-modmail-thread-returning/mtype
  (-> constants/message-type-map
      ((juxt :MOD_DISCUSSION :MOD_TO_USER_AS_MOD :MOD_TO_USER_AS_USER
             :USER_NOTIFICATION))
      set))
(spec/def ::insert-new-modmail-thread-returning/report-snip
  (spec/nilable ::db-spec/sqlvec))
(wrap-db-fn insert-new-modmail-thread-returning)
(db-spec/fdef insert-new-modmail-thread-returning
  :args (spec/cat
         :db ::db-spec/db
         :args (spec/keys :req-un
                          [::message-spec/subject
                           ::message-spec/content
                           ::sub-spec/sid
                           ::message-spec/posted
                           ::insert-new-modmail-thread-returning/mtype
                           ::message-spec/sentby
                           ::nilable/receivedby]))
  :ret (spec/coll-of
        (spec/keys :req-un [::message-spec/mid
                            ::message-spec/content
                            ::message-spec/posted
                            ::insert-new-modmail-thread-returning/mtype
                            ::sub-spec/sid
                            ::message-spec/sentby
                            ::nilable/receivedby
                            ::message-spec/mtid])))

(wrap-db-fn make-message-read)
(db-spec/fdef make-message-read
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn make-message-unread)
(db-spec/fdef make-message-unread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn make-thread-read)
(db-spec/fdef make-thread-read
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn make-thread-unread)
(db-spec/fdef make-thread-unread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret int?)

(wrap-db-fn select-comment-report)
(db-spec/fdef select-comment-report
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret (spec/keys :req-un [::report-id
                           ::sub-spec/sid
                           ::user-spec/name]))

(wrap-db-fn select-in-progress-modmail-threads)
(db-spec/fdef select-in-progress-modmail-threads
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::user-spec/uid
                                            ::sub-spec/sids]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::message-spec/mtid
                    ::message-spec/replies
                    ::message-spec/subject
                    ::sub-spec/sid
                    ::message-spec/mailbox
                    ::message-spec/posted
                    ::latest_posted
                    ::first_message
                    ::latest_message])))

(wrap-db-fn select-message-thread)
(db-spec/fdef select-message-thread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid]))
  :ret (spec/keys :req-un
                  [::message-spec/mtid
                   ::message-spec/replies
                   ::message-spec/subject
                   ::nilable/sid
                   ::message-spec/mtype
                   ::target_uid]))

(wrap-db-fn select-messages-by-mid)
(db-spec/fdef select-messages-by-mid
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mids]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::message-spec/mid
                    ::message-spec/content
                    ::message-spec/mtype
                    ::message-spec/posted
                    ::nilable/receivedby
                    ::message-spec/sentby
                    ::message-spec/first
                    ::message-spec/mtid
                    ::nilable/sid])))

(wrap-db-fn select-messages-in-thread)
(db-spec/fdef select-messages-in-thread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid
                                            ::element-spec/limit]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::message-spec/mid
                    ::message-spec/content
                    ::message-spec/mtype
                    ::message-spec/posted
                    ::message-spec/sentby
                    ::nilable/receivedby
                    ::message-spec/first
                    ::message-spec/mtid
                    ::nilable/sid
                    ::unread])))

(wrap-db-fn select-modmail-thread)
(db-spec/fdef select-modmail-thread
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtid
                                            ::user-spec/uid]))
  :ret (spec/keys :req-un
                  [::message-spec/mtid
                   ::message-spec/replies
                   ::message-spec/subject
                   ::sub-spec/sid
                   ::target_uid
                   ::message-spec/mailbox
                   ::message-spec/posted
                   ::latest_posted
                   ::first_message
                   ::latest_message]))

(wrap-db-fn select-modmail-threads)
(db-spec/fdef select-modmail-threads
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::message-spec/mtypes
                                            ::sub-spec/sids
                                            ::user-spec/uid
                                            ::message-spec/mailbox
                                            ::element-spec/limit]))
  :ret (spec/coll-of (spec/keys :req-un
                                [::message-spec/mtid
                                 ::message-spec/replies
                                 ::message-spec/subject
                                 ::sub-spec/sid
                                 ::message-spec/posted
                                 ::latest_posted
                                 ::first_message
                                 ::latest_message])))

(spec/def ::select-mods-with-notification-counts/messages int?)
(spec/def ::select-mods-with-notification-counts/notifications int?)
(spec/def ::select-mods-with-notification-counts/modmails int?)
(wrap-db-fn select-mods-with-notification-counts)
(db-spec/fdef select-mods-with-notification-counts
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::sub-spec/sid]))
  :ret (spec/coll-of
        (spec/keys :req-un
                   [::user-spec/uid
                    ::select-mods-with-notification-counts/messages
                    ::select-mods-with-notification-counts/notifications
                    ::select-mods-with-notification-counts/modmails])))

(wrap-db-fn select-new-modmail-threads)
(db-spec/fdef select-new-modmail-threads
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::sub-spec/sids
                                            ::user-spec/uid
                                            ::element-spec/limit]))
  :ret (spec/coll-of (spec/keys :req-un
                                [::message-spec/mtid
                                 ::message-spec/replies
                                 ::message-spec/subject
                                 ::sub-spec/sid
                                 ::message-spec/posted
                                 ::latest_posted
                                 ::first_message
                                 ::latest_message
                                 ::message-spec/mailbox]))  )

(wrap-db-fn select-post-report)
(db-spec/fdef select-post-report
  :args (spec/cat :db ::db-spec/db
                  :args (spec/keys :req-un [::report-spec/report_id
                                            ::sub-spec/sid
                                            ::user-spec/name]))
  :ret (spec/coll-of
        (spec/keys :req-un [::report-spec/id])))

(defn find-messages-by-mid
  "Find messages by their mids."
  ;; This doesn't fetch the unread field.
  [db mids]
  (select-messages-by-mid db {:mids mids}))

(defn- fixup-mod-thread-timestamps
  "Fix the message timestamps for a mod thread query.
  The timestamps in first_message and last_message are strings due to
  the JSON conversion they go through.  Replace them with the SQL
  timestamps which were also fetched."
  [{:keys [posted latest_posted] :as thread}]
  (when thread
    (-> thread
        (assoc-in [:first_message :posted] posted)
        (assoc-in [:latest_message :posted] latest_posted)
        (dissoc :posted)
        (dissoc :latest_posted))))

(defn fixup-report-timestamps
  "Fix the report timestamps for a mod thread query."
  [{:keys [post_report_datetime comment_report_datetime post_report
           comment_report] :as thread}]
  (cond-> thread
    post_report (assoc-in [:post_report :datetime] post_report_datetime)
    comment_report (assoc-in [:comment_report :datetime]
                             comment_report_datetime)))

(defn get-mod-thread-by-id
  "Get a modmail thread by the thread id."
  [db {:keys [uid thread_id]}]
  (-> (select-modmail-thread db {:uid uid :mtid thread_id})
      fixup-mod-thread-timestamps
      fixup-report-timestamps))

(defn list-mod-threads
  "Return a list of modmail threads ordered by their most recent message."
  [db {:keys [first after uid sids mtypes mailbox unread_only]}]
  (let [params {:sids sids
                :uid uid
                :limit first
                :unread_only unread_only
                :mailbox mailbox ; INBOX or ARCHIVED
                :mtypes mtypes
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (map fixup-mod-thread-timestamps (select-modmail-threads db params))))

(defn list-new-mod-threads
  "Return a list of threads started by users which don't have a reply yet."
  [db {:keys [first after uid sids unread_only]}]
  (let [params {:sids sids
                :uid uid
                :limit first
                :unread_only unread_only
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (->> (select-new-modmail-threads db params)
         (map fixup-mod-thread-timestamps))))

(defn list-in-progress-mod-threads
  "Return threads from users to mods with replies and from mods to users. "
  [db {:keys [first after uid sids unread_only]}]
  (let [params {:sids sids
                :uid uid
                :limit first
                :unread_only unread_only
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (map fixup-mod-thread-timestamps
         (select-in-progress-modmail-threads db params))))

(defn list-messages-in-thread
  "Return a list of the messages in a conversation.
  The mid passed should be that of the first message in the conversation."
  [db {:keys [first after uid mtid]}]
  (let [params {:uid uid
                :mtid mtid
                :limit first
                ;; Because we're sorting from newest to oldest.
                :before after}]
    (select-messages-in-thread db params)))

(defn insert-new-modmail-thread
  "Create a new modmail message and thread and other associated records."
  [db params]
  (let [report-snip (when (:report-type params)
                      (create-report-logs-snip params))]
    (->> (assoc params :report-snip report-snip)
         (insert-new-modmail-thread-returning db))))

(comment
  (require 'user)
  (def db (:db user/system))
  (select-message-thread db {:mtid 55}))
