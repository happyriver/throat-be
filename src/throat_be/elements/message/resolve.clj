;; elements/message/resolve.clj -- GraphQL resolvers for Message data
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.message.resolve
  "Contains custom resolvers for Messages."
  (:require
   [cambium.core :as log]
   [clojure.set :as set]
   [clojure.string :as str]
   [com.walmartlabs.lacinia.resolve :as resolve]
   [com.walmartlabs.lacinia.schema :as lacinia-schema]
   [promesa.core :as prom]
   [superlifter.api :refer [def-superfetcher] :as superlifter]
   [throat-be.elements.constants :as constants]
   [throat-be.elements.message.prepare :as prepare]
   [throat-be.elements.message.query :as query]
   [throat-be.elements.sub.prepare :as sub-prepare]
   [throat-be.elements.user.prepare :as user-prepare]
   [throat-be.elements.user.query :as user]
   [throat-be.protocols.current-user :refer [is-admin? get-uid get-name
                                             moderates? subs-moderated]]
   [throat-be.protocols.notify :as notify]
   [throat-be.protocols.schema :as schema]
   [throat-be.resolver-helpers.core :as args :refer [assoc-in*
                                                     fail-verification]]
   [throat-be.resolver-helpers.data :as data]
   [throat-be.resolver-helpers.pagination :as pag]
   [throat-be.util :as util]))

;;; Fetchers and Fixers

(defn- sender-or-recipient?
  [{:keys [current-user]} {:keys [mtype sid sentby receivedby]}]
  ;; Must be logged in to see a message.
  (when-let [uid (get-uid current-user)]
    (or
     ;; Admins can see any message.
     (is-admin? current-user)

     ;; Senders can see messages they sent.
     (= uid sentby)

     ;; People can see messages sent to them.
     (= uid receivedby)

     ;; Sub mods can see any mod message to or from a sub they mod.
     (and sid (moderates? current-user sid)
          (#{:USER_TO_MODS :MOD_TO_USER_AS_MOD :MOD_TO_USER_AS_USER
             :MOD_DISCUSSION :USER_BAN_APPEAL :USER_NOTIFICATION
             :MOD_NOTIFICATION}
           (get constants/reverse-message-type-map mtype))))))

(defn- assoc-sender-uid-if-sender-visible
  "Add the uid of the sender to the message if visible to the current
  user.  Put the sender's uid where :User/fetch-message-sender will
  look for it."
  [{:keys [sid sentby mtype] :as message} {:keys [current-user]}]
  (cond-> message
    (or
     ;; Admins can always see the sender.
     (is-admin? current-user)

     ;; Moderators can see the senders of modmails.
     (and sid (moderates? current-user sid))

     ;; You can see the sender of the messages you send.
     (= sentby (get-uid current-user))

     ;; Some mtypes always allow the receiver to see the sender.
     (#{:USER_TO_USER :USER_TO_MODS :MOD_TO_USER_AS_USER
        :MOD_DISCUSSION :USER_BAN_APPEAL :MOD_NOTIFICATION} mtype))
    (assoc :sender-uid sentby)))

(defn- fixup-message-fields
  "Make a Message row from the database match the schema.
  Also filter out anything the current user is not allowed to see."
  [{:keys [mtype posted receivedby unread]
    :as message} env]
  (if-not (sender-or-recipient? env message)
    (resolve/with-error nil {:message "Not authorized." :status 403})
    (let [message-type (get constants/reverse-message-type-map mtype)]
      (-> message
          (assoc
           :mtype message-type
           :time posted
           :cursor (pag/make-cursor (:posted message))
           ;; Put the receiver's uid where :User/fetch-by-reference
           ;; will look for it.
           :uid receivedby
           ;; The query gives a uid for unread messages and nil for read ones.
           ;; Convert to a boolean.
           :unread (some? unread))
          (assoc-sender-uid-if-sender-visible env)))))

(defn- fixup-thread-message-fields
  "Fix the messages in a thread object to match the schema."
  [context sid {:keys [mtype posted receivedby unread] :as message}]
  (let [message-type (get constants/reverse-message-type-map mtype)]
    (-> message
        (assoc
         :mtype message-type
         :time posted
         :uid receivedby
         :unread (some? unread)
         :sid sid)
        (assoc-sender-uid-if-sender-visible context))))

(defn- fixup-thread-fields
  "Make the result of one of the thread queries match the schema.
  Also filter out anything the current user is not allowed to see."
  [context {:keys [mtid first_message latest_message sid replies mailbox]
            :as thread}]
  (if-not (sender-or-recipient? context (assoc first_message :sid sid))
    (resolve/with-error nil {:message "Not authorized." :status 403})
    (-> thread
        (assoc
         :id (pag/encode-id mtid :MessageThread)
         :mailbox (get constants/reverse-message-mailbox-map mailbox)
         :cursor (pag/make-cursor (:posted latest_message))
         :reply_count replies)
        (set/rename-keys {:first_message ::first-message
                          :latest_message ::latest-message}))))

(defn load-messages-by-mid
  "Load messages for the message superfetcher."
  [selectors {:keys [db]}]
  (query/find-messages-by-mid db (map :mid selectors)))

(def-superfetcher FetchMessageByMid [id]
  (data/make-fetcher-fn :mid load-messages-by-mid fixup-message-fields))

(defn fetch-message
  "Promise to fetch a message by mid."
  [context select bucket]
  (let [mid (util/id-from-string (:mid select))
        id (data/id-from-fetch-args {:mid mid})]
    (data/with-superlifter context
      (superlifter/enqueue! bucket (->FetchMessageByMid id)))))

;;; Directive support

(defmethod schema/update-auth-info ::fetch-message
  [_ {:keys [auth-info db] :as context} {:keys [mid]} _]
  (if (:message auth-info)
    context
    (let [mids [(util/id-from-string mid)]
          message (when mid
                    (->
                     (query/find-messages-by-mid db mids)
                     first))]
      (if message
        (assoc-in context [:auth-info :message] message)
        context))))

(defmethod schema/update-auth-info ::fetch-message-thread
  [_ {:keys [auth-info db] :as context} {:keys [thread_id]} _]
  (if (:message-thread auth-info)
    context
    (let [mtid (pag/decode-id thread_id :MessageThread)
          thread (when mtid
                   (query/select-message-thread db {:mtid mtid}))]
      (if thread
        (assoc-in context [:auth-info :message-thread] thread)
        context))))

;; Set the :SENDER role if the user is authenticated.
(defmethod schema/set-roles ::sender
  [_ {:keys [current-user] :as context}]
  (update context :roles set/union
          (when (get-uid current-user) #{:SENDER})))

;; Set the :SENDER and :RECEIVER roles based on a message.
(defmethod schema/set-roles ::sender-and-receiver
  [_ {:keys [auth-info current-user] :as context}]
  (let [receivedby (schema/find-in-auth-info auth-info :receivedby)
        sentby (schema/find-in-auth-info auth-info :sentby)]
    (if (get-uid current-user)
      (update context :roles set/union
              (when (= receivedby (get-uid current-user))
                #{:RECEIVER})
              (when (= sentby (get-uid current-user))
                #{:SENDER}))
      context)))

(defmethod schema/possible-roles ::sender-and-receiver [_ _]
  #{:SENDER :RECEIVER})

;; Set the :SENDER and :RECEIVER roles based on a message thread.
(defmethod schema/set-roles ::thread-sender-and-receiver
  [_ {:keys [auth-info current-user] :as context}]
  (let [first-message (schema/find-in-auth-info auth-info ::first-message)]
    (if (get-uid current-user)
      (update context :roles set/union
              (when (= (:receivedby first-message) (get-uid current-user))
                #{:RECEIVER})
              (when (= (:sentby first-message) (get-uid current-user))
                #{:SENDER}))
      context)))

(defmethod schema/possible-roles ::thread-sender-and-receiver [_ _]
  #{:SENDER :RECEIVER})

(defn- inc-threshold-if-exists [key]
  (fn [trigger_opts msg]
    (cond-> trigger_opts
      (get msg key)
      (update trigger_opts :threshold inc))))

;;; Schema queries

(defn message-by-mid
  [context args _]
  (data/with-superlifter context
    (-> (fetch-message context args :immediate)
        (data/update-trigger! context :Message/sender :user-bucket
                              (inc-threshold-if-exists :sender-uid))
        (data/update-trigger! context :Message/receiver :user-bucket
                              (inc-threshold-if-exists :uid)))))

(defn- fixup-thread-report
  "Put the report linked to the thread, if any, in the right place."
  [{:keys [post_report comment_report] :as thread}]
  (let [report (cond
                 post_report (-> post_report
                                 (assoc :rtype :POST)
                                 (lacinia-schema/tag-with-type :PostReport))
                 comment_report (-> comment_report
                                    (assoc :rtype :COMMENT)
                                    (lacinia-schema/tag-with-type :CommentReport)))]
    (-> thread
        (assoc ::report report)
        (dissoc :post_report :comment_report))))

(defn report-by-reference
  [_ _ {:keys [::report]}]
  report)

(def modmail-message-thread-by-id
  {:name ::modmail-message-thread-by-id
   :prepare [(prepare/query-mod-thread :thread_id :thread)
             (args/verify-some [:thread :mtid])]
   :resolve
   (fn [context {:keys [thread]} _]
     (let [message-thread (->> thread
                               fixup-thread-report
                               (fixup-thread-fields context))]
       (data/with-superlifter context
         (-> (prom/promise message-thread)
             (data/update-trigger! context :MessageThread/sub :sub-bucket
                                   data/inc-threshold)))))})

(defn- modmail-category-mtypes
  "Return a list of mtypes appropriate for a modmail category query."
  [category]
  (let [message-types (case category
                        :MOD_DISCUSSIONS [:MOD_DISCUSSION]
                        :NEW [:USER_TO_MODS :USER_NOTIFICATION]
                        ;; Default for :ALL, :ARCHIVED and :IN_PROGRESS
                        [:USER_TO_MODS
                         :USER_NOTIFICATION
                         :MOD_TO_USER_AS_MOD
                         :MOD_TO_USER_AS_USER])]
    (map #(% constants/message-type-map) message-types)))

(defn thread-mtid-by-reference
  "Resolve only the id of a MessageThread object in a Message object."
  [_ _ resolved]
  {:id (pag/encode-id (:mtid resolved) :MessageThread)})

(defn- default-sids
  "Use the user's moderated subs if sids is empty."
  []
  {:name ::default-sids
   :func
   (fn [{:keys [current-user]} {:keys [sids] :as args}]
     (if (seq sids)
       args
       (->> (subs-moderated current-user)
            (map :sid)
            (assoc-in* args :sids))))})

(def modmail-threads
  "Get modmail conversations."
  {:name ::modmail-threads
   :prepare [(pag/verify-pagination-not-nested)
             (pag/verify-query-size :first)
             (default-sids)]
   :resolve
   (fn [{:keys [current-user db] :as context}
        {:keys [first after sids category unread_only] :as args} _]
     (let [select-args {:sids sids
                        :uid (get-uid current-user)
                        :first (inc first)
                        :after (when after (pag/timestamp-from-cursor after))
                        :unread_only unread_only
                        :mtypes (modmail-category-mtypes (:category args))
                        :mailbox (constants/message-mailbox-map
                                  (if (= :ARCHIVED category)
                                    :ARCHIVED :INBOX))}
           thread-data (case category
                         :NEW
                         (query/list-new-mod-threads db select-args)
                         :IN_PROGRESS
                         (query/list-in-progress-mod-threads db select-args)
                         ;; default for :ALL, :ARCHIVED and :MOD_DISCUSSIONS
                         (query/list-mod-threads db select-args))
           threads (map #(fixup-thread-fields context %) thread-data)
           raise-threshold (data/raise-threshold-fn (count threads))
           pagination-depth (list [:MessageThread first])]
       (data/with-superlifter context
         (-> (prom/promise threads)
             (data/update-trigger! context :MessageThread/sub :sub-bucket
                                   raise-threshold)
             (prom/then (fn [result]
                          (pag/resolve-pagination result first
                                                  pagination-depth)))))))})

(def modmail-thread
  "Get all messages in a conversation."
  {:name ::modmail-thread
   :prepare [(pag/verify-pagination-not-nested)
             (pag/verify-query-size :first)]
   :resolve
   (fn [{:keys [current-user db] :as context} {:keys [first after thread_id]} _]
     (let [mtid (pag/decode-id thread_id :MessageThread)
           select-args {:mtid mtid
                        :uid (get-uid current-user)
                        :first (inc first)
                        :after (when after (pag/timestamp-from-cursor after))}
           message-data (query/list-messages-in-thread db select-args)
           messages (map #(fixup-message-fields % context) message-data)
           pagination-depth (list [:Message first])]
       (pag/resolve-pagination messages first pagination-depth)))})

(defn modmail-thread-log
  "Resolve the query for modmail thread log entries."
  ;; will need sid in the resolution for identifying mods
  [_context _args _resolved]
  [])

(defn first-message-by-reference
  "Resolve the first message in an already fetched message thread."
  [context _ {:keys [sid ::first-message]}]
  (data/with-superlifter context
    (-> (fixup-thread-message-fields context sid first-message)
        prom/promise
        (data/update-trigger! context :Message/sender :user-bucket
                              (inc-threshold-if-exists :sender-uid))
        (data/update-trigger! context :Message/receiver :user-bucket
                              (inc-threshold-if-exists :uid)))))

(defn latest-message-by-reference
  "Resolve the latest message in an already fetched message thread."
  [context _ {:keys [sid ::latest-message]}]
  (data/with-superlifter context
    (-> (fixup-thread-message-fields context sid latest-message)
        prom/promise
        (data/update-trigger! context :Message/sender :user-bucket
                              (inc-threshold-if-exists :sender-uid))
        (data/update-trigger! context :Message/receiver :user-bucket
                              (inc-threshold-if-exists :uid)))))

;;; Schema Mutations

(defn- verify-sender-not-same-as-recipient
  []
  {:name ::verify-sender-not-same-as-recipient
   :func
   (fn [{:keys [current-user]} {:keys [username] :as args}]
     (if (and username (= (str/lower-case username)
                          (str/lower-case (get-name current-user))))
       (fail-verification args "Sender and recipient are the same")
       args))})

(defn- verify-report-reference
  "Resolve as an error if a report reference refers to an invalid report.
  Check that the receiving user is either the reporter or target of
  the report.  Does not check if user is authenticated or a mod of the
  sub."
  []
  {:name ::verify-report-reference
   :func
   (fn [{:keys [db]} {:keys [report_type report_id sid username] :as args}]
     (let
         [id (util/id-from-string report_id)
          params {:report_id id :sid sid :name username}]
       (if (and (or report_type report_id)
                (or (nil? report_type)
                    (nil? id)
                    (empty? (if (= :POST report_type)
                              (query/select-post-report db params)
                              (query/select-comment-report db params)))))
         (fail-verification args "Report does not exist")
         args)))})

(def create-modmail
  "Create a new modmail message."
  {:name ::create-modmail
   :prepare [(verify-sender-not-same-as-recipient)
             (verify-report-reference)
             (user-prepare/query-uid-by-name :username :uid)
             (args/ignore-if-nil
              :username
              (args/verify-some :uid "Recipient not found"))]

   :resolve
   (fn [{:keys [current-user db] :as context}
        {:keys [content sid show_mod_username report_id report_type
                username subject uid]}
        _]
     (let [mtype (cond
                   (nil? username) :MOD_DISCUSSION
                   (not show_mod_username) :MOD_TO_USER_AS_MOD
                   :else :MOD_TO_USER_AS_USER)
           timestamp (util/sql-timestamp)
           values {:content content
                   :subject subject
                   :mtype (mtype constants/message-type-map)
                   :posted timestamp
                   :receivedby uid
                   :sentby (get-uid current-user)
                   :sid sid
                   :report-type (when report_type (name report_type))
                   :report-id (util/id-from-string report_id)}
           msg (-> db
                   (query/insert-new-modmail-thread values)
                   first
                   (fixup-message-fields context))]
       (log/info {:mtype mtype :mid (:mid msg) :mtid (:mtid msg)
                  :report-type report_type :report-id report_id
                  :received-by uid}
                 "Mod started modmail thread")
       (data/with-superlifter context
         (cond-> (prom/promise msg)
           uid (data/update-trigger! context :Message/receiver :user-bucket
                                     data/inc-threshold)
           true (data/update-trigger! context :Message/sender :user-bucket
                                      data/inc-threshold)))
       msg))

   :notify
   [(fn [{:keys [notify] :as context} _ _ resolution]
      (doseq [event [:modmail-notification-counts
                     :recipient-notification-counts]]
        (notify/publish notify event context resolution)))]})

(def create-modmail-reply
  "Create a reply to a modmail conversation."
  {:prepare [(prepare/query-thread :thread_id :thread)
             (args/verify-some [:thread :mtid] "Not found")]
   :resolve
   (fn [{:keys [current-user db] :as context}
        {:keys [thread content send_to_user show_mod_username]}
        _]
     (let [mtype (cond
                   (not send_to_user) :MOD_DISCUSSION
                   (not show_mod_username) :MOD_TO_USER_AS_MOD
                   :else :MOD_TO_USER_AS_USER)
           {:keys [mtid sid target_uid]} thread
           mod-discussion? (= :MOD_DISCUSSION (:mtype thread))
           current-user-uid (get-uid current-user)
           timestamp (util/sql-timestamp)
           receiver-present? (and send_to_user
                                  (not mod-discussion?))
           values {:mtid mtid
                   :content content
                   :mtype (mtype constants/message-type-map)
                   :posted timestamp
                   :receivedby (when receiver-present? target_uid)
                   :sentby current-user-uid
                   :sid sid}
           msg (-> (query/insert-modmail-reply-returning db values)
                   first
                   (fixup-message-fields context))]
       (log/info {:mtype mtype :mid (:mid msg) :mtid mtid
                  :received-by (:receivedby values)}
                 "Mod replied to modmail thread")
       (data/with-superlifter context
         (cond-> (prom/promise msg)
           receiver-present? (data/update-trigger! context :Message/receiver
                                                   :user-bucket
                                                   data/inc-threshold)
           true (data/update-trigger! context :Message/sender :user-bucket
                                      data/inc-threshold)))))

   :notify
   [(fn [{:keys [notify] :as context} _ _ resolution]
      (doseq [event [:modmail-notification-counts
                     :recipient-notification-counts]]
        (notify/publish notify event context resolution)))]})

(def create-message-to-mods
  "Create a new message to the mods of a sub."
  {:name ::create-message-to-mods
   :prepare [(sub-prepare/query-sub :sid :sub {})
             (args/verify-some [:sub :sid] "Sub does not exist")]

   :resolve
   (fn [{:keys [current-user db] :as context} {:keys [content sid subject]} _]
     (let [uid (get-uid current-user)
           timestamp (util/sql-timestamp)
           values {:content content
                   :subject subject
                   :posted timestamp
                   :receivedby nil
                   :sentby uid
                   :sid sid
                   :reply_to nil
                   :replies 0}
           msg (-> (query/insert-contact-mods-thread-returning db values)
                   first
                   (fixup-message-fields context))]
       (log/info {:mid (:mid msg) :mtid (:mtid msg) :sid sid}
                 "User started modmail thread")
       (data/with-superlifter context
         (-> (prom/promise msg)
             (data/update-trigger! context :Message/sender :user-bucket
                                   data/inc-threshold)))))

   :notify
   [(fn [{:keys [notify] :as context} _ _ resolution]
      (notify/publish notify :modmail-notification-counts context resolution))]})

(defn create-private-message
  "Create a new private message."
  ;; For testing only, since it doesn't respect site.send_pm_to_user_min_level.
  [{:keys [current-user db] :as context} {:keys [subject content username]} _]
  (let [uid (get-uid current-user)
        ;; TODO check the recipient exists
        recipient-uid (:uid (user/uid-by-name db {:name username}))]
    (if (nil? recipient-uid)
      (resolve/with-error nil {:message "Recipient not found"})
      (let [timestamp (util/sql-timestamp)
            values {:content content
                    :subject subject
                    :posted timestamp
                    :sentby uid
                    :receivedby recipient-uid
                    :sid nil
                    :reply_to nil
                    :replies 0}
            msg (first (query/insert-message-thread-returning db values))]
        (log/info {:mid (:mid msg) :mtid (:mtid msg)
                   :recipient-uid recipient-uid} "Sent private message")
        (data/with-superlifter context
          (-> (fixup-message-fields msg context)
              prom/promise
              (data/update-trigger! context :Message/sender :user-bucket
                                    data/inc-threshold)
              (data/update-trigger! context :Message/receiver :user-bucket
                                    data/inc-threshold)))))))

(def update-message-unread
  "Change the read/unread state of a message."
  {:name ::update-message-unread

   :resolve
   (fn [{:keys [current-user db]} {:keys [mid unread]} _]
     (let [mid-as-int (util/id-from-string mid)
           params {:uid (get-uid current-user)
                   :mid mid-as-int}]
       (if unread
         (query/make-message-unread db params)
         (query/make-message-read db params))
       (log/info {:mid mid :unread unread} "Updated message unread")
       mid))

   :notify
   [(fn [{:keys [notify] :as context} _ _ resolution]
      (notify/publish notify :user-notification-counts context resolution))]})

(def update-thread-unread
  "Change the read/unread state of all messages in a thread."
  {:name ::update-thread-unread

   :resolve
   (fn [{:keys [current-user db]} {:keys [thread_id unread]} _]
     (let [mtid (pag/decode-id thread_id :MessageThread)
           params {:uid (get-uid current-user)
                   :mtid mtid}]
       (if unread
         (query/make-thread-unread db params)
         (query/make-thread-read db params))
       (log/info {:mtid mtid :unread unread} "Updated thread unread")
       (pag/encode-id mtid :MessageThread)))

   :notify
   [(fn [{:keys [notify] :as context} _ _ resolution]
      (notify/publish notify :user-notification-counts context resolution))]})

(def update-modmail-mailbox
  "Change the mailbox of a modmail conversation.
  Return the thread, supporting only the id, subject and sid fields."
  {:name ::update-modmail-mailbox
   :prepare [(prepare/query-thread :thread_id :thread)
             (args/verify-some [:thread :mtid] "Not found")
             (args/verify-fn :mailbox #{:INBOX :ARCHIVED}
                             "Invalid mailbox")]
   :resolve
   (fn [{:keys [current-user db]} {:keys [thread_id thread mailbox]} _]
     (let [{:keys [mtid subject sid]} thread
           mailbox-as-int (mailbox constants/message-mailbox-map)]
       (query/change-mailbox db {:mtid mtid
                                 :mailbox mailbox-as-int
                                 :uid (get-uid current-user)
                                 :updated (util/sql-timestamp)})
       (log/info {:mtid mtid :mailbox mailbox} "Changed modmail mailbox")
       {:id thread_id
        :subject subject
        :sid sid}))

   :notify
   [(fn [{:keys [notify] :as context} _ _ resolution]
      (notify/publish notify :modmail-notification-counts context
                      resolution))]})
