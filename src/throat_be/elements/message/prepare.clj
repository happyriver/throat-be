;; elements/message/prepare.clj -- Prepare message arguments for resolver execution
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.message.prepare
  (:require
   [throat-be.elements.message.query :as query]
   [throat-be.protocols.current-user :refer [get-uid]]
   [throat-be.resolver-helpers.pagination :as pag]
   [throat-be.resolver-helpers.core :as args :refer [assoc-in*
                                                     get-in*]]))

(defn query-mod-thread
  "Fetch a thread object and add it to `args` at `thread-path`.
  Include reports."
  [thread-id-path thread-path]
  {:name ::query-mod-thread
   :func
   (fn [{:keys [current-user db]} args]
     (let [id (-> (get-in* args thread-id-path)
                  (pag/decode-id :MessageThread))
           result (when id
                    (query/get-mod-thread-by-id db {:uid (get-uid current-user)
                                                    :thread_id id}))]
       (assoc-in* args thread-path result)))})

(defn query-thread
  "Fetch a thread object and add it to `args` at `thread-path`."
  [thread-id-path thread-path]
  {:name :query-thread
   :func
   (fn [{:keys [db]} args]
     (let [mtid (-> (get-in* args thread-id-path)
                    (pag/decode-id :MessageThread))
           result (when mtid
                    (query/select-message-thread db {:mtid mtid}))]
       (assoc-in* args thread-path result)))})
