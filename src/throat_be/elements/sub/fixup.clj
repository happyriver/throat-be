;; elements/sub/fixup.clj -- Make sub SQL results match schema expectations
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.sub.fixup
  (:require
   [clojure.set :as set]
   [throat-be.elements.constants :as constants]
   [throat-be.elements.sub :as-alias sub]
   [throat-be.util :as util]))

(defn- sum-report-counts
  "Total the open and closed post and comment reports."
  [{:keys [post_report_counts comment_report_counts] :as sub}]
  (let [{post-open :open post-closed :closed} post_report_counts
        {comment-open :open comment-closed :closed} comment_report_counts]
    (-> sub
        (assoc :open_report_count (+ (or post-open 0)
                                     (or comment-open 0)))
        (assoc :closed_report_count (+ (or post-closed 0)
                                       (or comment-closed 0)))
        (dissoc :post_report_counts :comment_report_counts))))

(defn fixup-moderator
  "Fix up a moderator record from the sub query."
  [{:keys [uid power_level]}]
  {:uid uid
   :moderation_level (constants/reverse-moderation-level-map power_level)})

(defn- fixup-post-type
  "Convert the post type to a keyword in a post type config object."
  [{:keys [ptype] :as post-type-config}]
  (-> post-type-config
      (assoc :post_type (constants/reverse-post-type-map ptype))
      (dissoc :ptype)))

(defn fixup-sub-fields
  "Merge the metadata fields, and rename some of them."
  [{:keys [meta agg_meta user_flair_choices] :as sub} _]
  (let [metadata (-> (merge meta agg_meta)
                     (set/rename-keys {:mod :creator_uid
                                       :ucf :user_can_flair
                                       :umf :user_must_flair
                                       :xmod2 :ex_mod_uids})
                     (util/convert-fields-to-boolean [:allow_link_posts
                                                      :allow_polls
                                                      :allow_text_posts
                                                      :allow_upload_posts
                                                      :freeform_user_flairs
                                                      :restricted
                                                      :sub_banned_users_private
                                                      :sublog_private
                                                      :user_can_flair
                                                      :user_can_flair_self
                                                      :user_must_flair]))]
    (-> (merge sub metadata)
        (update :banned true?)
        (assoc ::sub/user_flair_choices_with_ids user_flair_choices)
        (update :user_flair_choices #(vals (apply merge %)))
        (update :post_type_config #(map fixup-post-type %))
        (set/rename-keys {:mods ::sub/mods
                          :post_type_config ::sub/post_type_config})
        (dissoc :meta :agg_meta)
        (sum-report-counts))))

(defn fix-flair-post-types
  "Construct the list of allowed post types.
  The SQL query returns the disallowed ones.  Do the set arithmetic
  and convert to keywords."
  [{:keys [disallowed_ptypes] :as flair}]
  (let [ptypes (set/difference (set (vals constants/post-type-map))
                               (set disallowed_ptypes))]
    (-> flair
        (assoc :post_types (map constants/reverse-post-type-map ptypes))
        (dissoc :disallowed_post_types))))
