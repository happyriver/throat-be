;; elements/sql.clj -- Shared SQL query elements.
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.sql
  (:require
   [hugsql.core :as hugsql]
   [throat-be.elements.constants :refer [wrap-snip-fn]]
   [throat-be.elements.spec.db :as db-spec]))

(hugsql/def-db-fns "sql/elements.sql")

(wrap-snip-fn start-cte-snip)
(db-spec/fdef start-cte-snip
  :ret ::db-spec/sqlvec)

(wrap-snip-fn end-cte-snip)
(db-spec/fdef start-cte-snip
  :ret ::db-spec/sqlvec)
