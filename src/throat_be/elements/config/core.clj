;; elements/config/core.clj -- Configuration for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.config.core
  (:require
   [cambium.core :as log]
   [cheshire.core :as cheshire]
   [clojure.string :as str]
   [dotenv]
   [throat-be.elements.config.tasks :as tasks]
   [throat-be.protocols.cache :as cache]
   [throat-be.util :as util]))

(defn get-stored-config
  "Make the database query to get config fields, and cache the results."
  [{:keys [cache] :as context}]
  (or (cache/cache-get cache ::site-config)
      (tasks/fetch-config context)))

(def default-footer
  {"ToS" "/wiki/tos"
   "Privacy" "/wiki/privacy"
   "Canary" "/wiki/canary"
   "Donate" "/wiki/donate"
   "Bugs" "/wiki/bugs"
   "License" "/license"})

(def default-donate-presets [5 10 20])

(def default-expando-sites
  ["youtube.com"
   "www.youtube.com"
   "youtu.be"
   "gfycat.com"
   "streamja.com"
   "streamable.com"
   "player.vimeo.com"
   "vimeo.com"])

(defn json-from-env
  "If `envvar` is set, decode its value from JSON and return the result."
  [envvar]
  (when-let [value (dotenv/env envvar)]
    (try
      (cheshire/parse-string value)
      (catch Exception e
        (log/error {:exception-message (.getMessage e)
                    :envvar envvar}
                   "JSON parse error")
        nil))))

(defn enforce-footer-rules
  "Require some minimal content in the page footer."
  [footer]
  (if footer
    (-> footer
        (assoc "ToS" (or (get footer "ToS") (get default-footer "ToS")))
        (assoc "Privacy" (or (get footer "Privacy")
                             (get default-footer "Privacy")))
        (dissoc "License")
        (assoc "License" (get default-footer "License")))
    default-footer))

(defn make-footer-list [footer]
  (reduce-kv (fn [result k v]
               (conj result {:name k :link v})) [] footer))

(defn load-mottos [filename]
  (let [contents (when filename
                   (try (slurp filename)
                        (catch Exception e
                          (log/warn {:filename filename
                                     :exception-message (.getMessage e)}
                                    "Unable to read mottos file")
                          nil)))]
    (if (or (empty? contents) (str/blank? contents))
      []
      (try
        (cheshire/parse-string contents)
        (catch Exception _
          (str/split-lines contents))))))

(defn site-logo
  "Logo read from a file, if the name is supplied by :SITE_LOGO"
  []
  (let [logo-file-name (dotenv/env :SITE_LOGO)]
    (when logo-file-name
      (try
        (slurp logo-file-name)
        (catch java.io.FileNotFoundException _
          (log/error {:filename logo-file-name} "SITE_LOGO file not found")
          nil)))))

(defn build-site-config
  "The response to the site_configuration query."
  []
  {:sub_prefix (or (dotenv/env "SITE_SUB_PREFIX") "o")
   :footer (->
            (or (json-from-env "SITE_FOOTER_LINKS")
                default-footer)
            enforce-footer-rules
            make-footer-list)
   :last_word (or (dotenv/env "SITE_LAST_WORD") "")
   :logo (site-logo)
   :enable_totp (some? (#{"True" "true" "1"} (dotenv/env "SITE_ENABLE_TOTP")))
   :donate_presets (or (json-from-env "SITE_DONATE_PRESETS")
                       default-donate-presets)
   :donate_minimum (or (util/env-integer "SITE_DONATE_MINIMUM") 1)
   :thumbnail_url (or (dotenv/env "STORAGE_THUMBNAILS_URL") "/files")
   :metadata_image (dotenv/env "SITE_METADATA_IMAGE")
   :metadata_image_width (dotenv/env "SITE_METADATA_IMAGE_WIDTH")
   :metadata_image_height (dotenv/env "SITE_METADATA_IMAGE_HEIGHT")
   :expando_sites (or (json-from-env "SITE_EXPANDO_SITES")
                      default-expando-sites)
   ::fathom {:data-site (dotenv/env "FATHOM_DATA_SITE")
             :goal (dotenv/env "FATHOM_GOAL")}
   :upload_max_size (or (util/env-integer "SITE_UPLOAD_MAX_SIZE") 16777216)
   :software
   [{:name "Throat Reframed"
     :license_name "GNU 3.0 or later"
     :license_link
     "https://gnu.org/licenses/gpl-3.0.html"
     :source_code_link
     "https://gitlab.com/happyriver/throat-fe"}
    {:name "Throat GraphQL"
     :license_name "GNU AGPL 3.0 or later"
     :license_link
     "https://gnu.org/licenses/agpl-3.0.html"
     :source_code_link
     "https://gitlab.com/happyriver/throat-be"}
    {:name "Throat"
     :license_name "MIT"
     :license_link
     "https://raw.githubusercontent.com/Phuks-co/throat/master/LICENSE"
     :source_code_link
     "https://github.com/Phuks-co/throat"}]

   :mottos (load-mottos (dotenv/env "SITE_MOTTOS"))})

(def built-site-config (build-site-config))

(defn site-config
  [{:keys [env] :as context}]
  (try
    (merge (if (#{:dev :test} env) (build-site-config) built-site-config)
           (get-stored-config context))
    (catch Exception e
      (log/error e "Error fetching site configuration"))))
