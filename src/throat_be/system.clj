;; system.clj -- Integration of subsystems for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.system
  (:require
   [cambium.codec :as codec]
   [cambium.core :as log]
   [cambium.logback.json.flat-layout :as flat]
   [camel-snake-kebab.core :as csk]
   [cheshire.core :as cheshire]
   [clojure.core.reducers :as r]
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [clojure.spec.alpha :as spec]
   [clojure.string :as str]
   [com.rpl.specter :as s]
   [com.stuartsierra.component :as component]
   [dotenv]
   [java-time.api :as jt]
   [throat-be.elements.config.tasks :as config]
   [throat-be.elements.post.tasks :as post]
   [throat-be.elements.site.tasks :as site]
   [throat-be.elements.user.tasks :as user]
   [throat-be.protocols.asset-path :as asset-path]
   [throat-be.schema :as schema]
   [throat-be.system.bus :as bus]
   [throat-be.system.cache.redis :as redis-cache]
   [throat-be.system.db :as db]
   [throat-be.system.notify.throat-py :as throat-py-notify]
   [throat-be.system.payment-subscription.ovarit :as payment-subscription]
   [throat-be.system.pubsub.redis :as redis-pubsub]
   [throat-be.system.ratelimit.token-bucket :as ratelimit-implementation]
   [throat-be.system.runtime :as runtime]
   [throat-be.system.scheduler :as scheduler]
   [throat-be.system.server :as server]
   [throat-be.system.tasks :as tasks]
   [throat-be.system.visitor-counter.pg-hyperloglog :as visitor-counter]
   [throat-be.util :as util])
  (:gen-class))

;; Server configuration

(def session-lifetime
  (or (util/env-integer :PERMANENT_SESSION_LIFETIME)
      (* 31 24 60 60)))

(def headers-to-log
  (let [value (dotenv/env :LOG_REQUEST_HEADERS)
        headers (when value
                  (try
                    (cheshire/parse-string value)
                    (catch Exception e
                      (log/error {:exception-message (.getMessage e)}
                                 "JSON parse error in LOG_REQUEST_HEADERS")
                      nil)))]
    (map str/lower-case headers)))

(defn query-hashes
  "Load the query hash table and parse its dates into epoch milliseconds."
  []
  (let [to-ms (fn [datestr]
                (-> (jt/local-date "yyyy-MM-dd" datestr)
                    (jt/instant (jt/zone-offset 0))
                    jt/to-millis-from-epoch))]
    (->> (io/resource "query-hashes.edn")
         slurp
         edn/read-string
         (map (fn [entry]
                (update entry :date to-ms))))))

;; Look in the assets directory for the newest subdirectory of /fe.  This
;; allows you to replace the front end with a new version by copying it
;; into a new directory with a new (cachebusting) name.
(defrecord NewestSubdirectory [root-path]
  asset-path/AssetPath
  (asset-path [_ path]
    (let [dirs (when-not (nil? root-path)
                 (let [fe-dir (io/file root-path)
                       files (when (.isDirectory fe-dir) (.listFiles fe-dir))]
                   (filter (fn [f] (.isDirectory f)) files)))]
      (if (seq dirs)
        (let [dirs-with-times (zipmap dirs
                                      (map (fn [f] (.lastModified f)) dirs))
              newest-dir (key (apply max-key val dirs-with-times))
              basename (.getName newest-dir)]
          (str "/fe/" basename path))
        (str "/fe" path)))))

(defrecord DevAssets []
  asset-path/AssetPath
  (asset-path [_ path]
    path))

(defn get-server-config [{:keys [env server]}]
  (or
   server
   (let [configured-path (dotenv/env :ASSET_PATH)
         path (if (nil? configured-path)
                ""
                ;; Make the static asset path configuration more human-friendly
                ;; by doing the right thing whether or not they put a slash
                ;; on the end.
                (if (str/ends-with? configured-path "/")
                  (apply str (butlast configured-path))
                  configured-path))
         static-assets (if (= env :dev)
                         (DevAssets.)
                         (NewestSubdirectory. path))
         service-path (asset-path/asset-path static-assets "")
         secret-key (dotenv/env :SECRET_KEY)
         admin-totp (dotenv/env :SITE_ENABLE_TOTP)]
     ;; Put a warning in the logs if no files are present to serve.
     (when (= service-path "/fe")
       (log/info {:ASSET_PATH path}
                 "No subdirectories found in ASSET_PATH"))
     (when (nil? secret-key)
       (log/error "SECRET_KEY not set"))
     {:host (or
             (dotenv/env :SERVER_HOST)
             "localhost")
      :port (or
             (util/env-integer :SERVER_PORT)
             8888)
      :assets static-assets
      :env env
      :request-timeout (or (util/env-integer :SERVER_REQUEST_TIMEOUT) 2500)
      :process-queue-size (or (util/env-integer :SERVER_PROCESS_REQUESTS) 16)
      :waiting-queue-size (or (util/env-integer :SERVER_QUEUE_REQUESTS) 640)
      :trusted-proxy-count (or (util/env-integer :SITE_TRUSTED_PROXY_COUNT) 0)
      :headers-to-log headers-to-log
      :secret-key secret-key
      :session-lifetime session-lifetime
      :stop-hour (dotenv/env :SERVER_STOP_HOUR)
      :site-name (dotenv/env :SITE_SERVER_NAME)
      :sub-prefix (or (dotenv/env :SITE_SUB_PREFIX) "s")
      :pool-size (or (util/env-integer :SERVER_QUERY_POOL_SIZE) 16)
      :admin-totp? (#{"True" "true" "1"} admin-totp)
      :query-hashes (query-hashes)})))

(defn webserver-component
  [config]
  (component/using
   (server/map->Server (get-server-config config))
   [:bus :cache :db :notify :pubsub :ratelimiter :schema-provider :scheduler
    :taskrunner :visitor-counter]))

(defn health-check-server-component
  [config]
  (component/using
   (server/map->HealthCheckServer (get-server-config config))
   [:cache]))

;; Schema configuration

(defn get-schema-provider-config [{:keys [env schema-provider]}]
  (or schema-provider
      {:env env
       :secret-key (dotenv/env :SECRET_KEY)
       :session-lifetime session-lifetime
       :query-hashes (query-hashes)}))

(defn schema-provider-component [config]
  (schema/map->SchemaProvider (get-schema-provider-config config)))

(defn get-cache-config [{:keys [cache]}]
  (or cache
      {:uri (dotenv/env :REDIS_URL)}))

(defn cache-component [config]
  (redis-cache/map->RedisCache (get-cache-config config)))

(defn get-notify-config [{:keys [notify]}]
  (or notify {:prefixes {:outgoing "/sent"
                         :incoming "/snt"}}))

(defn notify-component [config]
  (component/using (throat-py-notify/map->ThroatNotify
                    (get-notify-config config))
                   [:pubsub]))

(defn get-pubsub-config [{:keys [pubsub]}]
  (or pubsub
      {:uri (dotenv/env :REDIS_URL)}))

(defn pubsub-component [config]
  (redis-pubsub/map->RedisPubSub (get-pubsub-config config)))

(defn get-bus-config [{:keys [bus]}]
  (or bus
      {:prefix "mbus"}))

(defn bus-component [config]
  (component/using (bus/map->MessageBus (get-bus-config config))
                   [:pubsub]))

(defn get-ratelimiter-config [{:keys [ratelimiter]}]
  (or ratelimiter
      (let [value (dotenv/env :RATELIMIT_ENABLE)
            explicitly-disabled (or (= value "False") (= value "false"))]
        {:enabled? (not explicitly-disabled)
         :uri (dotenv/env :REDIS_URL)})))

(defn ratelimiter-component [config]
  (ratelimit-implementation/map->RateLimiter (get-ratelimiter-config config)))

(defn get-visitor-counter-config [{:keys [visitor-counter]}]
  (or visitor-counter {}))

(defn visitor-counter-component [config]
  (component/using (visitor-counter/map->VisitorCounter
                    (get-visitor-counter-config config))
                   [:db]))

(defn get-db-config [{:keys [db]}]
  (or db
      {:host (dotenv/env :DATABASE_HOST)
       :port (dotenv/env :DATABASE_PORT)
       :name (dotenv/env :DATABASE_NAME)
       :user (dotenv/env :DATABASE_USER)
       :password (dotenv/env :DATABASE_PASSWORD)}))

(defn db-component [config]
  (db/map->Db (get-db-config config)))

(defn get-scheduler-config [{:keys [scheduler]}]
  (or scheduler {}))

(defn scheduler-component [config]
  (scheduler/map->Scheduler (get-scheduler-config config)))

(defn- task-envvars
  "Return the environment variables that configure tasks (the ones that
  include the substring '_TASK_') as a map."
  []
  (->> (dotenv/env)
       (filter #(str/includes? (key %) "_TASK_"))
       (map (fn [[k v]] [(keyword k) v]))
       (into {})))

(defn- extract-task-options
  "From the map of all task options (derived from environment
  variables), get the ones relevant to the current task into a map.
  Supply the default if a value is not given, and parse integers
  if an integer value is expected."
  [taskname defaults envvars]
  (let [prefix (-> taskname
                   name
                   csk/->snake_case
                   str/upper-case
                   (str "_TASK_"))
        remove-prefix #(-> (name %)
                           (subs (count prefix))
                           str/lower-case
                           csk/->kebab-case
                           keyword)
        prefix-mismatch? #(not (str/starts-with? (name %) prefix))
        task-envvars (->> envvars
                          (s/setval [s/MAP-KEYS prefix-mismatch?] s/NONE)
                          (s/transform [s/MAP-KEYS] remove-prefix))
        parse-int (fn [k v vdef]
                    (try
                      (Integer/parseInt v)
                      (catch java.lang.NumberFormatException _
                        (log/error {:task taskname :option k}
                                   "Non-numeric value in environment variable")
                        vdef)))
        option-or-default (fn [[k vdef]]
                            (let [option (k task-envvars)]
                              [k (if-not option
                                   vdef
                                   (if (integer? vdef)
                                     (parse-int k option vdef)
                                     option))]))]
    (s/transform [s/ALL] option-or-default defaults)))

(comment
  (def m {:REMOVE_OLD_COMMENT_VIEWS_TASK_BATCH_LIMIT "100000",
          :RANDOM 32432,
          :REMOVE_OLD_COMMENT_VIEWS_TASK_INTERVALS "600000",
          :REMOVE_OLD_COMMENT_VIEWS_TASK_BATCH_SIZE "10000"})
  #_(extract-task-options :remove-old-comment-views (:defaults (first tasks)) m))

(defn- add-task-options
  "Add options derived from environment variables to a task description."
  [{:keys [task-name defaults] :as task} envvars]
  (assoc task :options
         (extract-task-options task-name defaults envvars)))

(def taskrunner-tasks
  "Tasks to run if the system includes the :taskrunner role."
  (let [host (or (dotenv/env :OVARIT_SUBSCRIPTION_HOST) "")
        payment-subscription (payment-subscription/map->OvaritSubscription
                              {:secret-key (dotenv/env :SECRET_KEY)
                               :host host})]
    (map #(assoc % :single-instance? true)
         [{:task-name :remove-old-comment-views
           :task-fn post/remove-old-comment-views
           :defaults {:interval 60000
                      :batch-size 10000
                      :batch-limit 100000
                      :initial-delay 60000}
           :description "Remove old comment views"}

          {:task-name :visitor-count-update
           :task-fn visitor-counter/update-uniques
           :defaults {:interval 5000
                      :batch-size 10000
                      :batch-limit 100000
                      :initial-delay 90000}
           :description "Update visitor counts"}

          {:task-name :subscription-update
           :task-fn user/update-customers
           :defaults {:interval 60000
                      :payment-subscription payment-subscription
                      :initial-delay 30000}
           :description "Update payment subscriptions"}

          {:task-name :refresh-site-config
           :task-fn config/update-site-config
           :defaults {:interval 30000  ; in ms
                      :cache-ex 40     ; in s
                      :initial-delay 0}}])))

(def webserver-tasks
  "Tasks to run in each instance with the :webserver role."
  (let [host (or (dotenv/env :OVARIT_SUBSCRIPTION_HOST) "")
        payment-subscription (payment-subscription/map->OvaritSubscription
                              {:secret-key (dotenv/env :SECRET_KEY)
                               :host host})]
    (map #(assoc % :single-instance? false)
         [{:task-name :funding-progress-update
           :task-fn site/update-funding-progress
           :defaults {:interval 300000
                      :initial-state 0
                      :payment-subscription payment-subscription}
           :description "Update funding progress"}])))

(defn get-tasks-config
  "Add options extracted from environment variables to a list of tasks."
  [roles {:keys [env tasks]}]
  (or tasks
      {:env env
       :uri (dotenv/env :REDIS_URL)
       :parent "system"
       :taskrunner? (some? (roles :taskrunner))
       :tasks
       (let [envvars (task-envvars)]
         (map #(add-task-options % envvars)
              (concat (when (roles :webserver)
                        webserver-tasks)
                      (when (roles :taskrunner)
                        taskrunner-tasks))))}))

(defn tasks-component [roles config]
  (component/using
   (tasks/map->TaskRunner (get-tasks-config roles config))
   [:db :bus :cache :scheduler]))

(defn new-system-map
  "Build a map describing all the parts of throat-be."
  [roles config]
  (let [server (if (roles :webserver)
                 (webserver-component config)
                 (health-check-server-component config))]
    {:cache           (cache-component config)
     :db              (db-component config)
     :bus             (bus-component config)
     :pubsub          (pubsub-component config)
     :notify          (notify-component config)
     :ratelimiter     (ratelimiter-component config)
     :scheduler       (scheduler-component config)
     :schema-provider (schema-provider-component config)
     :visitor-counter (visitor-counter-component config)
     :taskrunner      (tasks-component roles config)
     :server          server}))

(defn new-system
  "Collect all the moving parts."
  [roles {:keys [env] :as config-map}]
  (let [system-map (new-system-map roles config-map)]
    (log/info {:env env} "Creating system")
    (apply component/system-map (into [] (r/flatten system-map)))))

(spec/def ::arg #{"--webserver" "--taskrunner"})
(spec/def ::args (spec/+ ::arg))

(defn parse-args
  "Return the arguments, keywordized, as a set."
  [args]
  (let [parsed-args (spec/conform ::args args)]
    (if (= ::spec/invalid parsed-args)
      (do (println "Unrecognized command line arguments")
          (spec/explain ::args args))
      (->> args
           (map #(get {"--webserver" :webserver
                       "--taskrunner" :taskrunner} %))
           set))))

(comment
  (parse-args ["--webserver"])
  (parse-args ["--webserver" "--taskrunner"])
  (parse-args ["--help"])
  (parse-args []))

(defn -main [& args]
  ;; Configure logging back end.
  (flat/set-decoder! codec/destringify-val)
  (let [env (if (dotenv/env :DEBUG) :dev :prod)
        roles (parse-args args)
        system (new-system (parse-args args) {:env env})]
    (runtime/set-default-uncaught-exception-handler!
     (fn [_thread e]
       (log/error e "Uncaught exception, system exiting.")
       (System/exit 1)))
    (runtime/add-shutdown-hook!
     ::stop-system #(do
                      (log/info "System exiting, running shutdown hooks")
                      (component/stop system)))
    (when (seq roles)
      (component/start-system system))))
