;; system/db.clj -- Database component for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.system.db
  (:require
   [cambium.core :as log]
   [com.stuartsierra.component :as component]
   [hugsql.core :as hugsql]
   [throat-be.system.db.hugsql-adapter :as adapter])
  (:import (com.mchange.v2.c3p0 ComboPooledDataSource)))

(defn ^:private pooled-data-source
  "Create a database connection using a pooled provider."
  [host port dbname user password]
  {:datasource
   (doto (ComboPooledDataSource.)
     (.setDriverClass "net.sf.log4jdbc.DriverSpy") ; org.postgresql.Driver
     (.setJdbcUrl (str "jdbc:log4jdbc:postgresql://" host ":" port "/" dbname))
     (.setUser user)
     (.setPassword password))})

(defrecord Db [host port name user password ds]
  component/Lifecycle

  (start [this]
    (log/info {:dbname name} "Creating database connection pool")
    (hugsql/set-adapter! (adapter/hugsql-adapter-clojure-java-jdbc-c3p0-counter))
    (assoc this
           :ds (pooled-data-source host port name user password)))

  (stop [this]
    (when ds
      (log/info {:dbname name} "Closing database connection pool")
      (-> ds :datasource .close)
      (assoc this :ds nil))))

