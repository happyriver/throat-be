;; system/scheduler.clj -- Task scheduling for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.system.scheduler
  (:require
   [cambium.core :as log]
   [com.stuartsierra.component :as component]
   [overtone.at-at :as at-at]
   [throat-be.protocols.scheduler :as protocol]))

(defrecord Scheduler [pool]
  component/Lifecycle

  (start [this]
    (let [threadpool (at-at/mk-pool)]
      (log/info "Starting scheduler")
      (assoc this :pool threadpool)))

  (stop [this]
    (when pool
      (log/info "Stopping scheduler"))
    (dissoc this :pool :instance)))

(extend-protocol protocol/Scheduler
  Scheduler

  (after
    ([{:keys [pool]} interval fn]
     (let [stop-it (at-at/after interval fn pool)]
       #(at-at/stop stop-it))))

  (every
    ([this interval fn]
     (protocol/every this interval fn {}))

    ([{:keys [pool]} interval fn {:keys [initial-delay desc]}]
     (let [stop-it (at-at/every interval fn pool
                                :initial-delay (or initial-delay 0)
                                :desc desc)]
       #(at-at/stop stop-it))))

  (interspaced
    ([this interval fn]
     (protocol/interspaced this interval fn {}))

    ([{:keys [pool]} interval fn
      {:keys [initial-delay desc]}]
     (let [stop-it (at-at/interspaced interval fn pool
                                      :initial-delay (or initial-delay 0)
                                      :desc desc)]
       #(at-at/stop stop-it)))))

(comment
  (require 'user)
  (at-at/show-schedule (get-in user/system [:scheduler :pool]))
  (at-at/scheduled-jobs (get-in user/system [:scheduler :pool])))
