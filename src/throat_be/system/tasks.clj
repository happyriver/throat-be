;; system/tasks.clj -- Task descriptions for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.system.tasks
  "Start scheduled tasks."
  (:require
   [cambium.core :as log]
   [com.stuartsierra.component :as component]
   [taoensso.carmine :as car]
   [throat-be.protocols.scheduler :as scheduler]
   [throat-be.protocols.tasks :as tasks]
   [throat-be.util :as util]))

(defn key-prefix [parent]
  (str (symbol ::component) "-" parent "-"))

(defn run-if-main-instance
  "Return a function that runs func only if this is the designated
  taskrunner instance (first in alphabetical order by instance
  name)."
  [func conn parent instance]
  (fn []
    (let [prefix (key-prefix parent)
          keynames (car/wcar conn (car/keys (str prefix "*")))
          instances (sort (map #(subs % (count prefix)) keynames))]
      (when (or (empty? instances) (= instance (first instances)))
        (func)))))

(def keepalive 5000)

(defn start-keepalive
  "Start a task that keeps a key alive on Redis."
  [scheduler conn parent instance]
  (let [key (str (key-prefix parent) instance)]
    (car/wcar conn (car/setex key keepalive true))
    (scheduler/interspaced scheduler (* keepalive 0.9)
                           #(car/wcar conn
                                      (car/setex key (/ keepalive 1000) true))
                           {:desc (str "taskrunner redis update " instance)})))

(defn start-task
  "Start a single task. If :single-instance? is set for the task, use
  Redis to prevent multiple instances from running the task at the
  same time."
  [{:keys [task-name run-task-fn options single-instance? description]}
   {:keys [conn parent instance scheduler]}]
  (let [wrapped-fn (if single-instance?
                     (run-if-main-instance run-task-fn conn parent instance)
                     run-task-fn)]
    (log/info {:task task-name
               :options (select-keys options [:interval
                                              :batch-size
                                              :batch-limit
                                              :initial-delay])}
              "Starting scheduled task")
    (scheduler/every scheduler (:interval options)
                     wrapped-fn
                     {:desc description
                      :initial-delay (:initial-delay options)})))

(defn add-context
  "Add a state atom, a context map and error handling to a task."
  [{:keys [task-name options task-fn] :as task} context]
  (let [state (atom (:initial-state options))
        task-context (assoc context
                            :options options
                            :state state)
        run-task-fn #(try
                       (task-fn task-context)
                       (catch Throwable e
                         (log/error {:task-name task-name} e
                                    "error running task")))]
    (assoc task
           :state state
           :run-task-fn run-task-fn)))

(defrecord TaskRunner [uri parent taskrunner? tasks env db bus cache scheduler
                       instance tasks-with-state stop-tasks stop-keepalive]
  component/Lifecycle

  (start [this]
    (let [tasks' (map #(add-context % {:env env
                                       :db db
                                       :bus bus
                                       :cache cache}) tasks)
          instance (util/rand-str 6)
          conn {:pool {} :spec {:uri uri}}
          params {:conn conn
                  :instance instance
                  :parent parent
                  :scheduler scheduler}]
      (log/info {:num-tasks (count tasks)
                 :parent parent
                 :instance instance} "Starting scheduled tasks")
      (assoc this
             :instance instance
             :tasks tasks'
             :stop-tasks (doall (map #(start-task % params) tasks'))
             :stop-keepalive (when taskrunner?
                               (start-keepalive scheduler conn parent
                                                instance)))))

  (stop [this]
    (log/info {:instance instance} "Stopping tasks")
    (doseq [stop-task stop-tasks]
      (stop-task))
    (when stop-keepalive
      (stop-keepalive))
    (dissoc this :stop-tasks :stop-keepalive))

  tasks/TaskList
  (state [_ name]
    (-> (filter #(= (:task-name %) name) tasks)
        first
        :state)))
