(defproject throat-be "0.1.0-SNAPSHOT"
  :description "GraphQL server for the link aggregator Throat"
  :url "http://gitlab.com/happyriver/throat-be"
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [org.clojure/java.jdbc "0.7.12"]
                 [org.clojure/tools.namespace "1.3.0"]
                 [org.clojure/core.async "1.6.673"]
                 [com.walmartlabs/lacinia "1.2-rc-1"]
                 [com.walmartlabs/lacinia-pedestal "1.1"]
                 [com.stuartsierra/component "1.1.0"]
                 [com.layerware/hugsql "0.5.3"]
                 [com.github.seancorfield/honeysql "2.3.928"]
                 [superlifter "0.1.4"]
                 [org.postgresql/postgresql "42.5.0"]
                 [com.mchange/c3p0 "0.9.5.5"]
                 [lynxeyes/dotenv "1.1.0"]
                 [com.rpl/specter "1.1.4"]
                 [pandect "1.0.2"]
                 [cheshire "5.11.0"]
                 [com.taoensso/carmine "3.1.0"]
                 [hiccup "1.0.5"]
                 [expound "0.9.0"]
                 [overtone/at-at "1.2.0"]
                 [camel-snake-kebab "0.4.3"]
                 [clj-http "3.12.3"]
                 [funcool/promesa "9.0.494"]
                 [manifold "0.2.4"]
                 [clojure.java-time "1.1.0"]
                 [com.github.slugify/slugify "3.0.2"]

                 ;; JSON logging
                 [cambium/cambium.core           "1.1.1"]
                 [cambium/cambium.codec-cheshire "1.0.0"]
                 [cambium/cambium.logback.json   "0.4.5"]

                 ;; Database logging
                 [com.googlecode.log4jdbc/log4jdbc "1.2"]

                 ;; Function argument and return spec checking
                 [orchestra "2021.01.01-1"]]
  :profiles
  {:dev {:dependencies [[cider/cider-nrepl "0.30.0"]
                        [etaoin "0.4.6"]
                        [ns-tracker/ns-tracker "0.4.0"]
                        [reaver "0.1.3"]
                        [refactor-nrepl "3.6.0"]
                        [stylefruits/gniazdo "1.2.1"
                         :exclusions [org.eclipse.jetty.websocket/websocket-client]]]
         :resource-paths ["dev-resources"]
         :main user}
   :uberjar {:omit-source true
             :aot [throat-be.system]
             :main throat-be.system
             :jvm-opts ["-Dclojure.spec.compile-asserts=false"]}}
  :target-path "target/%s/"
  :test-selectors {:default (complement :e2e)
                   :e2e :e2e}
  :jvm-opts ["-Dclojure.tools.logging.factory=clojure.tools.logging.impl/slf4j-factory"
             "-Dlog4j2.formatMsgNoLookups=true"])
