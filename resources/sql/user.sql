-- sql/user.sql -- User SQL queries for throat-be
-- Copyright (C) 2020-2022  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name _select-users :? :*
-- :doc Get multiple users by name or uid
:snip:start-cte-snip
:snip:badges-cte-snip
:snip:notification-cte-snip
:snip:score-cte-snip
:snip:end-cte-snip
select
  :snip:badges-field-snip
  :snip:content-blocks-field-snip
  :snip:meta-fields-snip
  :snip:notification-field-snip
  :snip:personal-field-snip
  :snip:score-field-snip
  :snip:subs-moderated-field-snip
  :snip:subscription-field-snip
  :snip:username-history-field-snip
  u.given,
  u.joindate,
  u.name,
  u.status,
  u.uid
  from "user" as u
  :snip:badges-join-snip
  :snip:notification-join-snip
  :snip:score-join-snip
:snip:where-snip
-- group by u.uid

-- :name uid-by-name :? :1
-- :doc Get a single uid by name
select uid
  from public.user
 where lower(name) = lower(:name)

-- :name insert-user :! :n
-- :doc Insert a new user
insert into public.user (uid, joindate, name, status)
values (:uid, :joindate, :name, :status);

-- :name select-customer-update-log :? :*
-- :doc Select unfinished items from the customer update log
select log.id, log.action, log.value, u.uid, u.resets
  from customer_update_log as log
       left join "user" as u on u.uid = log.uid
 where log.completed is null;

-- :name update-customer-update-log :! :n
-- :doc Mark a customer update log entry as completed
update customer_update_log
   set completed = :completed,
       success = :success
 where id = :id;

-- :name _update-subscription-status :<!
-- :doc Replace any existing sub_subscriber records with a new one.
with
  diff as
    (
      select :sid as sid,
             count(xid) as total
        from (select xid
                from sub_subscriber
               where status = :subscription-status/SUBSCRIBED
                 and uid = :uid
                 and sid = :sid) as existing
    ),
  update_subscriber_count as
    (
      update sub s
         set subscribers = subscribers - total + :adjust
        from diff d
       where d.sid = s.sid
    ),
  remove_existing as
    (
      delete from sub_subscriber
       where uid = :uid
         and sid = :sid
    ),
  new_subscription as
    (
      insert into sub_subscriber (sid, uid, status, time)
      values (:sid, :uid, :status, now())
    )
select sub.sid,
       sub.name,
       :status as status
  from sub
 where sub.sid = :sid;

-- :name _delete-subscription-status :<!
-- :doc Delete an existing subscription or sub block.
with
  diff as
    (
      select :sid as sid,
             count(xid) as total
        from (select xid
                from sub_subscriber
               where status = :status
                 and uid = :uid
                 and sid = :sid) as existing
    ),
  update_subscriber_count as
    (
      update sub s
         set subscribers = subscribers - total * :adjust
             from diff d
       where d.sid = s.sid
    ),
  delete_existing as
    (
    delete from sub_subscriber
    where uid = :uid
    and sid = :sid
    )
select sub.sid,
       sub.name,
       ss.status,
       ss.order
  from sub
       left join (select *
                    from sub_subscriber
                   where sid = :sid
                     and uid = :uid
                     and status != :status
                   order by xid desc
                   limit 1) as ss
           on ss.sid = sub.sid
 where sub.sid = :sid;

-- :snip by-uids-snip
where u.uid in (:v*:uids)

-- :snip by-names-snip
where lower(u.name) in (:v*:names)

-- :snip subscription-field-snip
   (select
      json_agg(json_build_object('order', sub_subscriber.order,
                                 'status', sub_subscriber.status,
                                 'sid', sub_subscriber.sid,
                                 'name', sub.name)) as subs
      from (select sub_subscriber.uid,
                   sub_subscriber.sid,
                   max(sub_subscriber.time) as maxtime
              from sub_subscriber
             group by uid, sid) as ss
           inner join sub_subscriber
               on sub_subscriber.sid = ss.sid
               and sub_subscriber.uid = ss.uid
               and sub_subscriber.time = ss.maxtime
           left join sub
               on sub.sid = sub_subscriber.sid
     where sub_subscriber.uid = u.uid
     group by sub_subscriber.uid
   ) as subs,

-- :snip notification-cte-snip
  message_count as (
    select :uid as uid,
           count(*)
     from message
          left join user_message_block
              on user_message_block.uid = :uid
              and user_message_block.target = message.sentby
          inner join user_unread_message
              on user_unread_message.uid = :uid
              and user_unread_message.mid = message.mid
          inner join user_message_mailbox
              on user_message_mailbox.uid = :uid
              and user_message_mailbox.mid = message.mid
    where user_message_mailbox.mailbox = :message/INBOX
      and (user_message_block.id is null
           or (message.mtype != :message/USER_TO_USER))
  ),
  notification_count as (
    select :uid as uid,
           count (*)
      from notification
           left join user_content_block
               on user_content_block.uid = :uid
               and user_content_block.target = notification.sentby
           left join sub_mod
               on sub_mod.uid = notification.sentby
               and sub_mod.sid = notification.sid
               and not sub_mod.invite
           left outer join sub_mod as sub_mod_current_user
               on sub_mod_current_user.uid = :uid
               and sub_mod_current_user.sid = notification.sid
               and not sub_mod_current_user.invite
     where notification.receivedby = :uid
       and notification.read is null
       and (user_content_block.id is null
            or not sub_mod.power_level is null
            or not sub_mod_current_user.power_level is null
            or not (notification.type in ('POST_REPLY',
                                          'COMMENT_REPLY',
                                          'POST_MENTION',
                                          'COMMENT_MENTION')))
  ),

-- :snip notification-field-snip
  message_count.count as unread_message_count,
  notification_count.count as unread_notification_count,

-- :snip notification-join-snip
  left join message_count
      on message_count.uid = u.uid
  left join notification_count
      on notification_count.uid = u.uid

-- :snip meta-fields-snip
   (select json_object_agg(user_metadata.key, user_metadata.value) as meta
      from user_metadata
     where user_metadata.key in ('admin', 'labrat', 'nochat', 'nostyles',
                                 'invitecode', 'nsfw', 'nsfw_blur', 'subtheme')
           and user_metadata.uid = u.uid
     group by user_metadata.uid
   ) as meta,

-- :snip subs-moderated-field-snip
   (select json_agg(
     json_build_object('sid', sub_mod.sid,
                       'sub_name', sub.name,
                       'power_level', sub_mod.power_level))
      from sub_mod
           left join sub
               on sub.sid = sub_mod.sid
     where sub_mod.uid = u.uid
       and not sub_mod.invite
     group by u.uid) as mods,

-- :snip content-blocks-field-snip
  (select json_agg(
    json_build_object('uid', user_content_block.target,
                      'content_block', user_content_block.method))
     from user_content_block
      where user_content_block.uid = u.uid
    group by u.uid) as content_blocks,

-- :snip badges-cte-snip
  user_badges as (
    select user_metadata.uid,
           sum(badge.score) as score,
           json_agg(json_build_object('name', badge.name,
                                      'alt', badge.alt,
                                      'icon', badge.icon)) as info
      from user_metadata
           left join badge
               on badge.bid = user_metadata.value::integer
           -- this join is just here to make using where-snip possible
           left join "user" u on u.uid = user_metadata.uid
      :snip:where-snip
        and user_metadata.key = 'badge'
     group by user_metadata.uid
  ),

-- :snip badges-field-snip
  user_badges.info as badges,

-- :snip badges-join-snip
  left join user_badges
      on user_badges.uid = u.uid

-- :snip personal-field-snip
  u.language,
  u.resets,

-- :snip score-cte-snip
  score as (
    select u.uid,
           u.score + coalesce(user_badges.score, 0) as score,
           case when (u.score + coalesce(user_badges.score, 0)) < 0
             then 0
           else trunc(sqrt((u.score + coalesce(user_badges.score, 0)) / 10.0))
           end as level
      from "user" u
           left join user_badges
               on user_badges.uid = u.uid
     :snip:where-snip
  ),

-- :snip score-field-snip
  u.score,
  score.level,
  case when score.score < 0
    then 0.0
  else (
    (score.score - score.level * score.level * 10.0)
    /
    (10.0 * ((score.level + 1) * (score.level + 1) - (score.level * score.level)))
  ) end as progress,

-- :snip score-join-snip
  left join score
  on score.uid = u.uid

-- :snip username-history-field-snip
  (select json_agg(json_build_object('name', name,
                                     'changed', extract(epoch from changed),
                                     'required_by_admin', required_by_admin)
                    order by changed desc)
      from username_history
     where username_history.uid = u.uid
     group by u.uid) as username_history,
