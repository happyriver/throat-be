-- sql/messages.sql -- Message-related SQL queries for throat-be
-- Copyright (C) 2020-2022  The Feminist Conspiracy

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.

-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

-- :name select-messages-by-mid :? :*
-- :doc Get messages by mids
select message.mid,
       message.content,
       message.mtype,
       message.posted,
       message.receivedby,
       message.sentby,
       message.first,
       message.mtid,
       message_thread.sid
  from message
       left join message_thread
           on message_thread.mtid = message.mtid
       left join message as first_message
           on first_message.mtid = message.mtid
           and first_message.first
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message.mtid
 where message.mid in (:v*:mids);

-- :name select-message-thread :? :1
-- :doc Get a single message thread
select message_thread.*,
       first_message.mtype,
       (case when first_message.mtype = :message/USER_TO_MODS
         then first_message.sentby
        else first_message.receivedby end) as target_uid
  from message_thread
       left join message as first_message
           on first_message.mtid = :mtid
           and first_message.first
 where message_thread.mtid = :mtid;

-- :name select-modmail-thread :? :1
-- :doc Get a single modmail message thread
select message_thread.*,
       (case when first_message.mtype = :message/USER_TO_MODS
         then first_message.sentby
        else first_message.receivedby end) as target_uid,
       sub_message_mailbox.mailbox,
       first_message.posted,
       latest_message.posted as latest_posted,
       row_to_json(first_message) as first_message,
       row_to_json(latest_message) as latest_message,
       -- row_to_json(sub_message_log) as log,
       row_to_json(sub_post_report) as post_report,
       row_to_json(sub_post_comment_report) as comment_report
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
       left join sub_message_log
           on sub_message_log.mtid = :mtid
           and sub_message_log.action in (:message/REF_POST_REPORT,
                                          :message/REF_COMMENT_REPORT)
       left join sub_post_report
           on sub_message_log.action = :message/REF_POST_REPORT
           and sub_post_report.id::text = sub_message_log.desc
       left join sub_post_comment_report
           on sub_message_log.action = :message/REF_COMMENT_REPORT
           and sub_post_comment_report.id::text = sub_message_log.desc
 where message_thread.mtid = :mtid;

-- :name select-modmail-threads
-- :doc Get modmail threads ordered by most recent message
select message_thread.*,
       sub_message_mailbox.mailbox,
       first_message.posted,
       latest_message.posted as latest_posted,
       row_to_json(first_message) as first_message,
       row_to_json(latest_message) as latest_message
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
--~             (if (:unread_only params) "inner join" "left join")
                user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
 where message_thread.sid in (:v*:sids)
   and first_message.mtype in (:v*:mtypes)
   and mailbox = :mailbox
--~ (when (:before params) "and latest_message.posted < :before")
 order by latest_message.posted desc
 limit :limit;

-- :name select-new-modmail-threads :? :*
-- :doc Get threads for the modmail "New" mailbox
-- :doc This includes threads from users to mods that don't have replies
-- :doc yet, and user notification threads with just one reply (the user)
select message_thread.*,
       first_message.posted,
       row_to_json(first_message) as first_message,
       latest_message.posted as latest_posted,
       row_to_json(latest_message) as latest_message,
       sub_message_mailbox.mailbox
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
--~ (if (:unread_only params) "inner join" "left join")
                  user_unread_message
                  on user_unread_message.uid = :uid
                  and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                  left join user_unread_message
                      on user_unread_message.uid = :uid
                      and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
 where message_thread.sid in (:v*:sids)
   and mailbox = :message/INBOX
   and ((message_thread.replies = 0
         and first_message.mtype = :message/USER_TO_MODS)
        or (message_thread.replies > 0
            and first_message.mtype = :message/USER_NOTIFICATION
            and not exists (
              select * from message
               where message.mtid = message_thread.mtid
                 and not message.first
                 and message.mtype != :message/USER_TO_MODS
            )
        )
   )
--~ (when (:before params) "and first_message.posted < :before")
order by first_message.posted desc
limit :limit;

-- :name select-in-progress-modmail-threads :? :*
-- :doc Get threads which are either from users with replies, or from mods.
select message_thread.*,
       sub_message_mailbox.mailbox,
       first_message.posted,
       latest_message.posted as latest_posted,
       row_to_json(first_message) as first_message,
       row_to_json(latest_message) as latest_message
  from message_thread
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
--~             (if (:unread_only params) "inner join" "left join")
                user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
          order by posted desc
          limit 1
       ) as latest_message on true
       inner join lateral (
         select message.*,
                user_unread_message.uid as unread
           from message
                left join user_unread_message
                    on user_unread_message.uid = :uid
                    and user_unread_message.mid = message.mid
          where message.mtid = message_thread.mtid
            and message.first
       ) as first_message on true
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message_thread.mtid
 where message_thread.sid in (:v*:sids)
   and ((first_message.mtype = :message/USER_TO_MODS
         and not latest_message.first)
         or first_message.mtype in (:message/MOD_TO_USER_AS_USER,
                                    :message/MOD_TO_USER_AS_MOD)
         or (first_message.mtype = :message/USER_NOTIFICATION
             and not latest_message.first
             and exists (
               select *
                 from message
                where message.mtid = message_thread.mtid
                  and not message.first
                  and message.mtype != :message/USER_TO_MODS)))
   and sub_message_mailbox.mailbox = :message/INBOX
--~ (when (:before params) "and latest_message.posted < :before")
 order by latest_message.posted desc
 limit :limit;

-- :name select-messages-in-thread :? :*
-- :doc Get messages in a conversation
select message.mid,
       message.content,
       message.mtype,
       message.posted,
       message.receivedby,
       message.sentby,
       message.first,
       message.mtid,
       message_thread.sid,
       user_unread_message.uid as unread
  from message
       left join message_thread
           on message_thread.mtid = :mtid
       left join message as first_message
           on first_message.mtid = :mtid
           and first_message.first
       left join user_unread_message
           on user_unread_message.uid = :uid
           and user_unread_message.mid = message.mid
       left join sub_message_mailbox
           on sub_message_mailbox.mtid = message.mtid
 where message.mtid = :mtid
--~ (when (:before params) "and message.posted < :before")
 order by message.posted desc
 limit :limit;

-- :name select-mods-with-notification-counts :? :*
-- :doc Get mods of a sub with their notification counts.
with
  mods as
    (
      select u.uid
        from public.user as u
             left join sub_mod
                 on sub_mod.uid = u.uid
       where sub_mod.sid = :sid
         and not sub_mod.invite
    )
select mods.uid,
       count(distinct(unread_messages.mid)) as messages,
       count(distinct(unread_notifications.id)) as notifications,
       count(distinct(unread_modmails.mid)) as modmails
  from mods
       left join (
         select message.mid, message.receivedby, user_unread_message.id as umb
           from message
                left join user_message_block
                    on user_message_block.uid = message.receivedby
                    and user_message_block.target = message.sentby
                inner join user_unread_message
                    on user_unread_message.uid = message.receivedby
                    and user_unread_message.mid = message.mid
                inner join user_message_mailbox
                    on user_message_mailbox.uid = message.receivedby
                    and user_message_mailbox.mid = message.mid
          where user_message_mailbox.mailbox = :message/INBOX
            and user_message_block.id is null
            and message.mtype = :message/USER_TO_USER
       ) as unread_messages
           on unread_messages.receivedby = mods.uid
       left join (
         select notification.id, notification.receivedby
           from notification
                left join user_content_block
                    on user_content_block.uid = notification.receivedby
                    and user_content_block.target = notification.sentby
                left join sub_mod
                    on sub_mod.uid = notification.sentby
                    and sub_mod.sid = notification.sid
                    and not sub_mod.invite
                left outer join sub_mod as sub_mod_current_user
                    on sub_mod_current_user.uid = notification.receivedby
                    and sub_mod_current_user.sid = notification.sid
                    and not sub_mod_current_user.invite
          where notification.read is null
            and (user_content_block.id is null
                 or not sub_mod.power_level is null
                 or not sub_mod_current_user.power_level is null
                 or not (notification.type in ('POST_REPLY',
                                               'COMMENT_REPLY',
                                               'POST_MENTION',
                                               'COMMENT_MENTION')))
       ) as unread_notifications
           on unread_notifications.receivedby = mods.uid
       left join (
         select msg.mid, user_unread_message.uid
	   from message_thread
                inner join lateral (
                  select * from message
                   where message.mtid = message_thread.mtid
                   order by posted desc
                   limit 1
                ) as msg on true
	        inner join user_unread_message
		    on user_unread_message.mid = msg.mid
                left join sub_message_mailbox
                    on sub_message_mailbox.mtid = msg.mtid
          where message_thread.sid = :sid
            and user_unread_message.mid is not null
            and mailbox = :message/INBOX
       ) as unread_modmails
           on unread_modmails.uid = mods.uid
  group by mods.uid;

-- :name make-message-unread :! :n
-- :doc Insert unread message record if one does not already exist.
insert into user_unread_message (uid, mid)
select uid, mid
  from (values (:uid, :mid)) as to_be_inserted (uid, mid)
 where uid not in (select uid
                     from user_unread_message
                    where uid = :uid and mid = :mid);

-- :name make-thread-unread :! :n
-- :doc Insert unread message records for all messages in thread.
insert into user_unread_message (uid, mid)
select uid, mid
  from (select :uid, message.mid
          from message
               left join user_unread_message
                   on user_unread_message.uid = :uid
                   and user_unread_message.mid = message.mid
         where message.mtid = :mtid
               and user_unread_message.uid is null)
         as to_be_inserted (uid, mid);

-- :name make-message-read :! :n
-- :doc Delete unread message record if one exists.
delete from user_unread_message
 where uid = :uid and mid = :mid;

-- :name make-thread-read :! :n
-- :doc Delete unread message records for all messages in thread.
delete from user_unread_message
 where id in (select id
                from user_unread_message
                     left join message
                         on message.mid = user_unread_message.mid
               where message.mtid = :mtid
                 and user_unread_message.uid = :uid);

-- :name change-mailbox :! :n
-- :doc Change mailbox for a modmail conversation.
with
  log_entry as
    (
      insert into sub_message_log (action, mtid, "desc", uid, updated)
      values (:message/CHANGE_MAILBOX, :mtid, :mailbox::text, :uid, :updated)
      )
update sub_message_mailbox
   set mailbox = :mailbox
 where mtid = :mtid

-- :name insert-new-modmail-thread-returning :<!
-- :doc Insert a modmail message, creating a thread, mailboxes and unreads for it
with
  thread as
    (
      insert into message_thread (replies, subject, sid)
      values (0, :subject, :sid)
      returning *
    ),
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content, :mtype, :posted, :receivedby, :sentby, True, (select mtid from thread))
      returning *
    ),
  sub_mbox as
    (
      insert into sub_message_mailbox (mtid, mailbox, highlighted)
      values ((select mtid from thread), :message/INBOX, False)
    ),
  recipient_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select msg.mid, :message/INBOX as mailbox, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  --~ (when (:report-type params) ":snip:report-snip")
  recipient_unread as
    (
      insert into user_unread_message (mid, uid)
      select mid, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  other_mods_unread as
    (
      insert into user_unread_message (uid, mid)
      select uid, (select mid from msg) from sub_mod
       where sid = :sid
         and uid <> :sentby
    )
select msg.mid, msg.content, msg.mtype, msg.posted, msg.receivedby, msg.sentby, :sid as sid,
       msg.mtid
  from msg;

-- :name insert-contact-mods-thread-returning :<!
-- :doc Insert a modmail message from a user, creating a thread, mailboxes and unreads for it
with
  thread as
    (
      insert into message_thread (replies, subject, sid)
      values (0, :subject, :sid)
      returning *
    ),
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content,
              :message/USER_TO_MODS,
              :posted,
              :receivedby,
              :sentby,
              True,
              (select mtid from thread))
      returning *
    ),
  sub_mbox as
    (
      insert into sub_message_mailbox (mtid, mailbox, highlighted)
      values ((select mtid from thread), :message/INBOX, False)
    ),
  sender_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select mid, :message/SENT as mailbox, :sentby as uid
        from msg
       where :sentby::text is not null
    ),
  mods_unread as
    (
      insert into user_unread_message (uid, mid)
      select uid, (select mid from msg) from sub_mod
       where sid = :sid
      )
select msg.mid, msg.content, msg.mtype, msg.posted,
       msg.receivedby, msg.sentby, :sid as sid,
       msg.mtid
  from msg;

-- :name insert-modmail-reply-returning :<!
-- :doc Insert a modmail message reply, creating a thread for it
with
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content, :mtype, :posted, :receivedby, :sentby, False, :mtid)
      returning *
    ),
  replies as
    (
      update message_thread
         set replies = replies + 1
       where mtid = :mtid
      ),
  recipient_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select msg.mid, :message/INBOX as mailbox, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  recipient_unread as
    (
      insert into user_unread_message (mid, uid)
      select mid, :receivedby as uid
        from msg
       where :receivedby::text is not null
    ),
  other_mods_unread as
    (
      insert into user_unread_message (uid, mid)
      select uid, (select mid from msg) from sub_mod
       where sid = :sid
         and uid <> :sentby
      )
select msg.mid, msg.content, msg.mtype, msg.posted,
       msg.receivedby, msg.sentby, :sid as sid, msg.mtid
  from msg;

-- :name insert-message-thread-returning :<!
-- :doc Insert a message, creating a thread, mailboxes and unreads for it
with
  thread as
    (
      insert into message_thread (replies, subject, sid)
      values (0, :subject, null)
      returning *
    ),
  msg as
    (
      insert into message (content, mtype, posted, receivedby, sentby, first, mtid)
      values (:content,
              :message/USER_TO_USER,
              :posted,
              :receivedby,
              :sentby,
              True,
              (select mtid from thread))
      returning *
    ),
  sender_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select mid, :message/SENT as mailbox, :sentby
        from msg
    ),
  recipient_mbox as
    (
      insert into user_message_mailbox (mid, mailbox, uid)
      select mid, :message/INBOX as mailbox, :receivedby
        from msg
    ),
  recipient_unread as
    (
      insert into user_unread_message (uid, mid)
      select :receivedby, mid
        from msg
    )
select msg.mid, msg.content, msg.mtype, msg.posted,
       msg.receivedby, msg.sentby, null as sid,
       msg.mtid
  from msg;

-- :name select-post-report :? :*
-- :doc "Return a post report matching id, sid and username if it exists.""
select id
  from sub_post_report
       left join sub_post
           on sub_post.pid = sub_post_report.pid
       left join "user" as reporter
           on reporter.uid = sub_post_report.uid
       left join "user" as target
           on target.uid = sub_post.uid
 where id = :report_id
   and sub_post.sid = :sid
   and (reporter.name = :name
        or target.name = :name);

-- :name select-comment-report :? :*
-- :doc "Return a comment report matching id, sid and username if it exists.""
select id
  from sub_post_comment_report
       left join sub_post_comment
           on sub_post_comment.cid = sub_post_comment_report.cid
       left join sub_post
           on sub_post.pid = sub_post_comment.pid
       left join "user" as reporter
           on reporter.uid = sub_post_comment_report.uid
       left join "user" as target
           on target.uid = sub_post_comment.uid
 where id = :report_id
   and sub_post.sid = :sid
   and (reporter.name = :name
        or target.name = :name);

--:snip create-report-logs-snip
  post_rpt_log as
    (
      insert into post_report_log (id, action, "desc", time, uid)
      select :report-id as id,
             (case
              when :receivedby = (select uid
                                    from sub_post_report
                                   where id = :report-id)
                then :report-log/MODMAIL_TO_REPORTER
              else :report-log/MODMAIL_TO_REPORTED_USER
              end) as action,
             msg.mtid::text as "desc",
             msg.posted as time,
             msg.sentby as uid
        from msg
       where :report-type = 'POST'
    ),
  comment_rpt_log as
    (
      insert into comment_report_log (id, action, "desc", time, uid)
      select :report-id as id,
             (case
              when :receivedby = (select uid
                                    from sub_post_comment_report
                                   where id = :report-id)
                then :report-log/MODMAIL_TO_REPORTER
              else :report-log/MODMAIL_TO_REPORTED_USER
              end) as action,
             msg.mtid::text as "desc",
             msg.posted as time,
             msg.sentby as uid
        from msg
       where :report-type = 'COMMENT'
    ),
  sub_msg_log as
    (
      insert into sub_message_log (action, mtid, uid, "desc", updated)
      select (case
              when :report-type = 'POST'
                then :message/REF_POST_REPORT
              else :message/REF_COMMENT_REPORT
              end) as action,
             msg.mtid,
             msg.sentby as uid,
             :report-id::text as "desc",
             msg.posted as updated
        from msg
    ),
