;; util_test.clj -- Testing utility functions for throat-be
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.util-test
  (:require [clojure.math :as math]
            [clojure.test :refer [deftest is]]
            [throat-be.util :as util]))

(deftest test-best-score
  (let [examples [[[ 1  1  1]  0.2065]
                  [[20  1 25]  0.6087]
                  [[20 20 25]  0.0071]
                  [[1  20 25] -0.5242]]]
    (doseq [[args result] examples]
      (let [val (apply util/best-score args)]
        (is (= result (-> (* val 10000.0)
                          math/round
                          (/ 10000.0))))))
    ;; Ensure new comments have the same score whether self-voting
    ;; is turned on or off.
    (is (= (util/best-score 1 1 1)
           (util/best-score 0 0 0)))))
