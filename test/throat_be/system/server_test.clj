;; system/server_test.clj -- Testing http server for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.system.server-test
  (:require [clojure.core.async :refer [close!]]
            [clojure.test :refer [deftest testing is use-fixtures]]
            [com.stuartsierra.component :as component]
            [throat-be.resolver-helpers.stream :as stream]
            [throat-be.test-utils :refer [test-system-config-map
                                          test-system-map
                                          test-system
                                          add-queries hash-query
                                          m:create-user
                                          open-websocket with-websocket
                                          send-data send-init expect-message]]))

(def ^:dynamic ^:private *system*)

(def s:quark "subscription { quark }")
(def q:current-user "{ current_user { name } }")

(defn get-quark [] "strange")

(defn stream-quark
  [_context _args callback-fn]
  (let [action (stream/action-at-intervals #(callback-fn (get-quark)) 5)]
    #(close! action)))

(defn system-map
  []
  (-> (test-system-config-map)
      (add-queries [s:quark q:current-user])
      (assoc-in [:schema-provider :extras :schema :subscriptions :quark]
                {:type :String
                 :stream :stream-quark})
      (assoc-in [:schema-provider :extras :streamers :stream-quark]
                stream-quark)
      test-system-map))

;; For speed, all the tests in this file share a database.
;; The stats test counts those things, so all adding of items
;; to the database should be done in the fixtures.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (component/start-system (test-system (system-map)))]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(deftest test-ws-endpoint-response
  (testing "websocket endpoint responds"
    (with-websocket id (open-websocket)
      (send-init)
      (expect-message {:type "connection_ack"}))))

(deftest test-quark-endpoint
  (testing "websocket endpoint can subscribe"
    (with-websocket id (open-websocket)
      (send-init)
      (expect-message {:type "connection_ack"})
      (send-data {:id id
                  :type :start
                  :payload {:query (hash-query s:quark)}})
      (expect-message {:id id
                       :type "data"
                       :payload {:data {:quark "strange"}}}))))

(deftest test-no-ws-query
  (testing "websocket endpoint can't make query"
    (with-websocket id (open-websocket)
      (send-init)
      (expect-message {:type "connection_ack"})
      (send-data {:id id
                  :type :start
                  :payload {:query (hash-query q:current-user) :variables {}}})
      (expect-message {:id id
                       :type "error"
                       :payload {:message "Bad Request"}}))))

(deftest test-no-ws-mutation
  (testing "websocket endpoint can't make mutation"
    (with-websocket id (open-websocket)
      (send-init)
      (expect-message {:type "connection_ack"})
      (send-data {:id id
                  :type :start
                  :payload {:query (hash-query m:create-user)
                            :variables {:name "foo"}}})
      (expect-message {:id id
                       :type "error"
                       :payload {:message "Bad Request"}}))))
