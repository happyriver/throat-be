;; elements/site/resolve_test.clj -- Tests for site queries for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.site.resolve-test
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.set :as set]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [dotenv]
   [throat-be.protocols.visitor-counter :as visitor-counter]
   [throat-be.system :as system]
   [throat-be.test-utils :refer [send-request
                                 test-system
                                 test-system-map
                                 test-system-config-map
                                 add-queries
                                 request-cookies] :as utils]
   [throat-be.util :as util]))

(def ^:dynamic ^:private *system*)
(def ^:dynamic ^:private *ids*)

(declare q:stats-all-time q:stats-timespan q:get-counts
         q:banned-username-strings m:ban-string-in-usernames
         m:unban-string-in-usernames q:invite-code-settings
         m:set-invite-code-settings q:invite-codes
         m:generate-invite-code m:generate-invite-code-id
         m:change-invite-code-expiration)

(defn system-map []
  (let [tasks (filter #(= :visitor-count-update (:task-name %))
                      system/taskrunner-tasks)
        defaults (:defaults (first tasks))
        options (assoc defaults
                       :interval 100
                       :batch-size 10000
                       :batch-limit 100000
                       :initial-delay 0)
        config {:uri (dotenv/env :REDIS_URL)
                :parent (util/rand-str 6)
                :tasks [(assoc (first tasks) :options options)]}]
    (-> (test-system-config-map)
        (add-queries [q:stats-all-time q:stats-timespan
                      q:get-counts q:banned-username-strings
                      m:ban-string-in-usernames m:unban-string-in-usernames
                      q:invite-code-settings m:set-invite-code-settings
                      q:invite-codes m:generate-invite-code
                      m:generate-invite-code-id
                      m:change-invite-code-expiration])
        (assoc :tasks config)
        (test-system-map #{:webserver :taskrunner}))))

;; For speed, all the tests in this file share a database.  The stats
;; test counts those things, so all adding of users, subs posts,
;; comments, and votes to the database should be done in the fixtures.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (component/start-system (test-system (system-map)))]
      (try
        (binding [*ids* (utils/mod-user-and-sub *system*)]
          (test-fn))
        (finally
          (component/stop-system *system*))))))

(def q:stats-all-time "
query {
  site_stats {
    users
    subs
    posts
    comments
    upvotes
    downvotes
  }
}")

(def q:stats-timespan "
query ($since: String, $until: String) {
  site_stats(since: $since, until: $until) {
    users
    subs
    posts
    comments
    upvotes
    downvotes
    users_who_posted
    users_who_commented
    users_who_voted
  }
}")

(defn make-epoch-ms [y m d]
  (str (.getTime (java.util.Date. (- y 1900)
                                  (- m 1)
                                  d))))

(deftest test-stats-query
  (testing "site stats query"
    (let [{:keys [user-uid admin-uid]} *ids*]
      (testing "fails for anonymous users"
        (let [response (send-request q:stats-all-time {})]
          (is (= (:status response) 403))
          (is (= (get-in response [:body :errors 0 :message])
                 "Not authorized"))))
      (testing "fails for non-admin user"
        (let [response (send-request q:stats-all-time {}
                                     (request-cookies user-uid))]
          (is (= (:status response) 403))
          (is (= (get-in response [:body :errors 0 :message])
                 "Not authorized"))))
      (testing "gives results for all dates"
        (let [response (send-request q:stats-all-time {}
                                     (request-cookies admin-uid))]
          (is (= (:status response) 200))
          (is (nil? (get-in response [:body :errors])))
          (is (= {:users 4
                  :subs 2
                  :posts 0
                  :comments 0
                  ;; These nils are here because there are no posts
                  ;; or comments yet.
                  :upvotes nil
                  :downvotes nil}
                 (get-in response [:body :data :site_stats])))))
      (testing "gives results for specified dates"
        (let [since (make-epoch-ms 2021 1 1)
              until (make-epoch-ms 2021 1 2)
              response (send-request q:stats-timespan
                                     {:since since :until until}
                                     (request-cookies admin-uid))]
          (is (= (:status response) 200))
          (is (= (get-in response [:body :data :site_stats])
                 {:users 0,
                  :subs 0,
                  :posts 0,
                  :comments 0,
                  :upvotes 0,
                  :downvotes 0
                  :users_who_posted 0
                  :users_who_commented 0
                  :users_who_voted 0})))))))

;; As long as this number is kept low enough that each week has around
;; 150-ish unique visitors, the count will be exact.
(def generate-count 1000)
(def days 30)
(def day-ms (* 24 60 60 1000))
(def timespan (* 24 60 60 days))
(def end-date (java.util.Date. 121 7 17))
(def begin-date (java.util.Date. (- (.getTime end-date)
                                    (* 1000 timespan))))
(def seed (atom 0))

(defn pseudo-rand-int
  "Make some noise, but the same noise every time."
  [limit]
  (let [next-int (.nextInt (java.util.Random. @seed))]
    (reset! seed next-int)
    (mod (Math/abs next-int) limit)))

(defn generate-ip-addr
  []
  (str "192.168.100."  (pseudo-rand-int 256)))

(defn generate-timestamp
  []
  (let [now (.getTime end-date)
        then (- now (* 1000 (pseudo-rand-int timespan)))]
    (java.sql.Timestamp. then)))

(defn generate-visitors
  []
  (map (fn [_] {:ipaddr (generate-ip-addr)
                :timestamp (generate-timestamp)})
       (range generate-count)))

(defn insert-visitors
  [vc visitors]
  (let [before-count (visitor-counter/visitor-count vc)
        total (+ before-count (count visitors))]
    (doseq [{:keys [ipaddr timestamp]} visitors]
      (visitor-counter/add! vc ipaddr timestamp))
    ;; Wait until all records have been added to the database.
    (loop [limit 0]
      (Thread/sleep 50)
      (when (or (> limit 100)
                (< (visitor-counter/visitor-count vc) total))
        (recur (inc limit))))))

(comment
  (def visitors (generate-visitors)))

(defn daystart-from-timestamp [ts]
  (let [java-date (java.util.Date. (.getTime ts))
        daystart (java.util.Date. (.getYear java-date)
                                  (.getMonth java-date)
                                  (.getDate java-date))]
    (.getTime daystart)))

(defn visitor-sets
  "Process the visitor list into a map from days (represented as
  longs) to sets of ip addresses."
  [visitors]
  (let [day-visitors (map (fn [{:keys [ipaddr timestamp]}]
                            {:start (daystart-from-timestamp timestamp)
                             :ipaddr ipaddr})
                          visitors)
        add-to-set (fn [previous ipaddr]
                     (if previous
                       (conj previous ipaddr)
                       #{ipaddr}))]
    (reduce (fn [wip {:keys [start ipaddr]}]
              (update wip start #(add-to-set % ipaddr)))
            {} day-visitors)))

(def q:get-counts "
query ($since: String, $until: String, $by: Int) {
  site_visitor_counts(since: $since, until: $until, by: $by) {
    start
    count
  }
}")

(deftest test-visitor-counts
  (reset! seed 0)
  (testing "visitor counts"
    (let [vc (:visitor-counter *system*)
          ;; Quiet a warning.
          vc-with-counter (assoc-in vc [:db :counter] (atom 0))
          {:keys [user-uid admin-uid]} *ids*
          visitors (generate-visitors)
          visitors-by-day (visitor-sets visitors)]
      (insert-visitors vc-with-counter visitors)
      ;; 5x more than the 100ms update interval ought to be
      ;; enough.
      (Thread/sleep 500)
      (testing "fetch query"
        (let [since (.getTime begin-date)
              day-args {:since (str since)
                        :until (str (+ since (* 7 day-ms)))
                        :by 1}
              week-args {:since (str since)
                         :until (str (+ since (* 28 day-ms)))
                         :by 7}]
          (testing "fails for anonymous users"
            (let [response (send-request q:get-counts day-args)]
              (is (= 403 (:status response)))
              (is (= "Not authorized"
                     (get-in response [:body :errors 0 :message])))
              (is (empty? (get-in response [:body :data
                                            :site_visitor_counts])))))
          (testing "fails for non-admin user"
            (let [response (send-request q:get-counts week-args
                                         (request-cookies user-uid))]
              (is (= 403 (:status response)))
              (is (= "Not authorized"
                     (get-in response [:body :errors 0 :message])))
              (is (empty? (get-in response [:body :data
                                            :site_visitor_counts])))))
          (testing "gets daily results"
            (let [response (send-request q:get-counts day-args
                                         (request-cookies admin-uid))
                  counts (get-in response [:body :data
                                           :site_visitor_counts])
                  days (map #(+ since (* % day-ms)) (range 7))
                  expected (map (fn [day]
                                  {:start (str day)
                                   :count (-> (get visitors-by-day day)
                                              count)})
                                days)]
              (is (= 200 (:status response)))
              (is (nil? (get-in response [:body :errors])))
              (is (= 7 (count counts)))
              (is (= expected counts))))
          (testing "gets weekly results"
            (let [response (send-request q:get-counts week-args
                                         (request-cookies admin-uid))
                  counts (get-in response [:body :data
                                           :site_visitor_counts])
                  weeks (map #(+ since (* % 7 day-ms)) (range 4))
                  day-sets (fn [week]
                             (let [days (map #(+ week (* % day-ms)) (range 7))]
                               (map #(get visitors-by-day %) days)))
                  expected (map (fn [week]
                                  {:start (str week)
                                   :count (->> (day-sets week)
                                               (apply set/union)
                                               count)})
                                weeks)]
              (is (= 200 (:status response)))
              (is (nil? (get-in response [:body :errors])))
              (is (= 4 (count counts)))
              (is (= expected counts)))))))))


(def q:banned-username-strings "
query {
  banned_username_strings {
    banned
    users
  }
}")

(def m:ban-string-in-usernames "
mutation ($banned: String!) {
  ban_string_in_usernames(banned: $banned) {
    banned
    users
  }
}")

(def m:unban-string-in-usernames "
mutation ($banned: String!) {
  unban_string_in_usernames(banned: $banned)
}")

(deftest test-banned-username-strings
  (let [{:keys [user-uid user-name admin-uid admin-name mod-name
                mod2-name]} *ids*
        simplify (fn [elems]
                   (->> elems
                        (map (fn [elem] [(:banned elem)
                                         (set (:users elem))]))
                        (into {})))]
    (testing "banned username strings"
      (testing "can be added by an admin"
        (let [ response (send-request m:ban-string-in-usernames {:banned "zzz"}
                                      (request-cookies admin-uid))]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (is (= {:ban_string_in_usernames {:banned "zzz"
                                            :users []}}
                 (get-in response [:body :data])))))
      (testing "when added return the list of affected usernames"
        (let [response (send-request m:ban-string-in-usernames {:banned "test"}
                                     (request-cookies admin-uid))
              result (-> (get-in response
                                 [:body :data :ban_string_in_usernames])
                         list
                         simplify)]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (is (= {"test" #{user-name mod-name mod2-name admin-name}} result))))
      (testing "cannot be added by a non-admin"
        (let [ response (send-request m:ban-string-in-usernames {:banned "ban"}
                                      (request-cookies user-uid))]
          (is (= 403 (:status response)))))
      (testing "do not accept non-alphanumeric characters or empty strings"
        (doseq [ban ["" "100%" "bad_user_name"]]
          (let [ response (send-request m:ban-string-in-usernames {:banned ban}
                                        (request-cookies admin-uid))]
            (is (= 400 (:status response))))))
      (testing "can be queried with user names by an admin"
        (let [response (send-request q:banned-username-strings {}
                                     (request-cookies admin-uid))
              result (-> response
                         (get-in [:body :data :banned_username_strings])
                         simplify)]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (is (= {"zzz" #{}
	          "test" #{user-name mod-name mod2-name admin-name}}
                 result))))
      (testing "cannot be queried by a non-admin"
        (let [response (send-request q:banned-username-strings {}
                                     (request-cookies user-uid))]
          (is (= 403 (:status response)))))
      (testing "can be removed by an admin"
        (let [response (send-request m:unban-string-in-usernames {:banned "test"}
                                     (request-cookies admin-uid))]
          (is (= 200 (:status response)))
          (is (= {:unban_string_in_usernames "test"}
                 (get-in response [:body :data])))))
      (testing "cannot be removed by an non-admin"
        (let [response (send-request m:unban-string-in-usernames {:banned "zzz"}
                                     (request-cookies user-uid))]
          (is (= 403 (:status response)))))
      (testing "can be queried after a removal"
        (let [response (send-request q:banned-username-strings {}
                                     (request-cookies admin-uid))]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (is (= {:banned_username_strings
	          [{:banned "zzz", :users nil}]}
                 (get-in response [:body :data]))))))))

(def q:invite-code-settings "
query {
  invite_code_settings {
    required
    visible
    minimum_level
    per_user
  }
}")

(def m:set-invite-code-settings "
mutation ($required: Boolean!, $visible: Boolean!, $minimum_level: Int!,
          $per_user: Int!) {
  set_invite_code_settings(required: $required, visible: $visible,
    minimum_level: $minimum_level, per_user: $per_user) {
    required
    visible
    minimum_level
    per_user
  }
}")

(deftest test-invite-code-settings
  (let [{:keys [user-uid admin-uid]} *ids*]
    (testing "invite code settings"
      (testing "can be fetched by an admin"
        (let [ response (send-request q:invite-code-settings {}
                                      (request-cookies admin-uid))]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (let [{:keys [required visible minimum_level per_user]}
                (get-in response [:body :data :invite_code_settings])]
            (is (boolean? required))
            (is (boolean? visible))
            (is (int? minimum_level))
            (is (int? per_user))
            (testing "and can be changed by an admin"
              (let [response (send-request m:set-invite-code-settings
                                           {:required (not required)
                                            :visible (not visible)
                                            :minimum_level (+ minimum_level 5)
                                            :per_user (+ per_user 10)}
                                           (request-cookies admin-uid))]
                (is (= 200 (:status response)))
                (is (nil? (get-in response [:body :errors])))
                (let [data (get-in response [:body :data
                                             :set_invite_code_settings])]
                  (is (= required (not (:required data))))
                  (is (= visible (not (:visible data))))
                  (is (= (+ minimum_level 5) (:minimum_level data)))
                  (is (= (+ per_user 10) (:per_user data)))))))))
      (testing "can be fetched by a non-admin"
        (let [response (send-request q:invite-code-settings {}
                                     (request-cookies user-uid))]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (let [{:keys [required visible minimum_level per_user]}
                (get-in response [:body :data :invite_code_settings])]
            (is (boolean? required))
            (is (boolean? visible))
            (is (int? minimum_level))
            (is (int? per_user)))))
      (testing "can not be changed by a non-admin"
        (let [response (send-request m:set-invite-code-settings
                                     {:required true
                                      :visible true
                                      :minimum_level 20
                                      :per_user 20}
                                     (request-cookies user-uid))]
          (is (= 403 (:status response))))))))

(def m:generate-invite-code "
mutation ($code: String!, $max_uses: Int, $expires: String) {
  generate_invite_code(code: $code, max_uses: $max_uses,
                       expires: $expires) {
    id
    code
    created_by {
      name
    }
    created_on
    expires
    used_by {
      name
      status
    }
    uses
    max_uses
  }
}")

(def q:invite-codes "
query ($first: Int!, $after: String, $search: String) {
  invite_codes(first: $first, after: $after, search: $search) {
    edges {
      node {
        code
        created_by {
          name
        }
        expires
        used_by {
          name
          status
        }
        uses
        max_uses
      }
    }
    pageInfo {
      hasNextPage
      endCursor
    }
  }
}")

(deftest test-invite-codes-non-admins
  (let [{:keys [user-uid]} *ids*]
    (testing "non-admins"
      (testing "can not generate invite codes"
        (let [response (send-request m:generate-invite-code
                                     {:code ""
                                      :max_uses 20}
                                     (request-cookies user-uid))]
          (is (= 403 (:status response)))))
      (testing "can not query for invite codes"
        (let [response (send-request q:invite-codes
                                     {:first 20}
                                     (request-cookies user-uid))]
          (is (= 403 (:status response))))))))

(deftest test-generate-query-invite-codes
  (jdbc/execute! (:ds (:db *system*)) ["DELETE FROM invite_code"])
  (let [{:keys [admin-uid admin-name]} *ids*
        now (.getTime (java.util.Date.))
        expiretime (+ now (* 1000 60 60))
        expires (str expiretime)
        result1 {:code "new-code"
                 :created_by {:name admin-name}
                 :expires nil
                 :used_by []
                 :uses 0
                 :max_uses 20}
        result2 {:code "another-code"
                 :created_by {:name admin-name}
                 :expires expires
                 :used_by []
                 :uses 0
                 :max_uses 11}]
    (testing "create a specified invite code"
      (let [response (send-request m:generate-invite-code
                                   {:code (:code result1)
                                    :max_uses (:max_uses result1)}
                                   (request-cookies admin-uid))
            errors (get-in response [:body :errors])
            data (get-in response [:body :data :generate_invite_code])]
        (is (nil? errors))
        (is (= 200 (:status response)))
        (is (some? (:id data)))
        (is (> expiretime (Long/parseLong (:created_on data)) now))
        (is (= result1 (dissoc data :id :created_on)))))
    (testing "create an invite code with an expiration date"
      (let [response (send-request m:generate-invite-code
                                   {:code (:code result2)
                                    :expires expires
                                    :max_uses (:max_uses result2)}
                                   (request-cookies admin-uid))
            errors (get-in response [:body :errors])
            data (get-in response [:body :data :generate_invite_code])]
        (is (nil? errors))
        (is (= 200 (:status response)))
        (is (some? (:id data)))
        (is (> expiretime (Long/parseLong (:created_on data)) now))
        (is (= result2 (dissoc data :id :created_on)))))
    (testing "create some random invite codes"
      (let [responses
            (for [i (range 8)]
              (send-request m:generate-invite-code
                            {:code ""
                             :max_uses (+ i 10)}
                            (request-cookies admin-uid)))
            codes (map #(-> (get-in % [:body :data :generate_invite_code])
                            (dissoc :id :created_on))
                       responses)
            page1 (reverse (take-last 5 codes))
            page2 (reverse (into [result1 result2] (take 3 codes)))]
        (is (every? #(nil? (get-in % [:body :errors])) responses))
        (is (every? #(= (:status %) 200) responses))
        (is (= 8 (count (set (map :code codes)))))
        (testing "and query for them with pagination"
          (let [response (send-request q:invite-codes {:first 5}
                                       (request-cookies admin-uid))
                errors (get-in response [:body :errors])
                data (get-in response [:body :data :invite_codes])
                cursor (get-in data [:pageInfo :endCursor])]
            (is (nil? errors))
            (is (= 200 (:status response)))
            (is (get-in data [:pageInfo :hasNextPage]))
            (is (= page1 (map :node (:edges data))))
            (let [response (send-request q:invite-codes {:first 5 :after cursor}
                                         (request-cookies admin-uid))
                  errors (get-in response [:body :errors])
                  data (get-in response [:body :data :invite_codes])]
              (is (nil? errors))
              (is (= 200 (:status response)))
              (is (false? (get-in data [:pageInfo :hasNextPage])))
              (is (= page2 (map :node (:edges data)))))))
        (testing "and query for ones matching a code"
          (let [response (send-request q:invite-codes {:first 5
                                                       :search "new-code"}
                                       (request-cookies admin-uid))
                errors (get-in response [:body :errors])
                data (get-in response [:body :data :invite_codes])]
            (is (nil? errors))
            (is (= 200 (:status response)))
            (is (false? (get-in data [:pageInfo :hasNextPage])))
            (is (= [result1] (map :node (:edges data))))))))))

(def m:generate-invite-code-id "
mutation ($code: String!, $max_uses: Int, $expires: String) {
  generate_invite_code(code: $code, max_uses: $max_uses,
                       expires: $expires) {
    id
  }
}")

(def m:change-invite-code-expiration "
mutation ($codes: [String!], $expires: String) {
  expire_invite_codes(codes: $codes, expires: $expires)
}")

(deftest test-change-invite-code-expirations
  (jdbc/execute! (:ds (:db *system*)) ["DELETE FROM invite_code"])
  (let [{:keys [user-uid admin-uid]} *ids*
        now (.getTime (java.util.Date.))
        expiretime (+ now (* 1000 60 60))
        expires (str expiretime)
        code-id1 (get-in (send-request m:generate-invite-code-id
                                       {:code "new-code"
                                        :max_uses 10}
                                       (request-cookies admin-uid))
                         [:body :data :generate_invite_code :id])

        code-id2 (get-in (send-request m:generate-invite-code-id
                                       {:code "another-code"
                                        :expires (str now)
                                        :max_uses 10}
                                       (request-cookies admin-uid))
                         [:body :data :generate_invite_code :id])]
    (is (int (Integer/parseInt code-id1)))
    (is (int (Integer/parseInt code-id2)))
    (testing "non-admins cannot change invite code expirations"
      (let [response (send-request m:change-invite-code-expiration
                                   {:codes [code-id1 code-id2]}
                                   (request-cookies user-uid))]
        (is (= 403 (:status response)))))
    (testing "check for validity of error code ids"
      (let [response (send-request m:change-invite-code-expiration
                                   {:codes ["abc" "def"]}
                                   (request-cookies admin-uid))]
        (is (= 400 (:status response)))))
    (testing "changing invite code expirations validates the list"
      (let [response (send-request m:change-invite-code-expiration
                                   {:codes []}
                                   (request-cookies admin-uid))
            {:keys [errors]} (:body response)]
        (is (= 400 (:status response)))
        (is (= "No invite code ids provided"
               (get-in errors [0 :message])))))
    (testing "admins can change invite code expirations"
      (let [response (send-request m:change-invite-code-expiration
                                   {:codes [code-id1 code-id2]
                                    :expires expires}
                                   (request-cookies admin-uid))]
        (is (= 200 (:status response)))
        (is (nil? (get-in response [:body :errors])))
        (is (= #{code-id1 code-id2}
               (set (get-in response [:body :data :expire_invite_codes]))))
        (testing "and verify the change"
          (let [response (send-request q:invite-codes {:first 5}
                                       (request-cookies admin-uid))
                errors (get-in response [:body :errors])
                data (get-in response [:body :data :invite_codes])
                codes (map :node (:edges data))]
            (is (nil? errors))
            (is (= 200 (:status response)))
            (is (not (get-in data [:pageInfo :hasNextPage])))
            (is (= 2 (count codes)))
            (is (every? #(= expires %) (map :expires codes)))))))))
