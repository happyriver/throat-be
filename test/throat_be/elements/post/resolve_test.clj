;; elements/post/resolve_test.clj -- Testing post functionality for throat-be
;; Copyright (C) 2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.post.resolve-test
  (:require
   [clojure.java.jdbc :as jdbc]
   [clojure.string :as str]
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [dotenv :refer [env]]
   [taoensso.carmine :as car]
   [throat-be.test-utils :refer [send-request] :as utils]
   [throat-be.util :as util]))

(def ^:dynamic ^:private *system*)

(declare q:get-single-post q:get-single-post-anon
         q:get-single-post-saved m:save-post m:create-sub-post-flair
         m:set-post-flair m:remove-post-flair m:update-post-title
         m:update-post-content q:get-single-post-history
         m:create-test-post-report q:get-single-post-with-reports
         m:create-test-post-viewed q:get-single-post-viewed
         m:update-post-viewed m:distinguish-post q:get-post-distinguish
         m:set-post-nsfw q:get-post-nsfw s:notification-counts
         q:modmail-threads m:update-flair)

(defn system-map []
  (-> (utils/test-system-config-map)
      (utils/add-queries
       [q:get-single-post q:get-single-post-anon
        q:get-single-post-saved m:save-post m:create-sub-post-flair
        m:set-post-flair m:remove-post-flair m:update-post-title
        m:update-post-content q:get-single-post-history
        m:create-test-post-report q:get-single-post-with-reports
        m:create-test-post-viewed q:get-single-post-viewed
        m:update-post-viewed m:distinguish-post q:get-post-distinguish
        m:set-post-nsfw q:get-post-nsfw s:notification-counts
        q:modmail-threads m:update-flair])
      utils/test-system-map))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (-> (system-map)
                           utils/test-system
                           component/start-system)]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(def q:get-single-post "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    content
    status
    link
    nsfw
    posted
    edited
    is_archived
    type
    score
    upvotes
    downvotes
    thumbnail
    title
    slug
    flair
    author_flair
    distinguish
    best_sort_enabled
    default_sort
    comment_count
    author {
      name
      status
    }
    user_attributes {
      vote
    }
  }
}
")

(def q:get-single-post-anon "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    content
    status
    link
    nsfw
    posted
    edited
    is_archived
    type
    score
    upvotes
    downvotes
    thumbnail
    title
    slug
    flair
    author_flair
    distinguish
    best_sort_enabled
    default_sort
    comment_count
    author {
      name
      status
    }
  }
}
")

(deftest test-create-text-post
  (let [{:keys [user-name user-uid sid]} (utils/mod-user-and-sub *system*)
        before (.getTime (java.util.Date.))
        rsp (send-request utils/m:create-test-post
                          {:title "Test post"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        after (.getTime (java.util.Date.))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "anon user can query a post"
      (let [rsp (send-request q:get-single-post-anon
                              {:pid pid})
            {:keys [data errors]} (:body rsp)
            posted (get-in data [:post_by_pid :posted])]
        (is (= 200 (:status rsp)))
        (is (< before (Long/parseLong posted) after))
        (is (nil? errors))
        (is (= {:post_by_pid {:content "testify"
                              :posted posted
	                      :is_archived false
	                      :type "TEXT"
	                      :title "Test post"
                              :slug "test-post"
	                      :nsfw false
	                      :edited nil
	                      :author {:name user-name
                                       :status "ACTIVE"}
	                      :author_flair nil
	                      :pid pid
	                      :upvotes 0
	                      :downvotes 0
	                      :thumbnail nil
	                      :status "ACTIVE"
	                      :comment_count 0
	                      :link nil
	                      :score 1
	                      :flair nil
	                      :distinguish nil
                              :best_sort_enabled true
                              :default_sort "BEST"}}
               data))))
    (testing "author can query a post"
      (let [rsp (send-request q:get-single-post
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)
            posted (get-in data [:post_by_pid :posted])]
        (is (= 200 (:status rsp)))
        (is (< before (Long/parseLong posted) after))
        (is (nil? errors))
        (is (= {:post_by_pid {:content "testify"
                              :posted posted
	                      :is_archived false
	                      :type "TEXT"
	                      :title "Test post"
                              :slug "test-post"
	                      :nsfw false
	                      :edited nil
	                      :author {:name user-name
                                       :status "ACTIVE"}
	                      :author_flair nil
	                      :pid pid
	                      :upvotes 0
	                      :downvotes 0
	                      :thumbnail nil
	                      :status "ACTIVE"
	                      :comment_count 0
	                      :link nil
	                      :score 1
	                      :flair nil
	                      :distinguish nil
                              :best_sort_enabled true
                              :default_sort "BEST"
                              :user_attributes {:vote nil}}}
               data))))))

(deftest test-query-user-deleted-post
  (let [{:keys [admin-uid mod-uid mod2-uid
                user-name user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test deleted post"
                           :content "test post content"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        {:keys [pid posted]} (get-in rsp [:body :data :create_post])
        post {:pid pid
              :content "test post content"
	      :title "Test deleted post"
              :slug "test-deleted-post"
              :posted posted
	      :is_archived false
	      :type "TEXT"
	      :nsfw false
	      :edited nil
	      :author {:name user-name
                       :status "ACTIVE"}
	      :author_flair nil
	      :upvotes 0
	      :downvotes 0
	      :thumbnail nil
	      :comment_count 0
	      :link nil
	      :score 1
	      :flair nil
	      :distinguish nil
              :best_sort_enabled true
              :default_sort "BEST"
              :user_attributes {:vote nil}}
        ds (get-in *system* [:db :ds])]
    (jdbc/update! ds "sub_post" {:deleted 1} ["pid = ?" (Integer/parseInt pid)])
    (testing "admin, mod can see deleted post title, user and content"
      (doseq [viewer [admin-uid mod-uid]]
        (let [rsp (send-request q:get-single-post
                                {:pid pid}
                                (utils/request-cookies viewer))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :slug nil
                                      :status "DELETED_BY_USER")}
                 data)))))
    (testing "author does not see content of self-deleted post"
      (let [rsp (send-request q:get-single-post
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid (assoc post
                                    :content nil :slug nil
                                    :status "DELETED_BY_USER")} data))))
    (testing "another user does not see content or title of deleted post"
      (let [rsp (send-request q:get-single-post
                              {:pid pid}
                              (utils/request-cookies mod2-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid (assoc post
                                    :content nil :title nil :slug nil
                                    :status "DELETED")}
               data))))
    (testing "anon does not see content or title of deleted post"
      (let [rsp (send-request q:get-single-post-anon
                              {:pid pid})
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= {:post_by_pid (-> post
                                 (dissoc :user_attributes)
                                 (assoc :content nil :title nil :slug nil
                                        :status "DELETED"))} data))))
    (testing "when author is deleted and post is user-deleted"
      (jdbc/update! ds "public.user" {:status 10} ["uid = ?" user-uid])
      (testing "admin can see deleted user name"
        (let [rsp (send-request q:get-single-post
                                {:pid pid}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :title nil :content nil :slug nil
                                      :author {:name user-name
                                               :status "DELETED"}
                                      :status "DELETED_BY_USER")} data))))
      (testing "mod can not see deleted user name"
        (let [rsp (send-request q:get-single-post
                                {:pid pid}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :title nil :content nil :slug nil
                                      :author {:name nil
                                               :status "DELETED"}
                                      :status "DELETED_BY_USER")} data))))
      (testing "another user can not see deleted user name"
        (let [rsp (send-request q:get-single-post
                                {:pid pid}
                                (utils/request-cookies mod2-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (assoc post
                                      :title nil :content nil :slug nil
                                      :author {:name nil
                                               :status "DELETED"}
                                      :status "DELETED")} data))))
      (testing "anon does not see content or title of deleted post"
        (let [rsp (send-request q:get-single-post-anon
                                {:pid pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:post_by_pid (-> post
                                   (dissoc :user_attributes)
                                   (assoc :content nil :title nil :slug nil
                                          :author {:name nil
                                                   :status "DELETED"}
                                          :status "DELETED"))} data)))))))

(def q:get-single-post-saved "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    user_attributes {
      is_saved
    }
  }
}")

(def m:save-post "
mutation ($pid: String!, $save: Boolean!) {
  save_post(pid: $pid, save: $save)
}")

(deftest test-save-and-unsave-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for saving"
                           :content "testify"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "a newly created post is not saved"
      (let [rsp (send-request q:get-single-post-saved
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (false? (get-in data [:post_by_pid :user_attributes :is_saved])))))
    (testing "a post can be saved"
      (let [rsp (send-request m:save-post
                              {:pid pid :save true}
                              (utils/request-cookies user-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (testing "and then is_saved will be set"
          (let [rsp (send-request q:get-single-post-saved
                                  {:pid pid}
                                  (utils/request-cookies user-uid))
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (true? (get-in data [:post_by_pid :user_attributes :is_saved])))))
        (testing "and unsaved"
          (let [rsp (send-request m:save-post
                                  {:pid pid :save false}
                                  (utils/request-cookies user-uid))
                {:keys [errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors)))
          (testing "and then is_saved will be unset"
            (let [rsp (send-request q:get-single-post-saved
                                    {:pid pid}
                                    (utils/request-cookies user-uid))
                  {:keys [data errors]} (:body rsp)]
              (is (= 200 (:status rsp)))
              (is (nil? errors))
              (is (false? (get-in data [:post_by_pid :user_attributes :is_saved]))))))))))

(def m:create-sub-post-flair "
mutation ($sid: String!, $text: String!) {
  create_sub_post_flair(sid: $sid, text: $text) {
    id
  }
}")

(defn new-post-flair-id
  "Create a post flair using a GraphQL mutation."
  [sid text mod-uid]
  (let [response (send-request m:create-sub-post-flair
                               {:sid sid :text text}
                               (utils/request-cookies mod-uid))
        {:keys [data]} (:body response)]
    (is (= 200 (:status response)))
    (get-in data [:create_sub_post_flair :id])))

(def m:set-post-flair "
mutation ($pid: String!, $flair_id: String!) {
  set_post_flair(pid: $pid, flair_id: $flair_id) {
    id
    text
  }
}")

(def m:remove-post-flair "
mutation ($pid: String!) {
  remove_post_flair(pid: $pid)
}")

(deftest test-set-post-flair-anonymous
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for flairs"
                           :content "anon"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "flair" mod-uid)]
    (testing "anonymous users"
      (testing "can't set a post flair"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id flair-id})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request q:get-single-post-anon
                                   {:pid pid})
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (not= "flair" (:flair post))))))
      (testing "can't delete a post flair"
        (let [rsp (send-request m:remove-post-flair
                                {:pid pid})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-set-post-flair-invalid-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for flairs"
                           :content "invalid"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies mod-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "testflair" mod-uid)]
    (testing "users"
      (testing "can't set a flair of someone else's post"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request q:get-single-post
                                   {:pid pid}
                                   (utils/request-cookies user-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (not= "testflair" (:flair post))))))
      (testing "can't delete a flair from someone else's post"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies mod-uid))
              _ (is (= 200 (:status rsp)))
              rsp1 (send-request m:remove-post-flair
                                 {:pid pid}
                                 (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp1)))
          (is (= "Not authorized" (get-in rsp1 [:body :errors 0 :message])))
          (let [rsp2 (send-request q:get-single-post
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                post (get-in rsp2 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "testflair" (:flair post))))))
      (testing "can't flair a post that doesn't exist"
        (let [rsp (send-request m:set-post-flair
                                {:pid "11111" :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Not found"
                 (get-in rsp [:body :errors 0 :message]))))))))

(deftest test-set-post-flair-deleted-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/insert! ds "sub_metadata" {:sid sid :key "ucf" :value "1"})
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for flairs"
                           :content "deleted"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid-deleted (get-in rsp [:body :data :create_post :pid])
        _ (jdbc/update! ds "sub_post" {:deleted 1}
                        ["pid = ?" (parse-long pid-deleted)])
        flair-id (new-post-flair-id sid "testflair" mod-uid)]
    (testing "users"
      (testing "can't flair deleted posts"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid-deleted :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Bad Request" (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request q:get-single-post
                                   {:pid pid-deleted}
                                   (utils/request-cookies user-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid-deleted (:pid post)))
            (is (nil? (:flair post)))))))
    (testing "mods can flair deleted posts"
      (let [rsp (send-request m:set-post-flair
                              {:pid pid-deleted :flair_id flair-id}
                              (utils/request-cookies mod-uid))]
        (is (= 200 (:status rsp)))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid-deleted}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "testflair" (:flair post))))))))

(deftest test-set-post-flair-archived-posts
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/insert! ds "sub_metadata" {:sid sid
                                           :key "ucf"
                                           :value "1"})
        days (-> (jdbc/query ds
                             ["select value from site_metadata where key= ?"
                              "site.archive_post_after"])
                 first
                 :value
                 parse-long)
        ms (* (inc days) 24 60 60 1000)
        rsp1 (send-request utils/m:create-test-post
                           {:title "Test post for flairs"
                            :content "archived"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "original" mod-uid)
        flair-id2 (new-post-flair-id sid "new" mod-uid)
        rsp2 (send-request m:set-post-flair
                           {:pid pid :flair_id flair-id}
                           (utils/request-cookies user-uid))]
    (is (= 200 (:status rsp2)))
    (jdbc/update! ds "sub_post" {:posted (-> (java.util.Date.)
                                             .getTime
                                             (- ms)
                                             java.sql.Timestamp.)}
                  ["pid = ?" (parse-long pid)])
    (testing "users and mods"
      (testing "can't flair archived posts"
        (doseq [uid [user-uid admin-uid mod-uid]]
          (let [rsp (send-request m:set-post-flair
                                  {:pid pid :flair_id flair-id2}
                                  (utils/request-cookies uid))]
            (is (= 400 (:status rsp)))
            (is (= "Post archived"
                   (get-in rsp [:body :errors 0 :message])))
            (let [rsp1 (send-request q:get-single-post
                                     {:pid pid}
                                     (utils/request-cookies uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (= "original" (:flair post)))))))
      (testing "can't remove flair from archived posts"
        (doseq [uid [user-uid admin-uid mod-uid]]
          (let [rsp (send-request m:remove-post-flair
                                  {:pid pid}
                                  (utils/request-cookies uid))]
            (is (= 400 (:status rsp)))
            (is (= "Post archived"
                   (get-in rsp [:body :errors 0 :message])))
            (let [rsp1 (send-request q:get-single-post
                                     {:pid pid}
                                     (utils/request-cookies uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (= "original" (:flair post))))))))))

(deftest test-set-post-flair-permissions
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/delete! ds "sub_metadata" ["sid = ? and key in (?, ?)",
                                           sid, "ucf" "umf"])
        rsp1 (send-request utils/m:create-test-post
                           {:title "Test post for flairs"
                            :content "user-must-flair"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])
        flair-id (new-post-flair-id sid "test" mod-uid)]
    (testing "users"
      (testing "can't flair a post unless sub permissions are set"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
      (jdbc/insert! ds "sub_metadata" {:sid sid :key "umf" :value "1"})
      (testing "can flair a post when user_must_flair is set"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id flair-id}
                                (utils/request-cookies user-uid))]
          (is (= 200 (:status rsp))))
        (testing "but can't remove it"
          (let [rsp (send-request m:remove-post-flair
                                  {:pid pid}
                                  (utils/request-cookies user-uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))
            (let [rsp1 (send-request q:get-single-post
                                     {:pid pid}
                                     (utils/request-cookies user-uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (= "test" (:flair post))))))
        (testing "and a mod can remove it"
          (let [rsp (send-request m:remove-post-flair
                                  {:pid pid}
                                  (utils/request-cookies mod-uid))]
            (is (= 200 (:status rsp)))
            (let [rsp1 (send-request q:get-single-post
                                     {:pid pid}
                                     (utils/request-cookies user-uid))
                  post (get-in rsp1 [:body :data :post_by_pid])]
              (is (= pid (:pid post)))
              (is (nil? (:flair post))))))))))

(def m:update-flair "
mutation update_sub_post_flair ($id: String!, $sid: String!, $mods_only: Boolean!,
                                $post_types: [PostType!]) {
  update_sub_post_flair(id: $id, sid: $sid, mods_only: $mods_only, post_types: $post_types)
}
")

(deftest test-set-post-flair-valid-flair
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        _ (jdbc/insert! ds "sub_metadata" {:sid sid :key "ucf" :value "1"})
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for flairs"
                           :content "testing"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])
        mod-flair-id (new-post-flair-id sid "mods-only" mod-uid)
        poll-flair-id (new-post-flair-id sid "polls-only" mod-uid)]
    (testing "users and mods can't add a flair that does not exist"
      (doseq [uid [admin-uid mod-uid user-uid]]
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id "999"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Not found" (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request q:get-single-post
                                   {:pid pid}
                                   (utils/request-cookies uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (nil? (:flair post)))))))
    (testing "users can't use a mods only flair"
      (let [rsp (send-request m:update-flair
                              {:sid sid :id mod-flair-id
                               :mods_only true
                               :post_types ["TEXT"]}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            _ (is (= {:data {:update_sub_post_flair nil}} (:body rsp)))
            rsp (send-request m:set-post-flair
                              {:pid pid :flair_id mod-flair-id}
                              (utils/request-cookies user-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (nil? (:flair post)))))
      (testing "but mods can"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id mod-flair-id}
                                (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp)))
          (is (= {:data {:set_post_flair {:id mod-flair-id :text "mods-only"}}}
                 (:body rsp)))
          (let [rsp1 (send-request q:get-single-post
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "mods-only" (:flair post)))))))
    (testing "users can't use a flair not permitted on the post type"
      (let [rsp (send-request m:remove-post-flair
                              {:pid pid}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            rsp (send-request m:update-flair
                              {:sid sid :id poll-flair-id
                               :mods_only false
                               :post_types ["POLL"]}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            _ (is (= {:data {:update_sub_post_flair nil}} (:body rsp)))
            rsp (send-request m:set-post-flair
                              {:pid pid :flair_id poll-flair-id}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Not permitted" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (nil? (:flair post)))))
      (testing "but mods can"
        (let [rsp (send-request m:set-post-flair
                                {:pid pid :flair_id poll-flair-id}
                                (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp)))
          (is (= {:data {:set_post_flair {:id poll-flair-id :text "polls-only"}}}
                 (:body rsp)))
          (let [rsp1 (send-request q:get-single-post
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "polls-only" (:flair post)))))))))

(def m:update-post-content "
mutation ($pid: String!, $content: String!) {
 update_post_content(pid: $pid, content: $content)
}")

(deftest test-update-content-unauthorized
  (let [{:keys [mod-uid user-uid admin-uid
                sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test post"
                           :content "anon"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "anonymous users"
      (testing "can't edit a post"
        (let [rsp (send-request m:update-post-content
                                {:pid pid :content "New content"})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request q:get-single-post-anon
                                   {:pid pid})
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (not= "New content" (:content post))))))
      (testing "mods and admins can't edit user's posts"
        (doseq [uid [mod-uid admin-uid]]
          (let [rsp (send-request m:update-post-content
                                  {:pid pid :content "New content"}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))
        (let [rsp1 (send-request q:get-single-post-anon
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "anon" (:content post))))))))

(deftest test-update-content-deleted-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for update content"
                           :content "deleted"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid-deleted (get-in rsp [:body :data :create_post :pid])
        _ (jdbc/update! ds "sub_post" {:deleted 2}
                        ["pid = ?" (parse-long pid-deleted)])]
    (testing "users can't edit deleted posts"
      (let [rsp (send-request m:update-post-content
                              {:pid pid-deleted :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Post deleted" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid-deleted}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "deleted" (:content post))))))))

(deftest test-update-content-archived-posts
  (let [{:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        days (-> (jdbc/query ds
                             ["select value from site_metadata where key= ?"
                              "site.archive_post_after"])
                 first
                 :value
                 parse-long)
        ms (* (inc days) 24 60 60 1000)
        rsp1 (send-request utils/m:create-test-post
                           {:title "Test post for update content"
                            :content "archived"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])]
    (jdbc/update! ds "sub_post" {:posted (-> (java.util.Date.)
                                             .getTime
                                             (- ms)
                                             java.sql.Timestamp.)}
                  ["pid = ?" (parse-long pid)])
    (testing "users can't edit archived posts"
      (let [rsp (send-request m:update-post-content
                              {:pid pid :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Post archived"
               (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "archived" (:content post))))))))

(def q:get-single-post-history "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    content
    title
    slug
    content_history {
      content
    }
    title_history {
      content
      user {
        name
      }
    }
  }
}
")

(deftest test-update-post-content-saves-history
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for update content"
                           :content "original"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "1"})
    (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
              (car/flushdb))
    (testing "users can edit post content"
      (let [rsp (send-request m:update-post-content
                              {:pid pid :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp)))
        (is (true? (get-in rsp [:body :data :update_post_content])))
        (testing "but not see their own edit history"
          (let [rsp1 (send-request q:get-single-post-history
                                   {:pid pid}
                                   (utils/request-cookies user-uid))
                _ (is (= 403 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (= "Not authorized" (get-in errors [0 :message])))
            (is (empty? (:content_history post)))))
        (testing "and mods can see the edit history"
          (let [rsp1 (send-request q:get-single-post-history
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                _ (is (= 200 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (nil? errors))
            (is (= {:pid pid
	            :content "changed"
	            :title "Test post for update content"
                    :slug "test-post-for-update-content"
	            :content_history [{:content "original"}]
	            :title_history nil} post)))))
      (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "0"})
      (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
                (car/flushdb)))
    (testing "mods can't see edit history when config option is off"
      (let [rsp1 (send-request q:get-single-post-history
                               {:pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp1)))
            {:keys [data errors]} (:body rsp1)
            post (:post_by_pid data)]
        (is (nil? errors))
        (is (= {:pid pid
	        :content "changed"
	        :title "Test post for update content"
                :slug "test-post-for-update-content"
	        :content_history nil
	        :title_history nil} post))))))

(deftest test-update-post-content-argument-checking
  (let [{:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for update content args"
                           :content "content"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "can't update a non-existent post"
      (let [rsp (send-request m:update-post-content
                              {:pid "999999" :content "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Not found"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "can't update post content to"
      (testing "empty"
        (let [rsp (send-request m:update-post-content
                                {:pid pid :content ""}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "just whitespace"
        (let [rsp (send-request m:update-post-content
                                {:pid pid :content "       "}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "too long"
        (let [rsp (send-request m:update-post-content
                                {:pid pid :content
                                 (apply str (map (fn [_] "x") (range 65536)))}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: content"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "post unchanged by failed attempts"
        (let [rsp1 (send-request q:get-single-post-anon
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "content" (:content post))))))))

(def m:update-post-title "
mutation ($pid: String!, $title: String!, $reason: String) {
 update_post_title(pid: $pid, title: $title, reason: $reason)
}")

(deftest test-update-title-unauthorized
  (let [{:keys [mod2-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test post"
                           :content "anon"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "anonymous users"
      (testing "can't edit a post title"
        (let [rsp (send-request m:update-post-title
                                {:pid pid :title "New title"})]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message])))
          (let [rsp1 (send-request q:get-single-post-anon
                                   {:pid pid})
                post (get-in rsp1 [:body :data :post_by_pid])]
            (is (= pid (:pid post)))
            (is (= "Test post" (:title post))))))
      (testing "other users can't edit user's titles"
        (let [rsp (send-request m:update-post-title
                                {:pid pid :title "New title"}
                                (utils/request-cookies mod2-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized"
                 (get-in rsp [:body :errors 0 :message]))))
        (let [rsp1 (send-request q:get-single-post-anon
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "Test post" (:title post))))))))

(deftest test-update-title-deleted-post
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for update title"
                           :content "deleted"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid-deleted (get-in rsp [:body :data :create_post :pid])
        _ (jdbc/update! ds "sub_post" {:deleted 3}  ; ADMIN_REMOVED
                        ["pid = ?" (parse-long pid-deleted)])]
    (testing "users can't edit deleted post titles"
      (let [rsp (send-request m:update-post-title
                              {:pid pid-deleted :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Post deleted" (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid-deleted}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "Test post for update title" (:title post))))))
    (testing "mods can edit deleted post titles"
      (let [rsp (send-request m:update-post-title
                              {:pid pid-deleted :title "changed"
                               :reason "rules"}
                              (utils/request-cookies mod-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid-deleted}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid-deleted (:pid post)))
          (is (= "changed" (:title post))))))))

(deftest test-update-title-time-limit
  (let [{:keys [mod-uid user-uid sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        seconds (-> (jdbc/query ds
                                ["select value from site_metadata where key= ?"
                                 "site.title_edit_timeout"])
                    first
                    :value
                    parse-long)
        ms (* (inc seconds) 1000)
        rsp1 (send-request utils/m:create-test-post
                           {:title "Test post for update title"
                            :content "timeout"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])]
    (jdbc/update! ds "sub_post" {:posted (-> (java.util.Date.)
                                             .getTime
                                             (- ms)
                                             java.sql.Timestamp.)}
                  ["pid = ?" (parse-long pid)])
    (testing "users can't edit titles on posts older than timeout"
      (let [rsp (send-request m:update-post-title
                              {:pid pid :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Time expired"
               (get-in rsp [:body :errors 0 :message])))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid}
                                 (utils/request-cookies user-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "Test post for update title" (:title post))))))
    (testing "mods can edit titles on posts older than timeout"
      (let [rsp (send-request m:update-post-title
                              {:pid pid :title "mod change"
                               :reason "moderation"}
                              (utils/request-cookies mod-uid))
            {:keys [errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (let [rsp1 (send-request q:get-single-post
                                 {:pid pid}
                                 (utils/request-cookies mod-uid))
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "mod change" (:title post))))))))

(def s:notification-counts "
subscription notification_counts {
  notification_counts {
    unread_message_count
    unread_notification_count
  }
}")

(def q:modmail-threads "
query ($first: Int!, $after: String, $sids: [String!],
       $category: ModmailCategory!) {
  modmail_threads (first: $first, after: $after, sids: $sids,
                   category: $category, unread_only: false) {
    edges {
      node {
        subject
        reply_count
        first_message {
          content
          mtype
          unread
        }
      }
    }
  }
}")

(deftest test-update-title-notifications
  (let [{:keys [mod-uid user-uid sid
                sub-name]} (utils/mod-user-and-sub *system*)
        rsp1 (send-request utils/m:create-test-post
                           {:title "Test post for update title"
                            :content "notifications"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp1)))
        pid (get-in rsp1 [:body :data :create_post :pid])
        user-session (utils/make-session user-uid)]
    (testing "mod edit title"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (utils/hash-query
                                            s:notification-counts)}})
        (let [rsp (send-request m:update-post-title
                                {:pid pid :title "mod change"
                                 :reason "moderation"}
                                (utils/request-cookies mod-uid))
              {:keys [errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (testing "notifies user"
            (utils/expect-message {:type "data"
	                           :id id
	                           :payload
                                   {:data
	                            {:notification_counts
	                             {:unread_message_count 1
	                              :unread_notification_count 0}}}}))
          (testing "sends a USER_NOTIFICATION modmail message"
            (let [rsp (send-request q:modmail-threads
                                    {:first 25
                                     :after nil
                                     :sids [sid]
                                     :category :ALL}
                                    (utils/request-cookies mod-uid))
                  _ (is (= 200 (:status rsp)))
                  {:keys [data errors]} (:body rsp)
                  thread (get-in data [:modmail_threads :edges 0 :node])
                  content (get-in thread [:first_message :content])]
              (is (nil? errors))
              (is (= {:subject "Moderation action: post title changed"
	              :reply_count 0
	              :first_message
	              {:mtype "USER_NOTIFICATION"
	               :unread false}}
                     (update thread :first_message dissoc :content)))
              (is (str/includes? content (str "/o/" sub-name)))
              (is (str/includes? content (str "/o/" sub-name "/" pid "/"
                                              "mod-change")))
              (is (str/includes? content "Reason: moderation")))))))))

(deftest test-update-post-title-saves-history
  (let [{:keys [mod-uid user-name user-uid
                sid]} (utils/mod-user-and-sub *system*)
        ds (get-in *system* [:db :ds])
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for update title"
                           :content "original"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "1"})
    (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
              (car/flushdb))
    (testing "users can edit post title"
      (let [rsp (send-request m:update-post-title
                              {:pid pid :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 200 (:status rsp)))
        (is (true? (get-in rsp [:body :data :update_post_title])))
        (testing "but not see their own edit history"
          (let [rsp1 (send-request q:get-single-post-history
                                   {:pid pid}
                                   (utils/request-cookies user-uid))
                _ (is (= 403 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (= "Not authorized" (get-in errors [0 :message])))
            (is (empty? (:title_history post)))))
        (testing "and mods can see the edit history"
          (let [rsp1 (send-request q:get-single-post-history
                                   {:pid pid}
                                   (utils/request-cookies mod-uid))
                _ (is (= 200 (:status rsp1)))
                {:keys [data errors]} (:body rsp1)
                post (:post_by_pid data)]
            (is (nil? errors))
            (is (= {:pid pid
	            :content "original"
	            :title "changed"
                    :slug "changed"
	            :title_history [{:content "Test post for update title"
                                     :user {:name user-name}}]
	            :content_history nil} post)))))
      (jdbc/insert! ds "site_metadata" {:key "site.edit_history" :value "0"})
      (car/wcar {:pool {} :spec {:uri (env :TEST_REDIS_URL)}}
                (car/flushdb)))
    (testing "mods can't see edit history when config option is off"
      (let [rsp1 (send-request q:get-single-post-history
                               {:pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp1)))
            {:keys [data errors]} (:body rsp1)
            post (:post_by_pid data)]
        (is (nil? errors))
        (is (= {:pid pid
	        :title "changed"
	        :content "original"
                :slug "changed"
	        :content_history nil
	        :title_history nil} post))))))

(deftest test-update-post-title-argument-checking
  (let [{:keys [user-uid sid]} (utils/mod-user-and-sub *system*)
        rsp (send-request utils/m:create-test-post
                          {:title "Test post for update title args"
                           :content "args"
                           :sid sid
                           :nsfw false
                           :type :TEXT}
                          (utils/request-cookies user-uid))
        _ (is (= 200 (:status rsp)))
        pid (get-in rsp [:body :data :create_post :pid])]
    (testing "can't change title of a non-existent post"
      (let [rsp (send-request m:update-post-title
                              {:pid "abcde" :title "changed"}
                              (utils/request-cookies user-uid))]
        (is (= 400 (:status rsp)))
        (is (= "Invalid argument: pid"
               (get-in rsp [:body :errors 0 :message])))))
    (testing "can't update post title to"
      (testing "empty"
        (let [rsp (send-request m:update-post-title
                                {:pid pid :title ""}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "just whitespace"
        (let [rsp (send-request m:update-post-title
                                {:pid pid :title "       "}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "too short"
        (let [rsp (send-request m:update-post-title
                                {:pid pid :title "a"}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "too long"
        (let [rsp (send-request m:update-post-title
                                {:pid pid :title
                                 (apply str (map (fn [_] "x") (range 300)))}
                                (utils/request-cookies user-uid))]
          (is (= 400 (:status rsp)))
          (is (= "Invalid argument: title"
                 (get-in rsp [:body :errors 0 :message])))))
      (testing "title unchanged by failed attempts"
        (let [rsp1 (send-request q:get-single-post-anon
                                 {:pid pid})
              post (get-in rsp1 [:body :data :post_by_pid])]
          (is (= pid (:pid post)))
          (is (= "Test post for update title args" (:title post))))))))

(def m:create-test-post-report "
mutation ($reason: String!, $send_to_admin: Boolean!, $pid: String!) {
  create_report(reason: $reason, send_to_admin: $send_to_admin, pid: $pid) {
    id
  }
}
")

(def q:get-single-post-with-reports "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    open_reports {
      id
      rtype
    }
  }
}
")

(deftest test-query-post-reports
  (let [{:keys [user-uid mod-uid mod2-uid admin-uid
                sid]} (utils/mod-user-and-sub *system*)]
    (testing "create a post with a report"
      (let [rsp1 (send-request utils/m:create-test-post
                               {:title "Test post"
                                :content "testify"
                                :sid sid
                                :nsfw false
                                :type :TEXT}
                               (utils/request-cookies user-uid))
            _ (is (= 200 (:status rsp1)))
            _ (is (nil? (get-in rsp1 [:body :errors])))
            pid (get-in rsp1 [:body :data :create_post :pid])
            _ (is (some? pid))
            rsp2 (send-request m:create-test-post-report
                               {:reason "test reason"
                                :send_to_admin false
                                :pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp2)))
            _ (is (nil? (get-in rsp2 [:body :errors])))
            rid (get-in rsp2 [:body :data :create_report :id])
            _ (is (some? rid))]
        (testing "and admins and mods can fetch the post with the report"
          (doseq [uid [mod-uid admin-uid]]
            (let [rsp (send-request q:get-single-post-with-reports
                                    {:pid pid}
                                    (utils/request-cookies uid))
                  {:keys [data errors]} (:body rsp)]
              (is (= 200 (:status rsp)))
              (is (nil? errors))
              (is (= {:post_by_pid {:pid pid
                                    :open_reports [{:id rid
                                                    :rtype "POST"}]}}
                     data)))))
        (testing "and authors, users and anons cannot fetch the report"
          (doseq [uid [nil user-uid mod2-uid]]
            (let [rsp (send-request q:get-single-post-with-reports
                                    {:pid pid}
                                    (utils/request-cookies uid))
                  {:keys [data errors]} (:body rsp)]
              (is (= 403 (:status rsp)))
              (is (= "Not authorized" (get-in errors [0 :message])))
              (is (empty? (get-in data [:post_by_pid :open_reports]))))))))))

(def m:create-test-post-viewed "
mutation ($content: String!, $nsfw: Boolean!, $type: PostType!, $title: String!,
          $sid: String!) {
  create_post(content: $content, nsfw: $nsfw, type: $type, title: $title,
              sid: $sid) {
   pid
   posted
   user_attributes {
     viewed
   }
  }
}
")

(def q:get-single-post-viewed "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    posted
    user_attributes {
      viewed
    }
  }
}
")

(def m:update-post-viewed "
mutation ($pid :String!) {
  update_post_viewed(pid: $pid)
}")

(deftest test-post-viewed
  (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        rsp1 (send-request m:create-test-post-viewed
                           {:title "Test post"
                            :content "testify"
                            :sid sid
                            :nsfw false
                            :type :TEXT}
                           (utils/request-cookies mod-uid))
        pid (get-in rsp1 [:body :data :create_post :pid])]
    (is (= 200 (:status rsp1)))
    (is (nil? (get-in rsp1 [:body :errors])))
    (is (some? pid))
    (is (= (get-in rsp1 [:body :data :create_post :posted])
           (get-in rsp1 [:body :data :create_post :user_attributes :viewed])))
    (testing "new post is marked viewed for author"
      (let [rsp (send-request q:get-single-post-viewed
                              {:pid pid}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (= (get-in data [:post_by_pid :posted])
               (get-in data [:post_by_pid :user_attributes :viewed])))))
    (testing "new post is marked unviewed for other users"
      (let [rsp (send-request q:get-single-post-viewed
                              {:pid pid}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (nil? (get-in data [:post_by_pid :user_attributes :viewed])))))
    (testing "anons can't see if a post is viewed"
      (let [rsp (send-request q:get-single-post-viewed
                              {:pid pid})
            {:keys [data]} (:body rsp)]
        (is (= 403 (:status rsp)))
        (is (nil? (get-in data [:post_by_pid :user_attributes :viewed])))))
    (testing "anons can't mark a post viewed"
      (let [rsp (send-request m:update-post-viewed
                              {:pid pid})]
        (is (= 403 (:status rsp)))))
    (testing "marking a viewed post viewed again updates the datetime"
      (let [rsp (send-request m:update-post-viewed
                              {:pid pid}
                              (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp)))
            rsp1 (send-request q:get-single-post-viewed
                               {:pid pid}
                               (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp1)]
        (is (= 200 (:status rsp1)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (string? (get-in data [:post_by_pid :user_attributes :viewed])))
        (is (not= (get-in data [:post_by_pid :posted])
                  (get-in data [:post_by_pid :user_attributes :viewed])))))
    (testing "other users can mark a post viewed"
      (let [rsp (send-request m:update-post-viewed
                              {:pid pid}
                              (utils/request-cookies user-uid))
            _ (is (= 200 (:status rsp)))
            rsp1 (send-request q:get-single-post-viewed
                               {:pid pid}
                               (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp1)]
        (is (= 200 (:status rsp1)))
        (is (nil? errors))
        (is (= pid (get-in data [:post_by_pid :pid])))
        (is (string? (get-in data [:post_by_pid :user_attributes :viewed])))))))

(def m:distinguish-post "
mutation ($pid: String!, $sid: String!, $distinguish: DistinguishType) {
  distinguish_post(pid: $pid, sid: $sid, distinguish: $distinguish)
}")

(deftest test-distinguish-authorization
  (let [{:keys [admin-uid user-uid mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        user-pid (-> (send-request utils/m:create-test-post
                                   {:title "user post"
                                    :content "test"
                                    :sid sid
                                    :nsfw false
                                    :type :TEXT}
                                   (utils/request-cookies user-uid))
                     :body :data :create_post :pid)
        mod-pid (-> (send-request utils/m:create-test-post
                                  {:title "mod post"
                                   :content "test"
                                   :sid sid
                                   :nsfw false
                                   :type :TEXT}
                                  (utils/request-cookies mod-uid))
                    :body :data :create_post :pid)
        admin-pid (-> (send-request utils/m:create-test-post
                                    {:title "mod post"
                                     :content "test"
                                     :sid sid
                                     :nsfw false
                                     :type :TEXT}
                                    (utils/request-cookies admin-uid))
                      :body :data :create_post :pid)]
    (is (and user-pid mod-pid admin-pid))
    (testing "users and anons"
      (doseq [uid [nil user-uid]]
        (testing "can't distinguish a post"
          (let [rsp (send-request m:distinguish-post
                                  {:pid user-pid :sid sid :distinguish :MOD}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))))
    (testing "mods can't distinguish other's posts"
      (doseq [pid [user-pid admin-pid]]
        (let [rsp (send-request m:distinguish-post
                                {:pid pid :sid sid :distinguish :MOD}
                                (utils/request-cookies mod-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))
    (testing "admins can't distinguish other's posts"
      (doseq [pid [user-pid mod-pid]]
        (let [rsp (send-request m:distinguish-post
                                {:pid pid :sid sid :distinguish :ADMIN}
                                (utils/request-cookies admin-uid))]
          (is (= 403 (:status rsp)))
          (is (= "Not authorized" (get-in rsp [:body :errors 0 :message]))))))
    (testing "mods can't distinguish as admin"
      (let [rsp (send-request m:distinguish-post
                              {:pid mod-pid :sid sid :distinguish :ADMIN}
                              (utils/request-cookies mod-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))
    (testing "admins can't distinguish as mod"
      (let [rsp (send-request m:distinguish-post
                              {:pid admin-pid :sid sid :distinguish :MOD}
                              (utils/request-cookies admin-uid))]
        (is (= 403 (:status rsp)))
        (is (= "Not authorized" (get-in rsp [:body :errors 0 :message])))))))

(def q:get-post-distinguish "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    distinguish
  }
}
")

(deftest test-distinguish
  (let [{:keys [admin-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        mod-pid (-> (send-request utils/m:create-test-post
                                  {:title "mod post"
                                   :content "test"
                                   :sid sid
                                   :nsfw false
                                   :type :TEXT}
                                  (utils/request-cookies mod-uid))
                    :body :data :create_post :pid)
        admin-pid (-> (send-request utils/m:create-test-post
                                    {:title "mod post"
                                     :content "test"
                                     :sid sid
                                     :nsfw false
                                     :type :TEXT}
                                    (utils/request-cookies admin-uid))
                      :body :data :create_post :pid)]
    (is (and mod-pid admin-pid))
    (testing "new posts are undistinguished"
      (doseq [pid [admin-pid mod-pid]]
        (let [rsp (send-request q:get-post-distinguish
                                {:pid pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid pid :distinguish nil} (:post_by_pid data))))))
    (testing "mods can distinguish as mod"
      (let [rsp (send-request m:distinguish-post
                              {:pid mod-pid :sid sid :distinguish :MOD}
                              (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= "MOD" (:distinguish_post data)))
        (let [rsp (send-request q:get-post-distinguish
                                {:pid mod-pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid mod-pid :distinguish "MOD"} (:post_by_pid data)))))
      (testing "and remove the distinguishing"
        (let [rsp (send-request m:distinguish-post
                                {:pid mod-pid :sid sid :distinguish nil}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (nil? (:distinguish_post data)))
          (let [rsp (send-request q:get-post-distinguish
                                  {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :distinguish nil} (:post_by_pid data)))))))
    (testing "admins can distinguish as admin"
      (let [rsp (send-request m:distinguish-post
                              {:pid admin-pid :sid sid :distinguish :ADMIN}
                              (utils/request-cookies admin-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (= "ADMIN" (:distinguish_post data)))
        (let [rsp (send-request q:get-post-distinguish
                                {:pid admin-pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid admin-pid :distinguish "ADMIN"}
                 (:post_by_pid data)))))
      (testing "and remove the distinguishing"
        (let [rsp (send-request m:distinguish-post
                                {:pid admin-pid :sid sid :distinguish nil}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (nil? (:distinguish_post data)))
          (let [rsp (send-request q:get-post-distinguish
                                  {:pid admin-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid admin-pid :distinguish nil}
                   (:post_by_pid data)))))))
    (testing "mods who are also admins can distinguish as mod or admin"
      (utils/promote-user-to-admin! *system* mod-uid)
      (doseq [distinguish [:MOD :ADMIN]]
        (let [rsp (send-request m:distinguish-post
                                {:pid mod-pid :sid sid :distinguish distinguish}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= (name distinguish) (:distinguish_post data)))
          (let [rsp (send-request q:get-post-distinguish
                                  {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :distinguish (name distinguish)}
                   (:post_by_pid data)))))))))

(def m:set-post-nsfw "
mutation ($pid: String!, $sid: String!, $nsfw: Boolean!) {
  set_post_nsfw(pid: $pid, nsfw: $nsfw)
}")

(deftest test-nsfw-authorization
  (let [{:keys [user-uid user-name mod2-uid
                sid]} (utils/mod-user-and-sub *system*)
        pid (-> (send-request utils/m:create-test-post
                              {:title "user post"
                               :content "nsfw"
                               :sid sid
                               :nsfw false
                               :type :TEXT}
                              (utils/request-cookies user-uid))
                :body :data :create_post :pid)
        extra-user-uid (-> utils/m:create-user
                           (send-request {:name (str user-name "-extra")})
                           (get-in [:body :data :register_user :uid]))]
    (is (and pid extra-user-uid))
    (testing "anons, other users and mods of other subs"
      (doseq [uid [nil mod2-uid extra-user-uid]]
        (testing "can't change post nsfw"
          (let [rsp (send-request m:set-post-nsfw
                                  {:pid pid :nsfw true}
                                  (utils/request-cookies uid))]
            (is (= 403 (:status rsp)))
            (is (= "Not authorized"
                   (get-in rsp [:body :errors 0 :message])))))))))

(def q:get-post-nsfw "
query ($pid: String!) {
  post_by_pid(pid: $pid) {
    pid
    nsfw
  }
}
")

(deftest test-set-nsfw
  (let [{:keys [admin-uid mod-uid user-uid
                sid]} (utils/mod-user-and-sub *system*)
        mod-pid (-> (send-request utils/m:create-test-post
                                  {:title "mod post"
                                   :content "test"
                                   :sid sid
                                   :nsfw false
                                   :type :TEXT}
                                  (utils/request-cookies mod-uid))
                    :body :data :create_post :pid)
        user-pid (-> (send-request utils/m:create-test-post
                                   {:title "user post"
                                    :content "test"
                                    :sid sid
                                    :nsfw false
                                    :type :TEXT}
                                   (utils/request-cookies user-uid))
                     :body :data :create_post :pid)]
    (testing "a mod can change nsfw tag on mod and user posts"
      (doseq [pid [mod-pid user-pid]]
        (let [rsp (send-request m:set-post-nsfw
                                {:pid pid :nsfw true}
                                (utils/request-cookies mod-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (:set_post_nsfw data))
          (let [rsp (send-request q:get-post-nsfw
                                  {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :nsfw true} (:post_by_pid data)))))))
    (testing "an admin can change nsfw tag on mod and user posts"
      (doseq [pid [mod-pid user-pid]]
        (let [rsp (send-request m:set-post-nsfw
                                {:pid pid :nsfw false}
                                (utils/request-cookies admin-uid))
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (false? (:set_post_nsfw data)))
          (let [rsp (send-request q:get-post-nsfw {:pid mod-pid})
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:pid mod-pid :nsfw false} (:post_by_pid data)))))))
    (testing "a user can change nsfw tag on their post"
      (let [rsp (send-request m:set-post-nsfw
                              {:pid user-pid :nsfw true}
                              (utils/request-cookies user-uid))
            {:keys [data errors]} (:body rsp)]
        (is (= 200 (:status rsp)))
        (is (nil? errors))
        (is (:set_post_nsfw data))
        (let [rsp (send-request q:get-post-nsfw {:pid user-pid})
              {:keys [data errors]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:pid user-pid :nsfw true} (:post_by_pid data))))))))
