;; elements/message/resolve_test.clj -- Testing messaging functionality for throat-be
;; Copyright (C) 2020-2022  The Feminist Conspiracy

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU Affero General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU Affero General Public License for more details.

;; You should have received a copy of the GNU Affero General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(ns throat-be.elements.message.resolve-test
  (:require
   [clojure.test :refer [use-fixtures deftest testing is]]
   [com.stuartsierra.component :as component]
   [throat-be.test-utils :refer [send-request] :as utils]))

(def ^:dynamic ^:private *system*)

(declare m:create-new-modmail m:contact-mods
         q:new-modmail-count q:new-modmail-count-for-admins
         m:contact-mods-mid m:update-unread
         m:contact-mods-mid-thread m:create-new-modmail-mid-thread
         m:update-modmail-mailbox m:create-modmail-reply
         q:modmail-thread q:modmail-threads m:update-thread-unread
         m:create-new-message m:get-message s:notification-counts
         q:modmail-thread-by-id m:create-new-modmail-with-report
         m:create-test-post-report
         q:modmail-thread-by-id-with-report q:message-thread-by-id-mailbox)

(defn system-map []
  (-> (utils/test-system-config-map)
      (utils/add-queries
       [m:create-new-modmail m:contact-mods
        q:new-modmail-count q:new-modmail-count-for-admins
        m:contact-mods-mid m:update-unread
        m:contact-mods-mid-thread m:create-new-modmail-mid-thread
        m:update-modmail-mailbox m:create-modmail-reply
        q:modmail-thread q:modmail-threads m:update-thread-unread
        m:create-new-message m:get-message s:notification-counts
        q:modmail-thread-by-id m:create-new-modmail-with-report
        m:create-test-post-report
        q:modmail-thread-by-id-with-report q:message-thread-by-id-mailbox])
      utils/test-system-map))

;; For speed, all the tests in this file share a database.
;; So they should avoid stepping on each other.
(use-fixtures :once
  (fn [test-fn]
    (binding [*system* (component/start-system (utils/test-system (system-map)))]
      (try
        (test-fn)
        (finally
          (component/stop-system *system*))))))

(def s:notification-counts "
subscription notification_counts {
  notification_counts {
    unread_message_count
    unread_notification_count
    sub_notifications {
      sid
      open_report_count
      unread_modmail_count
    }
  }
}")

(def m:create-new-modmail "
mutation ($subject: String!, $content: String!, $sid: String!,
          $username: String, $show_mod_username: Boolean) {
  create_modmail_message (subject: $subject, content: $content, sid: $sid,
                          username: $username,
                          show_mod_username: $show_mod_username) {
    content
    mtype
    receiver {
      name
    }
    sender {
      name
    }
  }
}")

(deftest test-create-modmail
  (let [{:keys [user-uid user-name mod-uid mod-name
                sid]} (utils/mod-user-and-sub *system*)
        user-session (utils/make-session user-uid)]
    (testing "create new modmail"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers user-session))
        (utils/send-init {:token (utils/sign-csrf-token user-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (utils/hash-query
                                            s:notification-counts)}})
        (let [response (send-request m:create-new-modmail
                                     {:subject "Welcome"
                                      :content "Modmail!"
                                      :sid sid
                                      :username user-name
                                      :show_mod_username true}
                                     (utils/request-cookies mod-uid))]
          (is (= 200 (:status response)))
          (is (= {:data
	          {:create_modmail_message
	           {:content "Modmail!"
                    :mtype "MOD_TO_USER_AS_USER"
                    :sender {:name mod-name}
                    :receiver {:name user-name}}}}
                 (:body response)))
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:notification_counts
	                                    {:unread_message_count 1
	                                     :unread_notification_count 0
	                                     :sub_notifications []}}}}))))
    (testing "with non-existent sub"
      (let [response
            (send-request m:create-new-modmail
                          {:subject "Welcome"
                           :content "Modmail!"
                           :sid "clearly a bogus sid"
                           :username user-name
                           :show_mod_username true}
                          (utils/request-cookies mod-uid))]
        (is (= 403 (:status response)))
        (is (nil? (get-in response [:body :data :create_modmail_message])))
        (is (= "Not authorized"
               (get-in response [:body :errors 0 :message])))))))

(def m:create-new-modmail-with-report "
mutation ($subject: String!, $content: String!, $sid: String!,
          $username: String, $show_mod_username: Boolean
          $report_type: ReportType, $report_id: String) {
  create_modmail_message (subject: $subject, content: $content, sid: $sid,
                          username: $username,
                          show_mod_username: $show_mod_username
                          report_type: $report_type, report_id: $report_id) {
    mtype
    thread {
      id
    }
  }
}")

(def m:create-test-post-report "
mutation ($reason: String!, $send_to_admin: Boolean!, $pid: String!) {
  create_report(reason: $reason, send_to_admin: $send_to_admin, pid: $pid) {
    id
  }
}
")

(def q:modmail-thread-by-id-with-report "
query ($thread_id: String!) {
  message_thread_by_id (thread_id: $thread_id) {
    report {
      id
      rtype
      reason
      open
      send_to_admin
    }
  }
}")

(deftest test-create-modmail-with-report
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)]
    (testing "create a modmail thread with a linked report"
      (let [rsp1 (send-request utils/m:create-test-post
                               {:title "Test post"
                                :content "testify"
                                :sid sid
                                :nsfw false
                                :type :TEXT}
                               (utils/request-cookies user-uid))
            _ (is (= 200 (:status rsp1)))
            _ (is (nil? (get-in rsp1 [:body :errors])))
            pid (get-in rsp1 [:body :data :create_post :pid])
            _ (is (some? pid))
            rsp2 (send-request m:create-test-post-report
                               {:reason "test reason"
                                :send_to_admin false
                                :pid pid}
                               (utils/request-cookies mod-uid))
            _ (is (= 200 (:status rsp2)))
            _ (is (nil? (get-in rsp2 [:body :errors])))
            rid (get-in rsp2 [:body :data :create_report :id])
            _ (is (some? rid))
            rsp3 (send-request m:create-new-modmail-with-report
                               {:subject "Your report"
                                :content "feedback"
                                :sid sid
                                :username user-name
                                :show_mod_username false
                                :report_type "POST"
                                :report_id rid}
                               (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body rsp3)
            mtid (get-in data [:create_modmail_message :thread :id])]
        (is (= 200 (:status rsp3)))
        (is (nil? errors))
        (is (= "MOD_TO_USER_AS_MOD"
               (get-in rsp3 [:body :data :create_modmail_message :mtype])))
        (testing "and fetch it with the report"
          (let [rsp (send-request q:modmail-thread-by-id-with-report
                                  {:thread_id mtid}
                                  (utils/request-cookies mod-uid))
                {:keys [data errors]} (:body rsp)]
            (is (= 200 (:status rsp)))
            (is (nil? errors))
            (is (= {:message_thread_by_id
                    {:report
                     {:id rid
                      :rtype "POST"
                      :reason "test reason"
                      :open true
                      :send_to_admin false}}}
                   data))))))))

(def q:modmail-threads "
query ($first: Int!, $after: String, $sids: [String!],
       $category: ModmailCategory!, $unread_only: Boolean!) {
  modmail_threads (first: $first, after: $after, sids: $sids,
                   category: $category, unread_only: $unread_only) {
    edges {
      node {
        id
        subject
        sub {
          sid
        }
        reply_count
        first_message {
          mid
          content
          time
          mtype
          unread
        }
        latest_message {
          mid
          content
          time
          mtype
          unread
        }
        mailbox
      }
      cursor
    }
    pageInfo {
      hasNextPage
      startCursor
      endCursor
    }
  }
}")

(deftest test-query-all-modmails
  (testing "Retrieve modmails:"
    (let [{:keys [mod-uid user-uid user-name
                  sid]} (utils/mod-user-and-sub *system*)
          how-many 10  ; Use an even number.
          make-subject #(format "message %d" %)
          create-modmail #(send-request m:create-new-modmail
                                        {:subject (make-subject %)
                                         :content "Modmail!"
                                         :sid sid
                                         :username user-name
                                         :show_mod_username true}
                                        (utils/request-cookies mod-uid))
          modmails (map create-modmail (range how-many))]
      (testing "first create some"
        (is (every? #(= (:status %) 200) modmails))
        (is (every? #(nil? (get-in % [:body :errors])) modmails)))

      ;; The recipient isn't a mod so can't use the modmails query.
      (testing "try query as non-mod")
      (let [response (send-request q:modmail-threads
                                   {:first 10
                                    :sids [sid]
                                    :category "ALL"
                                    :unread_only false}
                                   (utils/request-cookies user-uid))]
        (is (= (:status response) 403))
        (is (= (get-in response [:body :errors 0 :message]) "Not authorized")))

      ;; Try a mod query to get all the threads.
      (testing "query requesting more than available"
        (let [response (send-request q:modmail-threads
                                     {:first (* 2 how-many)
                                      :sids [sid]
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              page-info (get-in data [:modmail_threads :pageInfo])]
          (is (= (:status response) 200))
          (is (not (:hasNextPage page-info)))
          (is (= (map #(make-subject %) (range (dec how-many) -1 -1))
                 (map #(get-in % [:node :subject]) edges)))))

      ;; Try to get all the threads using pagination.
      (testing "query using pagination -"
        (let [response (send-request q:modmail-threads
                                     {:first (/ how-many 2)
                                      :sids [sid]
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              page-info (get-in data [:modmail_threads :pageInfo])]
          (testing "getting the first half"
            (is (= 200 (:status response)))
            (is (nil? errors))
            (is (:hasNextPage page-info))
            (is (= (/ how-many 2) (count edges)))
            (is (:endCursor page-info))
            (is (= (:cursor (last edges)) (:endCursor page-info)))
            (is (= (map #(make-subject %)
                        (range (dec how-many) (dec (/ how-many 2)) -1))
                   (map #(get-in % [:node :subject]) edges))))
          (let [response (send-request
                          q:modmail-threads
                          {:first (/ how-many 2)
                           :after (:endCursor page-info)
                           :sids [sid]
                           :category "ALL"
                           :unread_only false}
                          (utils/request-cookies mod-uid))
                {:keys [errors data]} (:body response)
                edges (get-in data [:modmail_threads :edges])
                page-info (get-in data [:modmail_threads :pageInfo])]
            (testing "getting the second half"
              (is (= 200 (:status response)))
              (is (nil? errors))
              (is (not (:hasNextPage page-info)))
              (is (= (/ how-many 2) (count edges)))
              (is (= (:cursor (last edges)) (:endCursor page-info)))
              (is (= (map #(make-subject %)
                          (range (dec (/ how-many 2)) -1 -1))
                     (map #(get-in % [:node :subject]) edges))))
            (let [response (send-request
                            q:modmail-threads
                            {:first how-many
                             :after (:endCursor page-info)
                             :sids [sid]
                             :category "ALL"
                             :unread_only false}
                            (utils/request-cookies mod-uid))]
              (testing "asking for more when at the end"
                (is (= 200 (:status response)))
                (is (= {:data {:modmail_threads nil}}
                       (:body response)))))))))))

(deftest test-modmail-query-with-empty-sid-list
  (testing "Empty sub list defaults to all the subs"
    (let [{:keys [mod-uid user-uid user-name
                  sid]} (utils/mod-user-and-sub *system*)
          mm (send-request m:create-new-modmail
                           {:subject "query-empty"
                            :content "Modmail!"
                            :username user-name
                            :sid sid
                            :show_mod_username false}
                           (utils/request-cookies mod-uid))]
      (is (= 200 (:status mm)))
      (testing "- query by mod"
        (let [response (send-request q:modmail-threads
                                     {:first 100
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              edges (get-in response [:body :data :modmail_threads
                                      :edges])]
          (is (= 200 (:status response)))
          (is (nil? (get-in response [:body :errors])))
          (is (some #(= (get-in % [:node :subject]) "query-empty") edges))))
      (testing "- query by non-mod user"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies user-uid))]
          (is (= 403 (:status response)))
          (is (= "Not authorized"
                 (get-in response [:body :errors 0 :message])))
          (is (= {:modmail_threads nil}
                 (get-in response [:body :data]))))))))

(def m:contact-mods "
mutation ($subject: String!, $content: String!, $sid: String!) {
  create_message_to_mods (subject: $subject, content: $content, sid: $sid) {
    content
    mtype
    receiver {
      name
    }
    sender {
      name
    }
  }
}")

(deftest test-contact-mods
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        mod-session (utils/make-session mod-uid)]
    (testing "create new message to mods"
      (utils/with-websocket id (utils/open-websocket
                                (utils/ws-headers mod-session))
        (utils/send-init {:token (utils/sign-csrf-token mod-session)})
        (utils/expect-message {:type "connection_ack"})
        (utils/send-data {:id id
                          :type :start
                          :payload {:query (utils/hash-query
                                            s:notification-counts)}})
        (let [response (send-request m:contact-mods
                                     {:subject "Random complaint"
                                      :content "Do better!"
                                      :sid sid}
                                     (utils/request-cookies user-uid))]
          (is (= 200 (:status response)))
          (is (= {:data
	          {:create_message_to_mods
	           {:content "Do better!"
                    :mtype "USER_TO_MODS"
                    :sender {:name user-name}
                    :receiver nil}}}
                 (:body response)))
          (utils/expect-message {:type "data"
                                 :id id
                                 :payload {:data
	                                   {:notification_counts
	                                    {:unread_message_count 0
	                                     :unread_notification_count 0
	                                     :sub_notifications
	                                     [{:sid sid
	                                       :open_report_count nil,
	                                       :unread_modmail_count 1}]}}}})))
      (testing "with non-existent sub"
        (let [response
              (send-request m:contact-mods
                            {:subject "Hello"
                             :content "Anybody there?"
                             :sid "clearly a bogus sid"}
                            (utils/request-cookies user-uid))]
          (is (= 400 (:status response)))
          (is (nil? (get-in response [:body :data :create_message_to_mods])))
          (is (= "Sub does not exist"
                 (get-in response [:body :errors 0 :message]))))))))

(def q:new-modmail-count "
query ($name: String!) {
  sub_by_name(name: $name) {
    all_modmail_count
    new_modmail_count
    unread_modmail_count
  }
}")

(def q:new-modmail-count-for-admins "
query ($name: String!) {
  sub_by_name(name: $name) {
    all_modmail_count
    new_modmail_count
  }
}")

(deftest test-modmail-stats
  (testing "modmail stats for mod dashboard")
  (let [{:keys [mod-uid user-uid
                sid sub-name]} (utils/mod-user-and-sub *system*)]
    (send-request m:contact-mods
                  {:subject "Random complaint"
                   :content "Do better!"
                   :sid sid}
                  (utils/request-cookies user-uid))
    (testing "- non-mods can't query"
      (let [response (send-request q:new-modmail-count
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            {:keys [data errors]} (:body response)]
        (is (= 403 (:status response)))
        (is (= "Not authorized" (get-in errors [0 :message])))
        ;; Lacinia fills in a 0 here.
        (is (not= 1 (get-in data [:sub_by_name :all_modmail_count])))
        (is (not= 1 (get-in data [:sub_by_name :new_modmail_count])))
        (is (not= 1 (get-in data [:sub_by_name :unread_modmail_count])))))
    (testing "- mods get results"
      (let [response (send-request q:new-modmail-count
                                   {:name sub-name}
                                   (utils/request-cookies mod-uid))
            {:keys [data errors]} (:body response)]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= 1 (get-in data [:sub_by_name :all_modmail_count])))
        (is (= 1 (get-in data [:sub_by_name :new_modmail_count])))
        (is (= 1 (get-in data [:sub_by_name :unread_modmail_count])))))
    (testing "- admins get results"
      (utils/promote-user-to-admin! *system* user-uid)
      (let [response (send-request q:new-modmail-count-for-admins
                                   {:name sub-name}
                                   (utils/request-cookies user-uid))
            {:keys [data errors]} (:body response)]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= 1 (get-in data [:sub_by_name :all_modmail_count])))
        (is (= 1 (get-in data [:sub_by_name :new_modmail_count])))))))

(deftest test-modmail-categories
  (let [{:keys [mod-uid user-uid user-name
                sid]} (utils/mod-user-and-sub *system*)
        user2mod (send-request m:contact-mods
                               {:subject "Random complaint"
                                :content "Do better!"
                                :sid sid}
                               (utils/request-cookies user-uid))
        mod2user (send-request m:create-new-modmail
                               {:subject "Welcome"
                                :content "Modmail!"
                                :sid sid
                                :username user-name
                                :show_mod_username false}
                               (utils/request-cookies mod-uid))
        mod2mod (send-request m:create-new-modmail
                              {:subject "Talk"
                               :content "Discussion!"
                               :sid sid}
                              (utils/request-cookies mod-uid))]
    (is (= 200 (:status user2mod)))
    (is (= 200 (:status mod2user)))
    (is (= 200 (:status mod2mod)))
    (testing "Querying all modmails doesn't fetch mod discussions"
      (let [response (send-request q:modmail-threads
                                   {:first 10
                                    :sids []
                                    :category "ALL"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Random complaint" "Welcome"} subjects))))
    (testing "Querying new modmails gets ones from users without replies."
      (let [response (send-request q:modmail-threads
                                   {:first 10
                                    :sids []
                                    :category "NEW"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Random complaint"} subjects))))
    (testing "Querying in progress modmails gets ones sent by mods and
              ones sent by users with no replies."
      (let [response (send-request q:modmail-threads
                                   {:first 10
                                    :sids []
                                    :category "IN_PROGRESS"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Welcome"} subjects))))
    (testing "Querying mod discussion gets only discussion messages."
      (let [response (send-request q:modmail-threads
                                   {:first 10
                                    :sids []
                                    :category "MOD_DISCUSSIONS"
                                    :unread_only false}
                                   (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body response)
            edges (get-in data [:modmail_threads :edges])
            subjects (set (map #(get-in % [:node :subject]) edges))]
        (is (= 200 (:status response)))
        (is (nil? errors))
        (is (= #{"Talk"} subjects))))))

(def m:contact-mods-mid "
mutation ($subject: String!, $content: String!, $sid: String!) {
  create_message_to_mods (subject: $subject, content: $content, sid: $sid) {
    mid
  }
}")

(def m:update-unread "
mutation ($mid: String!, $unread: Boolean!) {
  update_message_unread (mid: $mid, unread: $unread)
}")

(deftest test-mark-read-and-unread
  (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        mod-session (utils/make-session mod-uid)
        response (send-request m:contact-mods-mid
                               {:subject "Random complaint"
                                :content "Do better!"
                                :sid sid}
                               (utils/request-cookies user-uid))
        {:keys [data errors]} (:body response)]
    (is (= 200 (:status response)))
    (is (nil? errors))
    (let [mid (get-in data [:create_message_to_mods :mid])]
      (testing "a message starts out unread for the receiver"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only true}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              subject (get-in edges [0 :node :subject])
              unread (get-in edges [0 :node :latest_message :unread])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= "Random complaint" subject))
          (is unread)))

      (testing "the receiver can mark a message read"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers mod-session))
          (utils/send-init {:token (utils/sign-csrf-token mod-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (utils/hash-query
                                              s:notification-counts)}})
          (let [response (send-request m:update-unread
                                       {:mid mid
                                        :unread false}
                                       (utils/request-cookies mod-uid))
                {:keys [errors data]} (:body response)]
            (is (= 200 (:status response)))
            (is (nil? errors))
            (is (= mid (:update_message_unread data))))
          (utils/expect-message {:type "data"
                                 :id id
                                 :payload {:data
	                                   {:notification_counts
	                                    {:unread_message_count 0,
	                                     :unread_notification_count 0,
	                                     :sub_notifications
	                                     [{:sid sid
	                                       :open_report_count nil,
	                                       :unread_modmail_count 0}]}}}})))

      (testing "the read message flag is set correctly"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              subject (get-in edges [0 :node :subject])
              unread (get-in edges [0 :node :latest_message :unread])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= "Random complaint" subject))
          (is (not unread))))

      (testing "a query for unread messages doesn't fetch read ones"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only true}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= {:modmail_threads nil} data))))

      (testing "the receiver can mark a message unread"
        (utils/with-websocket id (utils/open-websocket
                                  (utils/ws-headers mod-session))
          (utils/send-init {:token (utils/sign-csrf-token mod-session)})
          (utils/expect-message {:type "connection_ack"})
          (utils/send-data {:id id
                            :type :start
                            :payload {:query (utils/hash-query
                                              s:notification-counts)}})
          (let [response (send-request m:update-unread
                                       {:mid mid
                                        :unread true}
                                       (utils/request-cookies mod-uid))
                {:keys [errors data]} (:body response)]
            (is (= 200 (:status response)))
            (is (nil? errors))
            (is (= mid (:update_message_unread data))))
          (utils/expect-message {:type "data"
                                 :id id
                                 :payload {:data
	                                   {:notification_counts
	                                    {:unread_message_count 0,
	                                     :unread_notification_count 0,
	                                     :sub_notifications
	                                     [{:sid sid
	                                       :open_report_count nil,
	                                       :unread_modmail_count 1}]}}}})))

      (testing "a message marked unread is unread"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])
              subject (get-in edges [0 :node :subject])
              unread (get-in edges [0 :node :latest_message :unread])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= "Random complaint" subject))
          (is unread))))))

(def m:contact-mods-mid-thread "
mutation ($subject: String!, $content: String!, $sid: String!) {
  create_message_to_mods (subject: $subject, content: $content, sid: $sid) {
    mid
    thread {
      id
    }
  }
}")

(def m:create-new-modmail-mid-thread "
mutation ($subject: String!, $content: String!, $sid: String!,
          $username: String, $show_mod_username: Boolean) {
  create_modmail_message (subject: $subject, content: $content, sid: $sid,
                          username: $username,
                          show_mod_username: $show_mod_username) {
    mid
    thread {
      id
    }
  }
}")

(def m:update-modmail-mailbox "
mutation ($thread_id: String!, $mailbox: MessageMailbox!) {
  update_modmail_mailbox (thread_id: $thread_id, mailbox: $mailbox) {
    subject
  }
}")

(def q:message-thread-by-id-mailbox "
query ($thread_id: String!) {
  message_thread_by_id (thread_id: $thread_id) {
    mailbox
  }
}")

(deftest test-queries-on-unarchived-messages
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        r1 (send-request m:contact-mods-mid-thread
                         {:subject "Random complaint"
                          :content "Do better!"
                          :sid sid}
                         (utils/request-cookies user-uid))
        r2 (send-request m:create-new-modmail-mid-thread
                         {:subject "Welcome"
                          :content "Modmail!"
                          :sid sid
                          :username user-name
                          :show_mod_username true}
                         (utils/request-cookies mod-uid))
        msg1 (get-in r1 [:body :data :create_message_to_mods])
        msg2 (get-in r2 [:body :data :create_modmail_message])]
    (is (= 200 (:status r1)))
    (is (= 200 (:status r2)))
    (is (nil? (get-in r1 [:body :errors])))
    (is (nil? (get-in r2 [:body :errors])))

    (testing "messages start out in the inbox"
      (let [r1 (send-request q:message-thread-by-id-mailbox
                             {:thread_id (get-in msg1 [:thread :id])}
                             (utils/request-cookies mod-uid))
            thread1 (get-in r1 [:body :data :message_thread_by_id])
            r2 (send-request q:message-thread-by-id-mailbox
                             {:thread_id (get-in msg2 [:thread :id])}
                             (utils/request-cookies mod-uid))
            thread2 (get-in r2 [:body :data :message_thread_by_id])]
        (is (= 200 (:status r1)))
        (is (nil? (get-in r1 [:body :errors])))
        (is (= "INBOX" (:mailbox thread1)))
        (is (= 200 (:status r2)))
        (is (nil? (get-in r2 [:body :errors])))
        (is (= "INBOX" (:mailbox thread2)))))

    (testing "new messages start out unarchived"
      (testing "- query for all messages finds them"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= 2 (count edges)))
          (is (= "Welcome" (get-in edges [0 :node :subject])))
          (is (= "Random complaint" (get-in edges [1 :node :subject])))
          (let [time (get-in edges [0 :node :first_message :time])]
            (is (parse-long time)))
          (is (= {:mid (:mid msg2)
                  :content "Modmail!"
                  :mtype "MOD_TO_USER_AS_USER"
                  :unread false}
                 (-> (get-in edges [0 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [0 :node :latest_message])
                     (dissoc :time))))
          (let [time (get-in edges [0 :node :first_message :time])]
            (is (parse-long time)))
          (is (= {:mid (:mid msg1)
                  :content "Do better!"
                  :mtype "USER_TO_MODS"
                  :unread true}
                 (-> (get-in edges [1 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [1 :node :latest_message])
                     (dissoc :time))))))
      (testing "- query for new messages finds them"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= 1 (count edges)))
          (is (= "Random complaint" (get-in edges [0 :node :subject])))
          (let [time (get-in edges [0 :node :first_message :time])]
            (is (parse-long time)))
          (is (= {:mid (:mid msg1)
                  :content "Do better!"
                  :mtype "USER_TO_MODS"
                  :unread true}
                 (-> (get-in edges [0 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [0 :node :latest_message])
                     (dissoc :time))))))
      (testing "- query for archived messages does not find them"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "ARCHIVED"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (nil? (:modmail_threads data))))))))

(deftest test-queries-on-archived-messages
  (let [{:keys [user-uid user-name mod-uid
                sid]} (utils/mod-user-and-sub *system*)
        r1 (send-request m:contact-mods-mid-thread
                         {:subject "Random complaint"
                          :content "Do better!"
                          :sid sid}
                         (utils/request-cookies user-uid))
        r2 (send-request m:create-new-modmail-mid-thread
                         {:subject "Welcome"
                          :content "Modmail!"
                          :sid sid
                          :username user-name
                          :show_mod_username true}
                         (utils/request-cookies mod-uid))
        msg1 (get-in r1 [:body :data :create_message_to_mods])
        msg2 (get-in r2 [:body :data :create_modmail_message])]
    (is (= 200 (:status r1)))
    (is (= 200 (:status r2)))
    (is (nil? (get-in r1 [:body :errors])))
    (is (nil? (get-in r2 [:body :errors])))

    (testing "messages can be archived"
      (let [r1 (send-request m:update-modmail-mailbox
                             {:thread_id (get-in msg1 [:thread :id])
                              :mailbox "ARCHIVED"}
                             (utils/request-cookies mod-uid))
            r2 (send-request m:update-modmail-mailbox
                             {:thread_id (get-in msg2 [:thread :id])
                              :mailbox "ARCHIVED"}
                             (utils/request-cookies mod-uid))]
        (is (= 200 (:status r1)))
        (is (= 200 (:status r2)))
        (is (nil? (get-in r1 [:body :errors])))
        (is (nil? (get-in r2 [:body :errors]))))
      (testing "- query for archived messages finds them"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "ARCHIVED"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)
              edges (get-in data [:modmail_threads :edges])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= 2 (count edges)))
          (is (= "Welcome" (get-in edges [0 :node :subject])))
          (is (= sid (get-in edges [0 :node :sub :sid])))
          (is (= {:mid (:mid msg2)
                  :content "Modmail!"
                  :mtype "MOD_TO_USER_AS_USER"
	          :unread false}
                 (-> (get-in edges [0 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [0 :node :latest_message])
                     (dissoc :time))))
          (is (= {:mid (:mid msg1)
                  :content "Do better!"
                  :mtype "USER_TO_MODS"
                  :unread true}
                 (-> (get-in edges [1 :node :first_message])
                     (dissoc :time))
                 (-> (get-in edges [1 :node :latest_message])
                     (dissoc :time))))))
      (testing "- query for new messages does not find them"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "NEW"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (nil? (:modmail_threads data)))))
      (testing "- query for all messages does not find them"
        (let [response (send-request q:modmail-threads
                                     {:first 10
                                      :sids []
                                      :category "ALL"
                                      :unread_only false}
                                     (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body response)]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (nil? (:modmail_threads data))))))))

(def m:create-modmail-reply "
mutation ($thread_id: String!, $content: String!, $sid: String!,
          $send_to_user: Boolean!, $show_mod_username: Boolean) {
  create_modmail_reply (thread_id: $thread_id, content: $content,
                        send_to_user: $send_to_user,
                        show_mod_username: $show_mod_username) {
    mid
    content
    mtype
    receiver {
      name
    }
    sender {
      name
    }
  }
}")

(def q:modmail-thread "
query ($first: Int!, $after: String, $thread_id: String!) {
  modmail_thread (first: $first, after: $after, thread_id: $thread_id) {
    edges {
      node {
        mid
        content
        unread
      }
      cursor
    }
    pageInfo {
      hasNextPage
      startCursor
      endCursor
    }
  }
}")

(def q:modmail-thread-by-id "
query ($thread_id: String!) {
  message_thread_by_id (thread_id: $thread_id) {
    id
    sub {
      name
    }
    subject
    first_message {
      content
    }
  }
}")

(deftest test-modmail-thread-query
  (let [{:keys [user-uid user-name mod-uid mod-name
                sid sub-name]} (utils/mod-user-and-sub *system*)
        rsp1 (send-request m:contact-mods-mid-thread
                           {:subject "Random complaint"
                            :content "Do better!"
                            :sid sid}
                           (utils/request-cookies user-uid))
        msg1 (get-in rsp1 [:body :data :create_message_to_mods])
        mid1 (:mid msg1)
        rsp2 (send-request m:create-modmail-reply
                           {:thread_id (get-in msg1 [:thread :id])
                            :content "Here's your answer!"
                            :send_to_user true
                            :show_mod_username true}
                           (utils/request-cookies mod-uid))
        msg2 (get-in rsp2 [:body :data :create_modmail_reply])
        mid2 (:mid msg2)]
    (is (= 200 (:status rsp1)))
    (is (= 200 (:status rsp2)))
    (is (nil? (get-in rsp1 [:body :errors])))
    (is (map? (:body rsp2)))
    (is (nil? (get-in rsp2 [:body :errors])))
    (is (= {:content "Here's your answer!"
            :mid mid2
            :mtype "MOD_TO_USER_AS_USER"
            :receiver {:name user-name}
            :sender {:name mod-name}}
           msg2))

    (testing "modmail_thread query"
      (let [rsp1 (send-request q:modmail-thread
                               {:first 10
                                :thread_id (get-in msg1 [:thread :id])}
                               (utils/request-cookies mod-uid))
            {:keys [errors data]} (:body rsp1)
            edges (get-in data [:modmail_thread :edges])]
        (is (= 200 (:status rsp1)))
        (is (nil? errors))
        (is (= 2 (count edges)))
        (is (= {:mid mid2
                :content "Here's your answer!"
                :unread false}
               (get-in edges [0 :node])))
        (is (= {:mid mid1 :content "Do better!"
                :unread true}
               (get-in edges [1 :node])))))
    (testing "modmail_thread_by_id query"
      (testing "works for mod"
        (let [rsp (send-request q:modmail-thread-by-id
                                {:thread_id (get-in msg1 [:thread :id])}
                                (utils/request-cookies mod-uid))
              {:keys [errors data]} (:body rsp)]
          (is (= 200 (:status rsp)))
          (is (nil? errors))
          (is (= {:message_thread_by_id
	          {:id (get-in msg1 [:thread :id])
	           :sub {:name sub-name}
	           :subject "Random complaint"
	           :first_message {:content "Do better!"}}}
                 data))))
      (testing "fails for user"
        (let [rsp (send-request q:modmail-thread-by-id
                                {:thread_id (get-in msg1 [:thread :id])}
                                (utils/request-cookies user-uid))
              {:keys [data]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= {:message_thread_by_id nil} data))))
      (testing "fails when unauthenticated"
        (let [rsp (send-request q:modmail-thread-by-id
                                {:thread_id (get-in msg1 [:thread :id])})
              {:keys [data]} (:body rsp)]
          (is (= 403 (:status rsp)))
          (is (= data {:message_thread_by_id nil})))))))

(def m:update-thread-unread "
mutation ($thread_id: String!, $unread: Boolean!) {
  update_thread_unread (thread_id: $thread_id, unread: $unread)
}")

(deftest test-modmail-mark-thread-unread
  (let [{:keys [user-uid mod-uid sid]} (utils/mod-user-and-sub *system*)
        mod-session (utils/make-session mod-uid)]
    (utils/with-websocket id (utils/open-websocket
                              (utils/ws-headers mod-session))
      (utils/send-init {:token (utils/sign-csrf-token mod-session)})
      (utils/expect-message {:type "connection_ack"})
      (utils/send-data {:id id
                        :type :start
                        :payload {:query (utils/hash-query
                                          s:notification-counts)}})
      (testing "update_thread_unread mutation"
        (let [rsp1 (send-request m:contact-mods-mid-thread
                                 {:subject "Random complaint"
                                  :content "Do better!"
                                  :sid sid}
                                 (utils/request-cookies user-uid))
              msg1 (get-in rsp1 [:body :data :create_message_to_mods])
              mtid (get-in msg1 [:thread :id])
              rsp2 (send-request m:create-modmail-reply
                                 {:thread_id mtid
                                  :content "Here's your answer!"
                                  :send_to_user true
                                  :show_mod_username false}
                                 (utils/request-cookies mod-uid))]
          (is (= 200 (:status rsp1)))
          (is (nil? (get-in rsp1 [:body :errors])))
          (is (= 200 (:status rsp2)))
          (is (nil? (get-in rsp2 [:body :errors])))
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:notification_counts
	                                    {:unread_message_count 0
	                                     :unread_notification_count 0
	                                     :sub_notifications
                                             [{:sid sid
                                               :open_report_count nil
                                               :unread_modmail_count 1}]}}}})
          ;; unread_modmail_count goes to zero on the second message because
          ;; it's really counting threads where the newest message is unread,
          ;; and since the mod sent the newest message, it's not unread.
          (utils/expect-message {:type "data"
	                         :id id
	                         :payload {:data
	                                   {:notification_counts
	                                    {:unread_message_count 0
	                                     :unread_notification_count 0
	                                     :sub_notifications
                                             [{:sid sid
                                               :open_report_count nil
                                               :unread_modmail_count 0}]}}}})
          (testing "marks a thread unread"
            (let [rsp1 (send-request m:update-thread-unread {:thread_id mtid
                                                             :unread true}
                                     (utils/request-cookies mod-uid))
                  errors (get-in rsp1 [:body :errors])]
              (is (= 200 (:status rsp1)))
              (is (= mtid (get-in rsp1 [:body :data :update_thread_unread])))
              (is (nil? errors))
              (utils/expect-message {:type "data"
                                     :id id
                                     :payload {:data
	                                       {:notification_counts
	                                        {:unread_message_count 0
	                                         :unread_notification_count 0
	                                         :sub_notifications
                                                 [{:sid sid
                                                   :open_report_count nil
                                                   :unread_modmail_count 1}]}}}})
              (let [rsp2 (send-request q:modmail-thread
                                       {:first 10
                                        :thread_id (get-in msg1 [:thread :id])}
                                       (utils/request-cookies mod-uid))
                    {:keys [errors data]} (:body rsp2)
                    edges (get-in data [:modmail_thread :edges])]
                (is (= 200 (:status rsp2)))
                (is (nil? errors))
                (is (= 2 (count edges)))
                (is (= [true true] (map #(get-in % [:node :unread]) edges))))))
          (testing "marks a thread read"
            (let [rsp1 (send-request m:update-thread-unread {:thread_id mtid
                                                             :unread false}
                                     (utils/request-cookies mod-uid))
                  errors (get-in rsp1 [:body :errors])]
              (is (= 200 (:status rsp1)))
              (is (nil? errors))
              (let [rsp2 (send-request q:modmail-thread
                                       {:first 10
                                        :thread_id mtid}
                                       (utils/request-cookies mod-uid))
                    {:keys [errors data]} (:body rsp2)
                    edges (get-in data [:modmail_thread :edges])]
                (is (= 200 (:status rsp2)))
                (is (nil? errors))
                (is (= 2 (count edges)))
                (is (= [false false] (map #(get-in % [:node :unread]) edges)))
                (utils/expect-message
                 {:type "data"
                  :id id
                  :payload {:data
	                    {:notification_counts
	                     {:unread_message_count 0
	                      :unread_notification_count 0
	                      :sub_notifications
                              [{:sid sid
                                :open_report_count nil
                                :unread_modmail_count 0}]}}}})))))))))

(def m:create-new-message "
mutation ($subject: String!, $content: String!, $username: String) {
  create_private_message (subject: $subject, content: $content,
                          username: $username) {
    mid
    content
    mtype
    receiver {
      name
    }
    sender {
      name
    }
  }
}")

(def m:get-message "
query ($mid: String!) {
  message_by_mid (mid: $mid) {
    content
    mtype
    receiver {
      uid
    }
    sender {
      uid
    }
  }
}")

(deftest test-create-private-message
  (testing "send new private message"
    (let [{:keys [mod-uid mod-name
                  user-uid user-name]} (utils/mod-user-and-sub *system*)
          response (send-request m:create-new-message
                                 {:subject "Hello"
                                  :content "Just between us!"
                                  :username user-name}
                                 (utils/request-cookies mod-uid))
          data (get-in response [:body :data])
          errors (get-in response [:body :errors])
          mid (get-in data [:create_private_message :mid])]
      (is (= 200 (:status response)))
      (is (nil? errors))
      (is (= {:create_private_message
	      {:content "Just between us!"
               :mtype "USER_TO_USER"
               :sender {:name mod-name}
               :receiver {:name user-name}}}
             (update data :create_private_message dissoc :mid)))
      (testing "recipient can read message"
        (let [response (send-request m:get-message
                                     {:mid mid}
                                     (utils/request-cookies user-uid))
              data (get-in response [:body :data])
              errors (get-in response [:body :errors])]
          (is (= 200 (:status response)))
          (is (nil? errors))
          (is (= {:message_by_mid
	          {:content "Just between us!"
                   :mtype "USER_TO_USER"
                   :sender {:uid mod-uid}
                   :receiver {:uid user-uid}}}
                 data)))))))
