let
  pkgs = import <nixpkgs> {};
in
pkgs.mkShell {
  buildInputs = [
    pkgs.leiningen pkgs.jdk17 pkgs.clojure pkgs.clojure-lsp
    pkgs.clj-kondo pkgs.geckodriver pkgs.chromedriver
  ];
}
